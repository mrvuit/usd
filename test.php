<?php

	$data = [
		1 	=> [
			'id'				=> 1,
			'name'				=> 'Item 1',
			'parent_item_id'	=> 0,
			'number_of_child'	=> 1,
			'node'				=> 1,
			'level_path1'		=> null,
			'level_path2'		=> null,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 1,
		],
		2 	=> [
			'id'				=> 2,
			'name'				=> 'Item 2',
			'parent_item_id'	=> 1,
			'number_of_child'	=> 2,
			'node'				=> 2,
			'level_path1'		=> 1,
			'level_path2'		=> null,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 1,
		],
		3 	=> [
			'id'				=> 3,
			'name'				=> 'Item 3',
			'parent_item_id'	=> 2,
			'number_of_child'	=> 0,
			'node'				=> 3,
			'level_path1'		=> 1,
			'level_path2'		=> 2,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 2,
		],
		4 	=> [
			'id'				=> 4,
			'name'				=> 'Item 4',
			'parent_item_id'	=> 2,
			'number_of_child'	=> 0,
			'node'				=> 3,
			'level_path1'		=> 1,
			'level_path2'		=> 2,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 1,
		],
		5 	=> [
			'id'				=> 5,
			'name'				=> 'Item 5',
			'parent_item_id'	=> 4,
			'number_of_child'	=> 0,
			'node'				=> 4,
			'level_path1'		=> 1,
			'level_path2'		=> 2,
			'level_path3'		=> 4,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 1,
		],
		6 	=> [
			'id'				=> 6,
			'name'				=> 'Item 6',
			'parent_item_id'	=> 5,
			'number_of_child'	=> 0,
			'node'				=> 5,
			'level_path1'		=> 1,
			'level_path2'		=> 2,
			'level_path3'		=> 4,
			'level_path4'		=> 5,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 1,
		],
		7 	=> [
			'id'				=> 7,
			'name'				=> 'Item 7',
			'parent_item_id'	=> 0,
			'number_of_child'	=> 0,
			'node'				=> 1,
			'level_path1'		=> null,
			'level_path2'		=> null,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 2,
		],
		8 	=> [
			'id'				=> 8,
			'name'				=> 'Item 8',
			'parent_item_id'	=> 0,
			'number_of_child'	=> 0,
			'node'				=> 1,
			'level_path1'		=> null,
			'level_path2'		=> null,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 1,
		],
		9 	=> [
			'id'				=> 9,
			'name'				=> 'Item 9',
			'parent_item_id'	=> 1,
			'number_of_child'	=> 0,
			'node'				=> 2,
			'level_path1'		=> 1,
			'level_path2'		=> null,
			'level_path3'		=> null,
			'level_path4'		=> null,
			'organization_id'	=> 1,
			'item_type'			=> 1,
			'sort_order'		=> 2,
		],
	];

function getTreeFreeItem($dataFreeItem, $parent_id = 0,  $char = '')
{
	global $b;
	global $tree;
	$b++;
    foreach ($dataFreeItem as $key => $item)
    {
        if ($item['parent_item_id'] == $parent_id)
        {
        	if($item['node'] == 1) {
        		$tree[$item['id']] = $item;
        	}
        	elseif($item['node'] == 2) {
        		$tree[$item['level_path1']]['child'][$item['id']] = $item;
        	}
        	elseif($item['node'] == 3) {
        		$tree[$item['level_path1']]['child'][$item['level_path2']]['child'][$item['id']] = $item;
        	}
        	elseif($item['node'] == 4) {
        		$tree[$item['level_path1']]['child'][$item['level_path2']]['child'][$item['level_path3']]['child'][$item['id']] = $item;
        	}
        	elseif ($item['node'] == 5) {
        		$tree[$item['level_path1']]['child'][$item['level_path2']]['child'][$item['level_path3']]['child'][$item['level_path4']]['child'][$item['id']] = $item;
        	}
        	echo $char.' '. $item['name'].' --- Node'.$item['node'].' --- Leve path '.$item['level_path1'].$item['level_path2'].$item['level_path3'].$item['level_path4'].'<br>';
            unset($dataFreeItem[$key]);
            getTreeFreeItem($dataFreeItem, $item['id'],  $char.'|---');
        }
    }
    if($b == 10) {
    	return $tree;
    }


}
echo '<pre>';
$TreeFreeItem = getTreeFreeItem($data);
// print_r($TreeFreeItem);
// usort($TreeFreeItem, function($a, $b) {return $a['sort_order'] <=> $b['sort_order'];});


foreach ($TreeFreeItem as $key => $item_level1) {
	echo '<ul>';
	echo '<li>'.$item_level1['name'].' - sort_order '.$item_level1['sort_order'].'</li>';
	if(isset($item_level1['child']))
	foreach ($item_level1['child'] as $key => $item_level2) {
		echo '<ul>';
		echo '<li>'.$item_level2['name'].' - sort_order '.$item_level2['sort_order'].'</li>';
		if(isset($item_level2['child']))
		foreach ($item_level2['child'] as $key => $item_level3) {
			echo '<ul>';
			echo '<li>'.$item_level3['name'].' - sort_order '.$item_level3['sort_order'].'</li>';
			if(isset($item_level3['child']))
			foreach ($item_level3['child'] as $key => $item_level4) {
				echo '<ul>';
				echo '<li>'.$item_level4['name'].' - sort_order '.$item_level4['sort_order'].'</li>';
				if(isset($item_level4['child']))
				foreach ($item_level4['child'] as $key => $item_level5) {
					echo '<ul>';
					echo '<li>'.$item_level5['name'].' - sort_order '.$item_level5['sort_order'].'</li>';
					echo '</ul>';
				}
				echo '</ul>';
			}
			echo '</ul>';
		}
		echo '</ul>';
	}
	echo '</ul>';
}
// $data1 = [
// 	1 => [
// 		'key' => 1,
// 	],
// 	2 => [
// 		'key' => 2,
// 	]
// ];

// $data2 = [
// 	1 => [
// 		'key' => 1,
// 		'child' => [
// 			2 => [
// 				'key' => 2,
// 			]

// 		]
// 	]

// ];

// echo count($data2, 1);

// echo '<pre>';
// print_r($data2);

