-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2018 at 10:32 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usd`
--

-- --------------------------------------------------------

--
-- Table structure for table `suport`
--

CREATE TABLE `suport` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `content` text,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `time` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suport`
--

INSERT INTO `suport` (`id`, `email`, `content`, `user_id`, `time`) VALUES
(1, 'vutrinh352@gmail.com', 'awdadwawd awd ăd', 1, 1516941357),
(2, 'vutl@soft-flight.work', 'awdawdawd awd aw', 0, 1516941400),
(3, 'vutrinh352@gmail.com', 'awdawd aw daw ad', 1, 1516950095),
(4, 'vutrinh352@gmail.com', 'awdawda wda awd', 1, 1516950102),
(5, 'vutrinh352@gmail.com', 'awdawda wd awd', 1, 1516950632);

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE `system` (
  `key` varchar(20) NOT NULL DEFAULT '',
  `value` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`key`, `value`) VALUES
('per_referral', '6.5'),
('btc_price_usd', '10220.2'),
('eth_price_usd', '1094.85'),
('time_out_cancel', '600');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `decline` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL,
  `money` double(20,2) NOT NULL DEFAULT '0.00',
  `wallet_id` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `time` int(10) NOT NULL DEFAULT '0',
  `updated` int(10) NOT NULL DEFAULT '0',
  `comment` varchar(300) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `decline`, `user_id`, `money`, `wallet_id`, `status`, `time`, `updated`, `comment`, `type`) VALUES
(1, 0, 1, 12.00, '1', 1, 1517385972, 1517385972, 'WITHDRAW - $12.00 - Perfect Money($ 88) - U12345678 - 2018/01/31 08:01:06', 2),
(2, 0, 1, 11.00, '1', 1, 1517389263, 1517389263, 'WITHDRAW - $11.00 - Perfect Money($ 77) - U12345678 - 2018/01/31 09:01:01', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `pay_status` tinyint(1) NOT NULL DEFAULT '0',
  `reg_date` int(10) NOT NULL DEFAULT '0',
  `last_login` int(10) NOT NULL DEFAULT '0',
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `ref` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `pay_status`, `reg_date`, `last_login`, `activated`, `ref`) VALUES
(1, 'vutrinh352@gmail.com', 'd72b7d682a9fd8cee79f8a58a636e730', 0, 1516589692, 0, 1, 0),
(2, 'vutrinh3521@gmail.com', 'a1ec50968cc45b2a5f87536817266c1c', 0, 1516785968, 0, 0, 0),
(3, 'awdawdawdawd@awd.com', '7b4822fd870b5f4c3cc3191e837d45b2', 0, 1516933963, 0, 0, 0),
(4, 'vutrinh35211@gmail.com', 'a1ec50968cc45b2a5f87536817266c1c', 0, 1516934146, 0, 0, 1),
(5, 'vutrinh35211111@gmail.com', 'd72b7d682a9fd8cee79f8a58a636e730', 0, 1517279701, 0, 0, 0),
(6, 'vutrinh@gmail.com', 'd72b7d682a9fd8cee79f8a58a636e730', 0, 1517374492, 0, 0, 0),
(7, 'vutrinh352111@gmail.com', 'a6b8c3983e7b9479495b911e9c1d415e', 0, 1517385094, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(10) UNSIGNED NOT NULL,
  `address` varchar(300) NOT NULL DEFAULT '',
  `user_id` int(10) NOT NULL DEFAULT '0',
  `wallet_id` int(10) NOT NULL DEFAULT '0',
  `money` double(20,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `address`, `user_id`, `wallet_id`, `money`) VALUES
(1, 'U12345678', 1, 1, 77.00),
(2, '', 1, 2, 0.00),
(3, '', 1, 3, 0.00),
(4, '', 1, 4, 0.00),
(5, '', 7, 1, 0.00),
(6, '', 7, 2, 0.00),
(7, '', 7, 3, 5.00),
(8, '', 7, 4, 2.00),
(9, '', 6, 1, 0.00),
(10, '', 6, 2, 0.00),
(11, '', 6, 3, 5.00),
(12, '', 6, 4, 3.00);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_cat`
--

CREATE TABLE `wallet_cat` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `simplename` varchar(100) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `regex` varchar(100) NOT NULL DEFAULT '',
  `deposit` varchar(100) NOT NULL DEFAULT '',
  `withdraw` varchar(100) NOT NULL DEFAULT '',
  `amount` double(20,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallet_cat`
--

INSERT INTO `wallet_cat` (`id`, `name`, `simplename`, `address`, `regex`, `deposit`, `withdraw`, `amount`) VALUES
(1, 'Perfect Money', 'perfectmoney', 'U12345678', '/^U[0-9]{8}$/', '1-2500', '0.10-unlimited', 0.00),
(2, 'Payeer', 'payeer', 'P12345678', '/^P[0-9]{8}$/', '1-2500', '0.10-unlimited', 0.00),
(3, 'Bitcoin', 'bitcoin', '1MVx9ewrgtrbdSXM7H3w67wZHoWwrsckwQ', '/^[a-zA-Z0-9]{27,34}$/', '20-50000', '20-unlimited', 5.00),
(4, 'Ethereum', 'ethereum', '0x32e5dA647140C58aD84A9d7932a9eCf0Ae0F0a91', '/^(0x)?[0-9a-fA-F]{40,42}$/', '20-50000', '20-unlimited', 3.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `suport`
--
ALTER TABLE `suport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_cat`
--
ALTER TABLE `wallet_cat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `suport`
--
ALTER TABLE `suport`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `wallet_cat`
--
ALTER TABLE `wallet_cat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
