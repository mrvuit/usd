	

	
	$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


    $("#show-password").click(function(){
    	if($('#password').attr('type') == 'password') {
			$('#password').attr('type', 'text');
			$('#repeatPassword').attr('type', 'text');
			$('#show-password').html('<i class="fa fa-eye-slash"></i>');
    	} else {
			$('#password').attr('type', 'password');
			$('#repeatPassword').attr('type', 'password');
			$('#show-password').html('<i class="fa fa-eye"></i>');
		}
    });
    $('#submitRegister').click(function() {
    	validateFormRegister();
    });
    $('#submitLogin').click(function() {
    	checkLogin();
    });
    function checkLogin() {
    	// INPUT
    	$('#email-text').html('');
    	$('#block-email').attr("class","form-group");
    	$('#password-text').html('');
    	$('#block-password').attr("class","form-group");
    	var email = $('#email').val().trim();
    	var password = $('#password').val().trim();
    	var checkInput = true;
    	if(email == '') {
  			$('#block-email').attr("class","form-group has-danger");
  			$('#email-text').html('<i class="fa fa-exclamation-circle"></i> Email is not vacant!');
  			checkInput = false;
    	}
    	if(password == '') {
  			$('#block-password').attr("class","form-group has-danger");
  			$('#password-text').html('<i class="fa fa-exclamation-circle"></i> Password is not vacant!');
  			checkInput = false;
    	}
    	if(checkInput == true) {
	 		$.ajax({
		          url: "/ajax/ajaxLogin",
		          type: "POST",
		          data: $("#form-login").serialize(),
		          beforeSend: function() {
					$('#login-loading').show();
		          },
		          success: function(response) {
		          	$('#login-loading').hide();
					
					console.log(response);
					if(response == 'true') {
						$('#login-div').hide();
						$('#login-success').show();
						window.location.href = '/' + $('#language-key').html() + '/home';
					} else {
						var data = $.parseJSON(response);
						$('#login-text').html('<i class="fa fa-exclamation-circle"></i> ' + data['message']);
					}
					grecaptcha.reset(reCAPTCHA);
		          },
		          error: function(response) {
		              alert('error');
		          }
		      });
    	}

    }

    function validateFormRegister() {
    	// INPUT
    	var email = $('#email').val().trim();
    	var password = $('#password').val();
    	var repeatPassword = $('#repeatPassword').val();
    	// REGX
    	var regxEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    	var regxPassWord = /^([a-zA-Z0-9_!@#$%^&*]{6,32})+$/;
    	var validated = true;
  		if(regxEmail.test(email) == false) {
  			$('#block-email').attr("class","form-group has-danger");
  			$('#email-text').html('<i class="fa fa-exclamation-circle"></i> Invalid email format !');
  			validated = false;
  		} else {
  			$('#block-email').attr("class","form-group has-success");
  			$('#email-text').html('');
  		}

  		if(regxPassWord.test(password) == false) {
  			$('#block-password').attr("class","form-group has-danger");
  			$('#password-text').html('<i class="fa fa-exclamation-circle"></i> Only allow letters and numbers and some characters! @ # $% ^ & *, length 6 to 32.'); 
  			validated = false;
  		} else {
  			$('#block-password').attr("class","form-group has-success");
  			$('#password-text').html('');
  			if(password != repeatPassword) {
  				$('#block-repeat-password').attr("class","form-group has-danger");
  				$('#repeat-password-text').html('<i class="fa fa-exclamation-circle"></i> Repeat the password incorrectly!'); 
  				validated = false;
  			} else {
  				$('#block-repeat-password').attr("class","form-group has-success");
  				$('#repeat-password-text').html(''); 
  			}
  		}
  		if($('input[id="agreement"]:checked').length == 1) {
  			$('#block-agreement').attr("class","form-group has-success");
  		} else {
  			$('#block-agreement').attr("class","form-group has-danger");
  			validated = false;
  		}
  		if(validated == true) {
	 		$.ajax({
		          url: "/ajax/ajaxRegister",
		          type: "POST",
		          data: $("#form-register").serialize(),
		          beforeSend: function() {
					$('#register-loading').show();
		          },
		          success: function(response) {
		          	$('#register-loading').hide();
		          		var data = $.parseJSON(response);
		              if(response == 'true') {
		              	$('#register-div').hide();
		              	$('#register-success').show();
		              } else {
		              	$('#block-' + data['type']).attr("class","form-group has-danger");
		              	$('#'+data['type']+'-text').html('<i class="fa fa-exclamation-circle"></i> ' + data['message']);
		              }
		              grecaptcha.reset(reCAPTCHA);
		          },
		          error: function(response) {
		             console.log(response);
		          }
		      });
  		}
	}

    function Calculator() {
		var deposit = parseInt($('#deposit').val());
		var days = parseInt($('#days').val());
		if(days > 999) {
			$('#days').val(999);
			days = 999;
		} 
		
		if(isNaN(days)|| isNaN(deposit)) {
			$('#realy-profit').html('<i class="fa fa-usd"></i>0');
			$('#abs-balance').html('<i class="fa fa-usd"></i>0');
			return false;
		}
		var recap = $('input[name=recap]:checked').val();
		var absbal = deposit;
		var realyprofit = 0;
		for(i=1;i<=days;i++){
			if(recap == 1)
				absbal=absbal*1.013;
			else
				absbal=absbal+deposit*0.013;
		}
		realyprofit = (absbal - deposit);
		absbal = absbal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		realyprofit = realyprofit.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		$('#realy-profit').html('<i class="fa fa-usd"></i> ' + realyprofit);
		$('#abs-balance').html('<i class="fa fa-usd"></i> ' + absbal);
	}
	$(document).ready(function(){
		Calculator();
	});