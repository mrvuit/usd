CREATE TABLE IF NOT EXISTS `users` (
  `id` 			    int(10) unsigned 	  NOT NULL AUTO_INCREMENT,
  `email` 		  varchar(200) 		    NOT NULL DEFAULT '',  
  `password`    varchar(32)         NOT NULL DEFAULT '', 
  `password-n` 	varchar(32) 		    NOT NULL DEFAULT '', 
  `pay_status`	tinyint(1)		      NOT NULL DEFAULT 0, 
  `reg_date`	  int(10)				      NOT NULL DEFAULT 0,
  `last_login`	int(10)				      NOT NULL DEFAULT 0,
  `activated`   tinyint(1)          NOT NULL DEFAULT 0, 
  `ref`			    int(10)				      NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `forgot_password` (
  `id`          int(10) unsigned      NOT NULL AUTO_INCREMENT,
  `user_id`     int(10)               NOT NULL ,
  `hash_key`    varchar(32)           NOT NULL,
  `time`        int(10)               NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `wallet` (
  `id`          int(10) unsigned      NOT NULL AUTO_INCREMENT,
  `address`     varchar(300)          NOT NULL DEFAULT '', 
  `user_id`     int(10)               NOT NULL DEFAULT 0,
  `wallet_id`   int(10)               NOT NULL DEFAULT 0,
  `money`       double(20,2)          NOT NULL DEFAULT 0, 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `wallet_cat` (
  `id`          int(10) unsigned      NOT NULL AUTO_INCREMENT,
  `name`        varchar(100)          NOT NULL DEFAULT '', 
  `simplename`  varchar(100)          NOT NULL DEFAULT '',
  `address`     varchar(100)          NOT NULL DEFAULT '',
  `regex`       varchar(100)          NOT NULL DEFAULT '',
  `deposit`     varchar(100)          NOT NULL DEFAULT '',
  `withdraw`    varchar(100)          NOT NULL DEFAULT '',
  `amount`      double(20,2)          NOT NULL DEFAULT 0, 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `wallet_cat` (`name`, `simplename`, `address`, `regex`, `deposit`, `withdraw`, `amount`) VALUES ('Perfect Money', 'perfectmoney', 'U12345678', '/^U[0-9]{8}$/', '1-2500', '0.10-unlimited', 0);
INSERT INTO `wallet_cat` (`name`, `simplename`, `address`, `regex`, `deposit`, `withdraw`, `amount`) VALUES ('Payeer', 'payeer', 'P12345678', '/^P[0-9]{8}$/', '1-2500', '0.10-unlimited', 0);
INSERT INTO `wallet_cat` (`name`, `simplename`, `address`, `regex`, `deposit`, `withdraw`, `amount`) VALUES ('Bitcoin', 'bitcoin', '1MVx9ewrgtrbdSXM7H3w67wZHoWwrsckwQ', '/^[a-zA-Z0-9]{27,34}$/', '20-50000', '20-unlimited', 5);
INSERT INTO `wallet_cat` (`name`, `simplename`, `address`, `regex`, `deposit`, `withdraw`, `amount`) VALUES ('Ethereum', 'ethereum', '0x32e5dA647140C58aD84A9d7932a9eCf0Ae0F0a91', '/^(0x)?[0-9a-fA-F]{40,42}$/', '20-50000', '20-unlimited', 2);

CREATE TABLE IF NOT EXISTS `system` (
  `key`       varchar(20)         NOT NULL DEFAULT '', 
  `value`     varchar(50)         NOT NULL DEFAULT '',
   PRIMARY KEY (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `system` (`key`, `value`) VALUES ('per_referral', '6.5');
INSERT INTO `system` (`key`, `value`) VALUES ('btc_price_usd', '');
INSERT INTO `system` (`key`, `value`) VALUES ('eth_price_usd', '');
INSERT INTO `system` (`key`, `value`) VALUES ('time_out_cancel', '');
INSERT INTO `system` (`key`, `value`) VALUES ('btc_address', '');
INSERT INTO `system` (`key`, `value`) VALUES ('eth_address', '');
INSERT INTO `system` (`key`, `value`) VALUES ('time_update_cmc', '');
INSERT INTO `system` (`key`, `value`) VALUES ('time_interest', '');

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` 			  int(10) unsigned 	NOT NULL AUTO_INCREMENT,
  `decline`   int(10)           NOT NULL DEFAULT 0, -- parent id decline
  `user_id` 	int(10) 			    NOT NULL,  
  `money` 		double(20,2) 		  NOT NULL DEFAULT 0,
  `wallet_id`	varchar(20)				NOT NULL,
  `status`		tinyint(1)		    NOT NULL DEFAULT 0, -- 1 PENDING -- 2 COMPLETED - 3 DECLINE
  `time`      int(10)           NOT NULL DEFAULT 0,
  `updated`   int(10)           NOT NULL DEFAULT 0,
  `comment`   varchar(300)      NOT NULL DEFAULT 0,
  `type`      tinyint(1)        NOT NULL DEFAULT 0, -- 1 DEPOSIT  -- 2 WITHDRAW -- 3 INTEREST ACCRUAL  --- 4 SYSTEM
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `suport` (
  `id` 			int(10) unsigned 	NOT NULL AUTO_INCREMENT,
  `email` 		varchar(200) 		NOT NULL DEFAULT '', 
  `content` 	text,
  `user_id`		int(10)				  NOT NULL DEFAULT 0,
  `time`      int(10)         NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE users AUTO_INCREMENT=1500;