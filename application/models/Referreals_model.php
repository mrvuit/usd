<?php
/**
* 
*/
class Referreals_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Ende_model');
	}

	public function getReferralsList($user_id)
	{
		$this->db->select('email, reg_date, activated')->where('ref', $user_id);
		$query = $this->db->get('users');
		if($query->num_rows() == 0) 
			return false;
		$query = $query->result_array();
		foreach ($query as $key => $value) {
			$query[$key]['reg_date'] = $this->Ende_model->convertTime($value['reg_date']);
		}

		return $query;
	}
	
}