<?php
/**
* 
*/
class Wallet_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function updateMonney($user_id, $wallet_id, $amount, $type = '-')
	{
		if ($type == '+') {
			return $this->db->where(['user_id' => $user_id, 'id' => $wallet_id])
			->set('money', '`money` + '.$amount.'', FALSE)
			->update('wallet');
		}
			return $this->db->where(['user_id' => $user_id, 'id' => $wallet_id])
			->set('money', '`money` - '.$amount.'', FALSE)
			->update('wallet');
	}
	public function interestIncrease($time = null)
	{
		if($time == null) {
			return false;
		}
		if($this->db->update('system', ['value' => $time], ['key' => 'time_interest']) == true)
			return $this->db->set('money', '`money` + `money` * 0.013', FALSE)->update('wallet');
		return false;
		
	}
	private function checkAddressWallet($regex, $address)
	{
		if ($address != null && !preg_match($regex, $address)) { 
			return false;
		}
		return true;
	}
	public function getFullInfoWallet($wallet_cat_id, $user_id)
	{
		$this->db->select('wallet_id, address, money, id')->where(['wallet_id' => $wallet_cat_id, 'user_id' => $user_id]);
		$out = [];
		$query = $this->db->get('wallet');
		if($query->num_rows() == 0) {
			return false;
		}
		$out['wallet_user'] = $query->result_array()[0];
		$this->db->select('name, simplename')->where('id', $wallet_cat_id);
		$queryWalletCat = $this->db->get('wallet_cat');
		if($queryWalletCat->num_rows() == 0) {
			return false;
		}
		$out['wallet_cat'] = $queryWalletCat->result_array()[0];
		return $out;
	}
	public function validAddressWallet($input_data)
	{
		$walletCat = $this->getWalletCat();
		$error = [];
		foreach ($walletCat as $key => $item) {
			if($this->checkAddressWallet($item['regex'], $input_data[$item['id']]) == false) {
				$error[$item['simplename']]['err'] = 1;
				$error[$item['simplename']]['regex'] = $item['regex'];
			}
		}
		return $error;

	}
	public function updateWallet($user_id, $wallet) {
		foreach ($wallet as $key => $item) {
			$this->db->update('wallet', ['address' => $item], ['user_id' => $user_id, 'wallet_id' => $key]);
		}
		return true;
	}
	public function calcuInterest($balance)
	{
		return ($balance * 0.013);
	}
	public function getAmount($user_id)
	{
		$this->db->select('SUM(`money`) as money')->where('user_id', $user_id);
		$query = $this->db->get('wallet');
		return $query->result_array()[0]['money'];
	}
	public function getWalletCat()
	{
		$query = $this->db->select('*')->order_by('id ASC')->get('wallet_cat');
		$query = $query->result_array();
		return $query;
	}
	public function checkAvailable($user_id)
	{
		$user_wallet = $this->getUserWallet($user_id);
		if(count($user_wallet) == 0) {
			$this->checkWallet($user_id);
			return false;
		}
		foreach ($user_wallet as $key => $item) {
			if($item['address'] != null) {
				return true;
			}
		}
		return false;
	}
	public function getWalletCatbyID($wallet_cat_id) {
		$this->db->select('*')->where('id', $wallet_cat_id);
		$query = $this->db->get('wallet_cat');
		if($query->num_rows() == 0) {
			return false;
		}
		return $query->result_array()[0];
	}
	public function getUserWallet($user_id, $wallet_id = null)
	{
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		if($wallet_id != null) {
			$this->db->where(['wallet_id' => $wallet_id]);
		}
		$this->db->order_by('wallet_id ASC');
		return $this->db->get('wallet')->result_array();
	}
	public function checkWallet($user_id) 
	{
		$userWallet = $this->getUserWallet($user_id);
		$walletCat = $this->getWalletCat();
		if(count($userWallet) == count($walletCat)) {
			return true;
		}
		$user_wallet = [];
		if(count($userWallet) > 0)
			foreach ($userWallet as $key => $value) {
				array_push($user_wallet, $value['wallet_id']);
			}
		foreach ($walletCat as $key => $value) {
			if(!in_array($value['id'], $user_wallet)) {
				$data = [
					'address'	=> '',
					'user_id'	=> $user_id,
					'wallet_id'	=> $value['id'],
					'money'		=> $value['amount']
				];
				$this->db->insert('wallet', $data);
			}
		}
		return true;
	}
}