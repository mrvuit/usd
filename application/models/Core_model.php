<?php
/**
* 
*/
class Core_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Ende_model');
	}

	public function getUser($cookie_user)
	{
		$id 		= $this->Ende_model->decryptionId($cookie_user[0]);
		$password 	= $this->Ende_model->decryptionPassword($cookie_user[1]);

		$this->db->select('*')->where(array(
				'id' 		=> $id,
				'password'	=> $password
			)
		);
		$query = $this->db->get('users');
		if ($query->num_rows() == 0)
			return false;
		return $query->result_array()[0];
	}
	public function getSystemData()
	{
		$query = $this->db->select('*')->get('system');
		$query = $query->result_array();
		$out = [];
		foreach ($query as $key => $value) {
			$out[$value['key']] = $value['value'];
		}
		return $out;
	}
	public function countAllUser()
	{
		$query = $this->db->select('COUNT(id) AS `all`')->get('users');
		return $query->result_array()[0]['all'];
	}

	public function getBtcEth()
	{
		$data = json_decode($this->Ende_model->curlApi('https://api.coinmarketcap.com/v1/ticker/?limit=5'), true);
		$btceth = [];
		if(is_array($data) && count($data) == 5) {
			foreach ($data as $key => $item) {
				if($item['id'] == 'bitcoin') {
					$btceth['bitcoin'] = $item;
				}
				if($item['id'] == 'ethereum') {
					$btceth['ethereum'] = $item;
				}
				if(count($btceth) == 2) {
					$this->db->update('system', ['value' => $btceth['bitcoin']['price_usd']], ['key' => 'btc_price_usd']);
					$this->db->update('system', ['value' => $btceth['ethereum']['price_usd']], ['key' => 'eth_price_usd']);
					$this->db->update('system', ['value' => time()], ['key' => 'time_update_cmc']);
					return true;
				}
			}
		}
		return false;
	}
}