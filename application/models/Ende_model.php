<?php
/**
* 
*/
class Ende_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
	}
    public function getTimeInterest()
    {
        date_default_timezone_set("UTC");
        $date = new DateTime(date('Y-m-d', strtotime(' +1 day')).' 00:00:00');
        $timestamp =  $date->getTimestamp();
        return $timestamp;
    }
    public function checkLanguage($lng)
    {
        $lngArr = ['vn', 'cn', 'in', 'jp', 'kr'];
        if(in_array($lng, $lngArr) == true)
            return $lng;
        return 'en';
    }
    public function calcuInterest($money)
    {
        return number_format($money * 0.013, 2);
    }
    public function encryptionId($id)
    {
        $id = $id + 31415926;
        $id = dechex($id);
        $id = str_replace(1, 'I', $id);
        $id = str_replace(2, 'W', $id);
        $id = str_replace(3, 'O', $id);
        $id = str_replace(4, 'U', $id);
        $id = str_replace(5, 'Z', $id);
        return strtoupper($id);
    }
    public function decryptionId($id)
    {
        $id = str_replace('I', 1, $id);
        $id = str_replace('W', 2, $id);
        $id = str_replace('O', 3, $id);
        $id = str_replace('U', 4, $id);
        $id = str_replace('Z', 5, $id);
        $id = hexdec($id);
        $id = $id - 31415926;
        return $id;
    }
	public function encryptionPassword($password) {
		return $this->encryption->encrypt($password);
	}
	public function decryptionPassword($password) {
		return $this->encryption->decrypt($password);
	}
    public function convertTimstamp($var)
    {
        date_default_timezone_set("UTC");
        $distance = $var - (time());
        $hours = floor(($distance % (60 * 60 * 24)) / (60 * 60));
        $minutes = floor(($distance % (60 * 60)) / (60));
        $seconds = floor(($distance % (60)));
        $jun['time']        = $hours."h ".$minutes."m ".$seconds."s";
        $zezo = 0;
        $jun['progressbar'] = 100 - round((($hours.(strlen($minutes) == 1 ? ${'zezo'}.$minutes : $minutes).(strlen($seconds) == 1 ? ${'zezo'}.$seconds : $seconds))/235959)*100,2);
        return $jun;
    }
    public function convertTime($var) {
        $time  = time();
        $jun   = round(($time - $var) / 60);
        $shift = 7 * 3600;
        if (date('Y', $var) == date('Y', time())) {
            if ($jun < 1)
                $jun = 'Right now';
            if ($jun >= 1 && $jun < 60)
                $jun = "$jun minutes ago";
            if ($jun >= 60 && $jun < 1440) {
                $jun = round($jun / 60);
                $jun = "$jun hours ago";
            }
            if ($jun >= 1440 && $jun < 2880)
                $jun = "Yesterday · ".date("H:i a", $var + $shift);;
            if ($jun >= 2880 && $jun < 10080) {
                $day = round($jun / 60 / 24);
                $jun = "$day days ago";
            }
        }
        if ($jun > 10080) {
            $jun = date("d/m/Y", $var + $shift);
        }
        return $jun;
    }
	public function reCaptcha($response) {
		$secret = '6LfHkD4UAAAAANeoMGQKvT7SzRFbpc4IhL6fyM6D';
		$verifyResponse = file_get_contents(
			'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response
		);
		$responseData = json_decode($verifyResponse);
		if($responseData->success)
			return true;
		return false;
	}
    public function curlApi($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        return $result;
    }
    public function paypalTransaction($id)
    {
        $info = 'USER=longvu2003_api1.gmail.com'
                .'&PWD=J8NFZZ2P7GVLCTUV'
                .'&SIGNATURE=AYvApQjPaEK2tFXuE1zKCJB4vLEIAY1hfxcFOll1806VF8w8ArNC-8Tt'
                .'&METHOD=GetTransactionDetails'
                .'&TRANSACTIONID='.$id
                .'&VERSION=94';
        // $info = 'USER=longvu2003_api1.gmail.com'
        //      .'&PWD=J8NFZZ2P7GVLCTUV'
        //      .'&SIGNATURE=AYvApQjPaEK2tFXuE1zKCJB4vLEIAY1hfxcFOll1806VF8w8ArNC-8Tt'
        //         .'&METHOD=TransactionSearch'
        //         .'&TRANSACTIONCLASS=RECEIVED'
        //         .'&STARTDATE=2013-01-08T05:38:48Z'
        //         .'&ENDDATE=2018-07-14T05:38:48Z'
        //         .'&VERSION=94';
                $curl = curl_init('https://api-3t.paypal.com/nvp');
                curl_setopt($curl, CURLOPT_FAILONERROR, true);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POSTFIELDS,  $info);
                curl_setopt($curl, CURLOPT_HEADER, 0);
                curl_setopt($curl, CURLOPT_POST, 1);
                $result = curl_exec($curl);
                $result = explode("&", $result);
                foreach($result as $key => $value){
                    $keyNew = explode('=', $value);
                    $data[$keyNew[0]] = urldecode($keyNew[1]);
                }
                return $data;
    }
}