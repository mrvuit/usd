<?php
/**
* 
*/
class Transactions_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Ende_model');
		$this->load->model('Wallet_model');
	}

	public function createTransaction($data) 
	{
		return $this->db->insert('transactions',$data);
	}
	public function getPaymentForSystem($system, $user_id)
	{
		$this->db->select('transactions.*')->from('transactions');
		$this->db->join('wallet', 'transactions.wallet_id = wallet.id')->join('wallet_cat', 'wallet_cat.id = wallet.wallet_id');
		$this->db->where('wallet_cat.simplename = "'.$system.'"')->where('transactions.user_id = '.$user_id);
		$this->db->order_by('transactions.id DESC')->limit(1);
		$query = $this->db->get();
		if($query->num_rows() == 0) {
			return false;
		}
		return $query->result()[0];
	}
	public function cancelTransaction($trans_id, $user_id, $timeout = 600)
	{
		$this->db->select('*')->where(['id' => $trans_id, 'user_id' => $user_id, 'status' => 1, 'type' => 2]);
		$query = $this->db->get('transactions');
		if($query->num_rows() == 0)
			return false;

		if($query->num_rows() == 1) {
			$query = $query->result_array()[0];
			if($query['time'] + $timeout < time()) {
				return false;
			}
			if($this->Wallet_model->updateMonney($user_id, $query['wallet_id'], $query['money'], '+') == true) {
				return $this->db->delete('transactions', ['id' => $trans_id]);
			}
			
		}
		return false;
	}
	public function getListTransactions($user_id = null, $type = null, $limit = null, $timeout_cancel = 600, $completed = null)
	{
		$this->db->select('*');
		if($user_id != null) {
			$this->db->where('user_id', $user_id);
		}
		if($completed != null) {
			$this->db->where('status', 2);
		}
		if($type != null) {
			$this->db->where('type', $type);
		}
		$this->db->order_by('TIME DESC');
		if($limit != null) {
			$this->db->limit($limit);
		}
		$query = $this->db->get('transactions');
		if($query->num_rows() == 0)
			return false;

		$query = $query->result_array();
		foreach ($query as $key => $value) {
			$query[$key]['time'] 		= '<i class="fa fa-clock-o"></i> '.$this->Ende_model->convertTime($value['time']);
			$query[$key]['cancel']		= $value['time'] + $timeout_cancel > time() && $value['status'] == 1 && $value['type'] == 2 ? 1 : 0; // 1 enable
			$query[$key]['status'] 		= $this->convertStatus($value['status']);
			$query[$key]['type_none'] 	= $value['type'];
			$query[$key]['type'] 		= $this->convertType($value['type']);
			$query[$key]['wallet_cat']	= $this->getWalettCatbyIdWallet($value['wallet_id']);
			
		}
		return $query;
	}
	public function getTemporary($user_id)
	{
		$out = [];
		$this->db->select('SUM(`money`) AS deposit')->where(['user_id' => $user_id, 'status' => 1, 'type' => 1]);
		$out['deposit'] = $this->db->get('transactions')->result_array()[0]['deposit'];		
		$this->db->select('SUM(`money`) AS withdraw')->where(['user_id' => $user_id, 'status' => 1, 'type' => 2]);
		$out['withdraw'] = $this->db->get('transactions')->result_array()[0]['withdraw'];
		return $out;

	}
	public function getWalettCatbyIdWallet($wallet_id)
	{
		$this->db->select('wallet_id')->where('id', $wallet_id);
		$query = $this->db->get('wallet');
		if($query->num_rows() == 0) {
			return false;
		}
		$wallet_cat_id = $query->result_array()[0]['wallet_id'];
		$this->db->select('name, simplename')->where('id', $wallet_cat_id);
		$queryWalletCat = $this->db->get('wallet_cat');
		if($queryWalletCat->num_rows() == 0) {
			return false;
		}
		return $queryWalletCat->result_array()[0];
	}
	private function convertStatus($status)
	{
		if ($status == 1)
			return '<i class="fa fa-spinner text-warning"></i> PENDING';
		if ($status == 2)
			return '<i class="fa fa-check text-success"></i> COMPLETED';
		return '<i class="fa fa-times text-danger"></i> DECLINE';
	}
	private function convertType($type)
	{
		if ($type == 1)
			return '<i class="fa fa-arrow-up text-success"></i> DEPOSIT';
		if ($type == 2)
			return '<i class="fa fa-arrow-down text-danger"></i> WITHDRAW';
		return '<i class="fa fa-arrow-up text-success"></i> INTEREST ACCRUAL ';
	}
}