<?php
/**
* 
*/
class User_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function getLocation($ip) {
		$json = file_get_contents('http://ip-api.com/json/'.$ip.'?lang=ja');
		return json_decode($json);
	}
	public function countReferrer($user_id)
	{
		$this->db->select('COUNT(id) as total')->where('ref' , $user_id);
		$query = $this->db->get('users');
		return $query->result_array()[0]['total'];
	}
	public function checkPassword($user_id, $password)
	{
		$password = md5('usd'.$password);
		$this->db->select('password')->where(['id' => $user_id, 'password' => $password]);
		$query = $this->db->get('users');
		if($query->num_rows() == 0)
			return false;
		return true;
	}
	public function updatePassword($user_id, $newPassword)
	{
		$password = md5('usd'.$newPassword);
		$this->db->set('password', $password)->where('id', $user_id);
		return $this->db->update('users');
	}
	public function deleteForgot($user_id)
	{
		return $this->db->delete('forgot_password', ['user_id' => $user_id]);
	}
	public function deleteForgotTimeOut()
	{
		return $this->db->delete('forgot_password', ['time<' => (time() - (3600 * 60 * 2))]);
	}
	public function checkForgotPassword($user_id, $hash_key)
	{
		$this->deleteForgotTimeOut();
		$this->db->select('id')->where(['user_id' => $user_id, 'time>' => (time() - (3600 * 60 * 2))]);
		if($hash_key != null) {
			$this->db->where('hash_key', $hash_key);
		}
		$query = $this->db->get('forgot_password', 1);
		if($query->num_rows() == 0) {
			return false;
		}
		return true;
	}
	public function getUserbyId($id)
	{
		$this->db->select('*')->where('id', $id);
		$query = $this->db->get('users', 1);
		if($query->num_rows() == 0) {
			return false;
		}
		return $query->result()[0];
	}
	public function getUserbyEmail($email)
	{
		$this->db->select('*')->where('email', $email);
		$query = $this->db->get('users', 1);
		if($query->num_rows() == 0) {
			return false;
		}
		return $query->result()[0];
	}
	public function sendMailResetPassword($user_id, $lng = 'en')
	{
		$this->db->select('*')->where('user_id', $user_id);
		$query = $this->db->get('forgot_password');
		if($query->num_rows() == 0) {
			return false;
		}
		$query = $query->result()[0];
		require $_SERVER["DOCUMENT_ROOT"].'/application/libraries/Mailin.php';
	    $html = '
		<div style="background: #f9f9f9;padding: 20px;">
			<div style="background: #ffffff; border: 1px #ddd;text-align: center;max-width: 800px;margin: auto;">
				<div style="background: #7376df;padding: 10px;text-align: center;font-size: 20pt; font-weight: bold;color: white;">
					Reset Password
				</div>
				<div style="padding: 10px;">
					<p>
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA3CAYAAACo29JGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gIGFR4HmDvXcgAACZFJREFUaN7NmnuMXFUZwH/fufPotLvbLl1sC9tSIHTTQqWUhoKgSBeIaYMGMEGDJL7+EDEoJo2xSIySkBijgpGmjUqiUBMTatWljSClYvyD0tryqLt0Ld12KbuLu/Sx3d153XuOf5w7M3dm53FnZtf6JTd37mTume93vsc55ztHmAFJ3PVE7mMM6ARWA9cBq4BlQAeQAKKAC0wBY8Ag0AscBt4GTgEZgGTPlqb1khmAcoDLgA3AnT7UJT5MmPYNkASGfMiXgFeAk4DXDGRDcD5UFFgLfAHYhLWQ00xn+eJhLbobeA44BGQbgawLzocS4GrgIeCzWJebLRkDngeeBv4FmHogQ/e0D9YGPAg8BdwOzJ1FMPz21wEb/efeaFd32u3fG+rlmpYLJIuVwOPAp7Eu+b+WLPBn4DGgD2onnaqWC4BtAH4F3FbrnVkUB5t9Pw70AwPRrm6qWbGiooH4ugfYBqy4QFClsgjb2SeBd6oBloULWOwebDBfeqGJSmQ+cCtwHOirBDgNLgDWjbXY/xtYTlqAm4C3qOCiqsKLK4GfAEsvNEENWQb81Nd3mhRZzrfafOBJbPJoSrQxeNogArGIQyzqIICnNZ42ICDS1CQJYLF/vVg6TORbDrjjt4AfA5FG/80YMBg6O9r4xOpl3NB1KUs72kjEo0ylswyOnuP1o0P848ggp8bGEYQmGV1gM9Yo+SFCSsBWA38CLm8czJCIR7l/w2oe3HQ9XUsX4qjp3u96mv5TH7Jt9z/Zse9tJlNZVHOEA8BnsBNwkj1brFtGu7rBWur7wB3NgLUk4vzggVv57n23sOSilrzCrqfJehoRQYmglHDxgnl0r7mchW0JXut7n1TGbcZN27HGehHQbv/eItdbB9zbTNc5juKRe9bztU3XE3EUntYcPjbC7teP0Tc4Sirr0ZqIcd2Vi9l0w1Ws6FxILOrw1U+tZXwqww+fe9XGYuNyL/Ab4DUACQzWPwe+0WirnjZsWLOcHd+5m4taE6SzHk/3HODJXfv54MwEJqCzUsIVi9t59PO38LlPXoOjhLMTKe7/0S5ePny8rBvXIb8AHgZMrpUrKExOG5J41OGLd1zLRa0JAH63720e3/F3/nN2EkcpIk7hUiK8O3yGzb98mT2v/xuABS1zeKD7o8SjDeexnGz0efLj3G3A8kZb09qwfNECbr7aDovDpyfY2nOQqXTlJOEoYWx8ip/9YT+nzycBuGllJ50dbWjTlGsu93lQ2Bn+HVQe0GvDGUNX50IWLZgHwIGjQ/Sf+rCmezmO4vC7Ixw4OgTAR9rnsXzRAnRzcad8nqjCjvJrm2kN4NKFrUQjdk5wbOg0qaxX8x0BptJZegdHATvQt82LN6sKPs8yBVwDLGm2tTmxQqxkXA9bGgkhxpAOdETT8xUrS4BrFLCGGVhRx2MNLvNmiKZE5gJrItgFYFN/YRBEnMAzaCOoEM1qI0U29oygmycWYFUEG3NNgV2WOMu6id/D/r+Bgdsnj9G6oj/UbMMYw/rJo7D/FcTT3MkAB+csYjjbjoR17fKyTBJ3PXEMuLIxMGhxMmzrep67Lz6C1vZ7pUDV0fnaUPTu8yMr+fo7dzMprc0AvquwxdOGxBjhkvg4N84/6WtmLw24JvylA+8icPP8AZZl+iA90ahqAIncONcYHOCgUZhw/RviR8ZYq0e9KZzx95D0eKPqRRW2ZNaQ5Dyvps4GiCSgZbG9h4EUB7SLc34YSZ4N1zPFklXYOv3siQHpWIWz8Vmc+17F2fgs0rGqpq5GBIODMQY1NYZMnYH6pmXJCLZk3VBCCSVOFLV+C3KVv5pqX4FyU3h/+RLoak6jQBSITSmSOgcYTKLdfl9bRhW2/jd7Ep0L7SUlz/YV9vsqYkQwojBSgCQzCckzoGtP7YBBhd0fa2pAqSqZCczIgWLFRw5ApkomFHygwpUDFTcNqXOg3ap9A/Qq4A3sZuDsiPYwb26HyQ/s8+QH9rlq7wt+acy6ZQ4sB6tdSJ8Hr6JbTwFvKOAIMDxrcIBJnwUvZR+8lH2u9U7OWgRdM/DZGMgmKwEOA0cUdqPv0GzCNSRFMIJIMSRiR1fjpcsBHsKPuSzwV/yJwswrCWTGMecGAOw9Mx5iqi55GAtWcFPrquJnTcFoNxiD2ufJ5hZh+4AT+LWHGZf0WfTeh5AlN2KGX4OQbgkK8WOuMDRMtyAiYDQYD8Q54fPkS3vHgT00Uf2qqexYL2a0N58rqv4WfAv5FkNNy575CykkGgOg9+DEjqNdlF96NsAOYLQuhf17qDqqr2OYpZog0yFUGavlwfLZdRTYgZcxyZ4tRUWhg8DOeuBEYFLHmPTidS1xqokjcN6Ncd6L2xl0Ls4QDIXPxWB5wJ0gB3M9GIRzga3YmnsoURjeT81n+6n1jKQSpD1V+dJOqGsoNZftJ65lKNVmy4KlcUYuyUjedX2wAYStCC5+1a3pXR4DRESzKnqCzsxRlJfxxyfJ34Oxk8tyhoJihgLASLqFo5MduDj5WCpNIBKEsu7pImzG6CcRRbLn0WK4AOB84NfUuW/gGUGlzxMdP4lkJjHiFA3EVkknr2jx3SkorhRKlVpMAmCqBAwQ2Ql8BTiXA4OSzUe3fy/Rru40dhvoFuymXjgXFZBIHDOnDWU8HC+NKEGUr7AoRNndHXvPgdjvHUf835dxxSKwIlcEkTexB35OBcFKYy4ofcC3sbOXOsRgnDlkF1yG27oEoyLW/VBlLCb5Z8lZt1rKD8ZYYe456OvZVy4NTys2+tYDm1gGsacGWupiFIWJtYATQ7y0LUKUdUln+rSqUspXhc8+6AjwMKT2QLTsgZuyldQA4DvYAf5j2FishxATSWCiCTv3M7ootvIWy8ecgHIKyUYFM2OunJa34HvAw4jaBZGKJ4kqlokDgH3Am9ijhovqAwScGCY6F4xGtDd9+VJm3VYMJsWuibyF8CDx+B5cl9I4CwVXAjgAvIpNMFfVem+aKMdfefuT3CKrqSoJpCjlZ4E/IvIQ2hzA6Kpg1ndCSuDU3peBb9Lofl5mCslM5OOwPJiUpvwTwFMgzwDjyRceDfVXoS0QGCb2Ay9jM+1y6t1EcaL2Mv4KS5zC8mX6WDYG8ltEHkGcHjDpsGBwIU/Kag/cJEZ7pWAakZOI2g3yHKIOgckmX/he3Xpe2DPOxoCXMUa7SUQNgRwWUS8hss+uy4yX3P1Yw/rNyFy+wun0NdjjwrnT6XOx89Xg6fSTYHqNm34DY46IqPeM0ZlIJMHEC82fTv8vWTymMrMORoMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMDItMDZUMjE6MzA6MDctMDU6MDBlJUBSAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTAyLTA2VDIxOjMwOjA3LTA1OjAwFHj47gAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=">
					</p>
					<p style="font-size: 16pt;">You have requested a password reset, please click on the link below to retrieve your password.</p>
					<p style="color: #777;">* Note: The password recovery link can only be used once, the link is valid for 2 hours.</p>
					<p style="margin-top: 30px;">
						<a href="https://myperday.com/'.$lng.'/forgot-password?user_id='.$query->user_id.'&hash_key='.$query->hash_key.'" style="font-size: .9rem;padding: 1rem 1.5rem;border-radius: 499rem;text-transform: uppercase;letter-spacing: .1rem;font-size: .8rem;padding: .75rem 1.25rem;cursor: pointer;background-color: #7376df !important;border-color: #7376df !important;color: white;text-decoration: none;">Reset Password</a>
					</p>
					<div style="background: #ddd;padding: 1px;margin: 10px;margin-top: 25px;"></div>
					<div style="font-size: 11pt;">
						<p>© 2018 <a href="https://myperday.com">MyPerDay.Com</a></p>
						<p>This email can not receive replies, please do not reply to this email.</p>
						<p>Thank you for choosing us.</p>
					</div>
				</div>
			</div>
		</div> 
		';

        $mailin = new Mailin('dsdviet@gmail.com', 'zBPRqJ18t3d9cHhx');
        $mailin->addTo($query->email, '')->setFrom('no-reply@myperday.com', 'Support Team MyPerDay')->setReplyTo('admin@myperday.com', 'myperday.com')->setSubject('Forgot password')->setText('Hello')->setHtml($html);
        
        return $mailin->send();
	}

}