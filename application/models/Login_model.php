<?php
/**
* 
*/
class Login_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function checkLogin($email, $password) {
		$this->db->select('*')->where(array(
			'email' => $email,
			'password' => $password)
		);
		$query = $this->db->get('users');
		if($query->num_rows() == 1)
			return $query->result_array()[0];
		return false;
	}
}