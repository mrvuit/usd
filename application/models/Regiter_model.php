<?php
/**
* 
*/
class Regiter_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function checkExists($email) {
		$this->db->select('id')->where('email', $email);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
			return true;
		return false;
	}

	public function createUser($data) {
		return $this->db->insert('users', $data);
	}
}