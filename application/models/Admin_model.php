<?php
/**
* 
*/
class Admin_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Ende_model');
	}
	public function updateSetting($data)
	{
		foreach ($data as $key => $value) {
			$this->db->update('system', ['value' => $value], ['key' => $key]);
		}
		return true;
	}
	public function updateWalletCat($data)
	{
		foreach ($data as $key => $value) {
			$this->db->update('wallet_cat', ['amount' => $value], ['simplename' => $key]);
		}
		return true;
	}
	public function getWalletCat()
	{
		return $this->db->select('*')->get('wallet_cat')->result();
	}
	public function getSetting()
	{
		$this->db->select('*');
		$query = $this->db->get('system');
		if($query->num_rows() == 0)
			return false;
		return $query->result();
	}
	public function getListContact($start = 0, $limit = 10)
	{
		$this->db->select('*')->order_by('time DESC');
		$query = $this->db->get('suport', $limit, $start);
		if($query->num_rows() == 0) {
			return false;
		}
		$query = $query->result();
		foreach ($query as $key => $item) {
			$query[$key]->time = $this->Ende_model->convertTime($item->time);
		}
		return $query;
	}

	public function getStatistics()
	{
		$out = [];
		$out['count_trans'] = $this->db->select('COUNT(`id`) AS count')->get('transactions')->result_array()[0]['count'];
		$out['count_suport'] = $this->db->select('COUNT(`id`) AS count')->get('suport')->result_array()[0]['count'];
		return $out;
	}

	public function getWallet($wallet_id)
	{
		$out = [];
		$query	 = $this->db->select('*')->where('id', $wallet_id)->get('wallet');
		if($query->num_rows() == 0) {
			return false;
		}
		$out['user_wallet'] = $query->result_array()[0];
		$out['wallet_cat']	= $this->db->select('name, simplename')->where('id', $out['user_wallet']['wallet_id'])->get('wallet_cat')->result_array()[0];
		return $out;

	}
	public function getUser($user_id)
	{
		$this->db->select('id, email, reg_date')->where('id', $user_id);
		$query = $this->db->get('users');
		if($query->num_rows() == 0)
			return false;
		$query = $query->result_array()[0];
		$query['email-f'] = explode('@', $query['email'])[0];
		$query['reg_date'] = $this->Ende_model->convertTime($query['reg_date']);
		return $query;
	}
	public function getListTransactions($type, $status, $sort, $user)
	{
		$this->db->select('*');
		if($type != 0 && $type != '') {
			$this->db->where('type', $type);
		}
		if($status != 0 && $status != '') {
			$this->db->where('status', $status);
		}
		if($sort != '') {
			$this->db->order_by(str_replace('-', ' ', $sort));
		} else {
			$this->db->order_by('time desc');
		}
		if($user != '') {
			$this->db->where('user_id', $user);
		}

		$query = $this->db->get('transactions');

		if($query->num_rows() == 0)
			return ['query' => false, 'amount' => 0];
		$query = $query->result_array();
		$amount = 0;
		foreach ($query as $key => $value) {
			$query[$key]['time'] 	= '<i class="fa fa-clock-o"></i> '.$this->Ende_model->convertTime($value['time']);
			$query[$key]['updated'] = '<i class="fa fa-clock-o"></i> '.($value['updated'] == 0 ? 0 : $this->Ende_model->convertTime($value['updated']));
			$query[$key]['status'] 	= $this->convertStatus($value['status']);
			$query[$key]['type-t'] 	= $this->convertType($value['type']);
			$query[$key]['user'] 	= $this->getUser($value['user_id']);
			$query[$key]['wallet'] 	= $this->getWallet($value['wallet_id']);
			if($value['type'] != 4)
			$amount 				+= $value['money'];
		}
		return ['query' => $query, 'amount' => $amount];
	}
	public function getTransaction($transaction_id)
	{
		$this->db->select('*')->where('id', $transaction_id);
		$query = $this->db->get('transactions');
		if($query->num_rows() == 0)
			return false;
		return $query->result_array()[0];
	}
	public function updateMonney($user_id ,$usd, $type = '-')
	{
		if ($type == '+') {
			return $this->db->where('id', $user_id)
			->set('money', '`money` + '.$usd.'', FALSE)
			->update('users');
		}
			return $this->db->where('id', $user_id)
			->set('money', '`money` - '.$usd.'', FALSE)
			->update('users');
	}
	public function deleteTransaction($transaction_id)
	{
		$this->db->select('*')->where('id', $transaction_id);
		$query = $this->db->get('transactions');
		if($query->num_rows() == 0)
			return false;
		$query = $query->result_array()[0];

		if($query['type'] == 4) {
			if($this->updateStatusTransaction($query['decline'], 1) == true) {
				$this->db->delete('transactions', array('id' => $query['id']));
				return $this->updateMonney($query['user_id'], $query['money'], '-');
			}
		}
		return false;
	}
	public function updateStatusTransaction($transaction_id, $status, $return = false)
	{
		if($status == 3 && $return == true) {
			$this->db->select('*')->where('decline', $transaction_id);
			$check = $this->db->get('transactions');
			if($check->num_rows() == 0) {
				$dataTransaction = $this->getTransaction($transaction_id);
				if($dataTransaction['decline'] == 0 && $dataTransaction['type'] == 2) {
					$data = [
						'decline' 	=> $transaction_id,
						'user_id'	=> $dataTransaction['user_id'],
						'money'		=> $dataTransaction['money'],
						'wallet'	=> 'system',
						'status'	=> 2,
						'time'		=> time(),
						'comment'	=> strtoupper('Returns the hold from transaction #'.$transaction_id),
						'type'		=> 4
					];
					if($this->db->insert('transactions',$data) == true) {
						$this->updateMonney($dataTransaction['user_id'], $dataTransaction['money'], '+');
					}
				}
			}
		}
		return $this->db->update('transactions', array('status' => $status, 'updated' => time()), array('id' => $transaction_id));
	}
	private function convertStatus($status)
	{
		if ($status == 1)
			return '<i class="fa fa-spinner text-warning"></i> PENDING';
		if ($status == 2)
			return '<i class="fa fa-check text-success"></i> COMPLETED';
		return '<i class="fa fa-times text-danger"></i> DECLINE';
	}
	private function convertType($type)
	{
		if ($type == 1)
			return '<i class="fa fa-arrow-up text-success"></i> DEPOSIT';
		if ($type == 2)
			return '<i class="fa fa-arrow-down text-danger"></i> WITHDRAW';
		if ($type == 3)
			return '<i class="fa fa-arrow-up text-success"></i> PROFIT';

		return '<i class="fa fa-arrow-up text-success"></i> SYSTEM ';
	}

}