<?php
class Base extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->library('session');
		$this->load->helper('security');
		$this->load->library('user_agent');
		$this->load->model('Core_model');
		$this->load->model('User_model');
		$this->load->model('Wallet_model');
		$this->load->model('Ende_model');
		$this->load->model('Transactions_model');
		$this->Data = array();

		//LANGUAGE
		$this->Data['lng'] = $this->Ende_model->checkLanguage($this->uri->segment(1));

		//CHECK LOGIN
		if(get_cookie('uid') !== NULL) {
			$cookie_user = explode('$U$S$D', get_cookie('uid'));
			if(count($cookie_user) != 2 || empty($cookie_user[0]) || empty($cookie_user[1])) {
				delete_cookie('uid');
				redirect(base_url($this->Data['lng'].'/home'));
			}
			$this->Data['user'] = $this->Core_model->getUser($cookie_user);
			if($this->Data['user'] == false) {
				delete_cookie('uid');
				redirect(base_url($this->Data['lng'].'/home'));
			}
		}
		//SYSTEM DATA
		$this->Data['system'] 		= $this->Core_model->getSystemData();
		if($this->Data['system']['time_update_cmc'] + 600 < time()) {
			$this->Core_model->getBtcEth();
		}
		$fakeUser = round((time() - 1523687368) / 100) + 25141;
		$this->Data['all_user']		= $this->Core_model->countAllUser() + $fakeUser;
		$this->Data['trans_all'] 	= $this->Transactions_model->getListTransactions(null, null, 5, null, 1);
		//ADMIN DATA
		if(get_cookie('uid') !== NULL && $this->Data['user']['id'] == ADMIN) {
			$this->load->model('Admin_model');
			$this->Data['admin_data']	= $this->Admin_model->getStatistics();
		}

		$this->lang->load('lng', $this->Data['lng']);
		// $this->dump($this->lang->line('error_email_missing'));
	}
	private function checkLogin()
	{
		if(!isset($this->Data['user'])) {
			redirect(base_url($this->Data['lng'].'/home'));
		}
	}
	//ADMIN
	private function checkAdmin()
	{
		if(!isset($this->Data['user'])) {
			redirect(base_url($this->Data['lng'].'/home'));
		}
		if($this->Data['user']['id'] != ADMIN) {
			redirect(base_url($this->Data['lng'].'/home'));
		}
		return true;
	}
	public function support_admin($start = 0)
	{
		$this->checkAdmin();
		$this->Data['contact_list'] = $this->Admin_model->getListContact($start);
		$this->Data['title'] 		= 'Contact List || ADMIN';
		$this->Data['icon'] 		= 'fa fa-envelope-o';
		$this->Data['menu_right'] 	= false;
		$this->load->view('admin/support_list_admin_view', $this->Data);
	}
	public function transactions_admin()
	{
		$this->checkAdmin();
		$trans_id 		= $this->input->get('trans_id');
		$delete 		= $this->input->get('delete');
		$return 		= ($this->input->get('return') == 1 ? true : false);
		$change_status 	= $this->input->get('change_status');
		$status 		= $this->input->get('status');
		$sort 			= $this->input->get('sort');
		$type 			= $this->input->get('type');
		$user 			= $this->input->get('user');

		if(!empty($trans_id) && !empty($change_status)) {
			$this->Admin_model->updateStatusTransaction($trans_id, $change_status, $return);
			redirect($this->agent->referrer());
		}

		if(!empty($trans_id) && !empty($delete) && $delete == 'true') {
			$this->Admin_model->deleteTransaction($trans_id);
			redirect($this->agent->referrer());
		}


		$data_transactions 					= $this->Admin_model->getListTransactions($type, $status, $sort, $user);
		if(!empty($user)) {
			$this->Data['data_user']		= $this->Admin_model->getUser($user);
		}
		// $this->dump($data_transactions);
		$this->Data['list_transactions'] 	= $data_transactions['query'];
		$this->Data['all_amount'] 			= $data_transactions['amount'];
		$this->Data['title'] 				= 'Transactions History || ADMIN';
		$this->Data['menu_right'] 			= false;
		$this->Data['icon'] 				= 'fa fa-list';
		$this->load->view('admin/transaction_admin_view', $this->Data);
	}
	public function setting_admin()
	{
		$this->checkAdmin();
		if(isset($_POST['submit'])) {
			$data = $this->input->post();
			unset($data['submit']);
			$this->Admin_model->updateSetting($data);
			redirect(base_url($this->Data['lng'].'/setting-admin'));
		}
		if(isset($_POST['sub_wallet_cat'])) {
			$data = $this->input->post();
			unset($data['sub_wallet_cat']);
			$this->Admin_model->updateWalletCat($data);
			redirect(base_url($this->Data['lng'].'/setting-admin'));
		}

		$this->Data['wallet_cat']	= $this->Admin_model->getWalletCat();
		$this->Data['data_setting']	= $this->Admin_model->getSetting();
		$this->Data['menu_right'] 	= false;
		$this->Data['title'] 		= 'Setting || ADMIN';
		$this->Data['icon'] 		= 'fa fa-cogs';
		$this->load->view('admin/setting_admin_view', $this->Data);
	}
	public function dump($var)
	{
		echo '<pre>';
			print_r($var);
		echo '</pre>';
		exit();
	}
	public function test() {
		$this->Wallet_model->interestIncrease();
	}

	//MEMBER
	public function index()
	{
		if($this->Data['system']['time_interest'] < $this->Ende_model->getTimeInterest()) {
			$this->Wallet_model->interestIncrease($this->Ende_model->getTimeInterest());
		}
		if(isset($_GET['ref']) && is_numeric($_GET['ref'])) {
			$timeout = time() + (3600 * 24 * 30);
			setcookie('ref', $_GET['ref'], (int)$timeout, '/');
		}

		if(get_cookie('uid') === NULL) {
			$this->Data['title'] 		= $this->lang->line('home');
			$this->Data['icon'] 		= 'fa fa-dashboard';
			$this->load->view('index_view', $this->Data);
		} else {
			$this->Data['user_wallet']	= $this->Wallet_model->getUserWallet($this->Data['user']['id']);
			$this->Data['wallet_cat']	= $this->Wallet_model->getWalletCat();
			$this->Data['amount']		= $this->Wallet_model->getAmount($this->Data['user']['id']);
			$this->Data['interest']		= $this->Wallet_model->calcuInterest($this->Data['amount']);
			$this->Data['referrer']		= $this->User_model->countReferrer($this->Data['user']['id']);
			$this->Data['countd']		= $this->Ende_model->getTimeInterest();
			$this->Data['countd_view']	= $this->Ende_model->convertTimstamp($this->Data['countd']);
			$this->Data['temporary']	= $this->Transactions_model->getTemporary($this->Data['user']['id']);
			$this->Data['title'] 		= $this->lang->line('dashboard');
			$this->Data['icon'] 		= 'fa fa-dashboard';
			$this->load->view('dashboard_view', $this->Data);
		}

	}
	public function error()
	{
		$this->Data['menu_right'] 	= false;
		$this->Data['title'] 	= $this->lang->line('404_title');
		$this->load->view('error_view', $this->Data);
	}
	public function account()
	{
		$this->checkLogin();
		if(isset($_POST['submit'])) {
			$old_password 		= $this->input->post('oldPassword');
			$password 			= $this->input->post('newPassword');
			$repeat_password 	= $this->input->post('repeatPassword');

			$error = false;
			if(!preg_match('/^([a-zA-Z0-9_!@#$%^&*]{6,32})+$/', $password)) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('allow_password'));
			}
			if($this->User_model->checkPassword($this->Data['user']['id'], $old_password) == false) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('incorrect_password'));
			}
			if($password != $repeat_password) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('same_password'));
			}
			if($error == false) {
				$this->User_model->updatePassword($this->Data['user']['id'], $password);
			}
			redirect(base_url($this->Data['lng'].'/account'));

		}


		$this->Data['title'] 		= $this->lang->line('myAccount');
		$this->Data['icon'] 		= 'fa fa-user';
		$this->Data['menu_right'] 	= false;
		$this->load->view('account_view', $this->Data);
	}
	public function wallet()
	{
		$this->checkLogin();
		$this->Data['wallet_cat']	= $this->Wallet_model->getWalletCat();

		if(isset($_POST['submit'])) {
			$reCaptcha 	= $this->input->post('g-recaptcha-response');
			$checkReCaptcha = $this->Ende_model->reCaptcha($reCaptcha);
			$post_wallet = [];

			foreach ($this->Data['wallet_cat'] as $item) {
				$post_wallet[$item['id']] = $this->input->post($item['id']);
			}

			$error = false;
			$validWallet = $this->Wallet_model->validAddressWallet($post_wallet);
			$icon_err = '<i class="fa fa-exclamation-circle"></i>';
			if(count($validWallet) > 0) {
				$error = true;
				foreach ($validWallet as $key => $item) {
					$this->session->set_flashdata($key, $icon_err. ' '.$this->lang->line('address_wallet_invalid').' ('.$item['regex'].'). ');
				}
			}
			if($checkReCaptcha == false) {
				$error = true;
				$this->session->set_flashdata('captcha', $icon_err. ' '. $this->lang->line('wrong_captcha'));
			}
			if($error == false) {
				if($this->Wallet_model->updateWallet($this->Data['user']['id'], $post_wallet) == true) {
					$this->session->set_flashdata('status', $this->lang->line('update_wallet_success'));
				}
			}

			redirect(base_url($this->Data['lng'].'/wallet'), 'refresh'); 
		}

		$this->Wallet_model->checkWallet($this->Data['user']['id']);
		$this->Data['user_wallet']	= $this->Wallet_model->getUserWallet($this->Data['user']['id']);
		// $this->dump($this->Data['user_wallet']);
		$this->Data['title'] 		= $this->lang->line('myWallet');
		$this->Data['icon'] 		= 'fa fa-server';
		$this->Data['menu_right'] 	= false;
		$this->load->view('wallet_view', $this->Data);
	}

	public function deposit()
	{
		$this->checkLogin();
		$this->Data['title']		= $this->lang->line('deposit');
		$this->Data['icon'] 		= 'fa fa-arrow-circle-o-up';
		$this->Data['menu_right'] 	= false;
		$this->Data['available'] 	= $this->Wallet_model->checkAvailable($this->Data['user']['id']);
		$this->Data['user_wallet'] 	= $this->Wallet_model->getUserWallet($this->Data['user']['id']);
		$this->Data['deposit_htr']	= $this->Transactions_model->getListTransactions($this->Data['user']['id'],1);	
		$this->Data['wallet_cat'] 	= $this->Wallet_model->getWalletCat();
		$this->load->view('deposit_view', $this->Data);
	}

	public function withdraw()
	{
		$this->checkLogin();
		if(isset($_POST['submit'])) {
			$wallet = $this->input->post('wallet');
			$amount = $this->input->post('amount');
			$error = false;
			$userWallet 	= $this->Wallet_model->getUserWallet($this->Data['user']['id'], $wallet);
			$walletCat 		= $this->Wallet_model->getWalletCatbyID($wallet);

			if(count($userWallet) == 0) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('invalid_wallet'));
			} else {
				$userWallet = $userWallet[0];
			}
			if($walletCat == false) {
				$error == true;
				$this->session->set_flashdata('error', $this->lang->line('invalid_wallet'));
			} else {
				$limitWithdraw = explode('-', $walletCat['withdraw']);
				if($amount < $limitWithdraw[0]) {
					$error = true;
					$this->session->set_flashdata('error', $this->lang->line('invalid_amount'));
				}
			}
			if(empty($wallet) || !is_numeric($wallet)) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('invalid_wallet'));
			}
			if(!is_numeric($amount) || empty($amount) || $amount < 0.1) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('invalid_amount'));
			}
			if($error == false && $amount > $userWallet['money']) {
				$error = true;
				$this->session->set_flashdata('error', $this->lang->line('not_enough_withdraw'));
			}

			if($error == false) {
				$comment = "WITHDRAW - $".$amount." - ".$walletCat['name']."($ ".($userWallet['money'] - $amount).") - ".$userWallet['address']." - ".date("Y/m/d h:m:i");
				$data = [
					'user_id'	=> $this->Data['user']['id'],
					'money'		=> $amount,
					'wallet_id'	=> $userWallet['id'],
					'status'	=> 1,
					'time'		=> time(),
					'updated'	=> time(),
					'comment'	=> $comment,
					'type'		=> 2
				];
				if($this->Transactions_model->createTransaction($data) == true) {
					if($this->Wallet_model->updateMonney($this->Data['user']['id'], $userWallet['id'], $amount, '-') == true) {
						$this->session->set_flashdata('success', $this->lang->line('success_withdraw'));
					}
				}
			}
			redirect(base_url($this->Data['lng'].'/withdraw'), 'refresh'); 
		}

		$this->Data['title'] 		= $this->lang->line('withdraw');
		$this->Data['icon'] 		= 'fa fa-arrow-circle-o-down';
		$this->Data['menu_right'] 	= false;
		$this->Data['available'] 	= $this->Wallet_model->checkAvailable($this->Data['user']['id']);
		$this->Data['user_wallet'] 	= $this->Wallet_model->getUserWallet($this->Data['user']['id']);
		$this->Data['withdraw_htr']	= $this->Transactions_model->getListTransactions($this->Data['user']['id'], 2);
		$this->Data['wallet_cat'] 	= $this->Wallet_model->getWalletCat();
		$this->load->view('withdraw_view', $this->Data);
	}
	public function transactions()
	{
		$this->checkLogin();
		$this->load->model('Transactions_model');

		if(isset($_GET['cancel_id'])) {
			$cancel_id = $this->input->get('cancel_id');
			$this->Transactions_model->cancelTransaction($cancel_id, $this->Data['user']['id'], $this->Data['system']['time_out_cancel']);
			redirect(base_url($this->Data['lng'].'/transactions'));
		}

		$this->Data['list_transactions'] 	= $this->Transactions_model->getListTransactions($this->Data['user']['id'], null, null, $this->Data['system']['time_out_cancel']);
		$this->Data['title'] 				= $this->lang->line('history');
		$this->Data['icon'] 				= 'fa fa-list';
		$this->Data['menu_right'] 			= false;
		$this->load->view('transaction_view', $this->Data);
	}
	public function referrals()
	{
		$this->checkLogin();
		$this->load->model('Referreals_model');
		$this->Data['referrals_list'] 	= $this->Referreals_model->getReferralsList($this->Data['user']['id']);
		$this->Data['title'] 			= $this->lang->line('myReferrals');
		$this->Data['icon'] 			= 'fa fa-users';
		$this->Data['menu_right'] 		= false;
		$this->load->view('referrals_view', $this->Data);
	}

	public function register()
	{
		$this->Data['title'] 		= $this->lang->line('register');
		$this->Data['icon'] 		= 'fa fa-registered';
		$this->Data['menu_right'] 	= false;
		if(get_cookie('uid') !== NULL) {
			redirect(base_url($this->Data['lng'].'/home'));
		}
		$this->load->view('register_view', $this->Data);
	}
	public function login()
	{
		if(get_cookie('uid') !== NULL) {
			redirect(base_url($this->Data['lng'].'/home'));
		}
		$this->Data['title'] 		= $this->lang->line('login');
		$this->Data['icon'] 		= 'fa fa-sign-in';
		$this->Data['menu_right'] 	= false;
		$this->load->view('login_view', $this->Data);
	}
	public function forgot_password()
	{
		if(get_cookie('uid') !== NULL) {
			redirect(base_url($this->Data['lng'].'/home'));
		}
		$user_id = $this->input->get('user_id');
		$hash_key = $this->input->get('hash_key');
		$flagResetPassword = false;

		if($user_id != '' && $hash_key != '') {
			$flagResetPassword = $this->User_model->checkForgotPassword($user_id, $hash_key);
		}
		$view = 'forgot_password_view';
		if($flagResetPassword == true) {
			$view = 'reset_password_view';
			$this->Data['dataUser'] = $this->User_model->getUserbyId($user_id);

			if(isset($_POST['submitReset'])) {
				$password 				= $this->input->post('newPassword');
				$repeat_password 		= $this->input->post('repeatPassword');
				$error = false;

				if(!preg_match('/^([a-zA-Z0-9_!@#$%^&*]{6,32})+$/', $password)) {
					$error = true;
					$this->session->set_flashdata('error', $this->lang->line('allow_password'));
				}
				if($password != $repeat_password) {
					$error = true;
					$this->session->set_flashdata('error', $this->lang->line('same_password'));
				}
				if($error == false) {
					$this->User_model->deleteForgot($user_id);
					$this->User_model->updatePassword($user_id, $password);
					$this->session->set_flashdata('status', $this->lang->line('reset_pw_success'));
				}
				redirect(base_url($this->Data['lng'].'/forgot-password?user_id='.$user_id.'&hash_key='.$hash_key), 'refresh'); 
			}
		}

		if(isset($_POST['submit'])) {
			$reCaptcha 	= $this->input->post('g-recaptcha-response');
			$email		= $this->input->post('email');
			$checkReCaptcha = $this->Ende_model->reCaptcha($reCaptcha);

			$error = false;
			if($checkReCaptcha == false) {
				$error = true;
				$this->session->set_flashdata('captcha', $this->lang->line('wrong_captcha'));
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$error = true;
				$this->session->set_flashdata('email', $this->lang->line('wrong_email'));
			}
			if($error == false) {
				$dataUser 	= $this->User_model->getUserbyEmail($email);
				if($dataUser == false) {
					$error = true;
					$this->session->set_flashdata('email', $this->lang->line('forgot_wrong_email'));
				}
			}

			if($error == false) { 
				$flagResetPassword = $this->User_model->checkForgotPassword($dataUser->id, null);
				if ($flagResetPassword == true) {
					$this->session->set_flashdata('status', $this->lang->line('forgot_success'));
					$this->User_model->sendMailResetPassword($dataUser->id, $this->Data['lng']);
				} else {
					$hash = md5($email.time().'!@#$%^MPD'.$dataUser->id);
					$data = array(
						'user_id'	=> $dataUser->id,
						'hash_key'	=> $hash,
						'time'		=> time()
					);
					if($this->db->insert('forgot_password', $data) == true) {
						$this->session->set_flashdata('status', $this->lang->line('forgot_success'));
						$this->User_model->sendMailResetPassword($dataUser->id, $this->Data['lng']);
					}
				}
			}
			redirect(base_url($this->Data['lng'].'/forgot-password'), 'refresh'); 
		}

		$this->Data['title'] 		= $this->lang->line('forgot_password');
		$this->Data['icon'] 		= 'fa fa-question-circle';
		$this->Data['menu_right'] 	= false;
		$this->load->view($view, $this->Data);
	}
	public function logout() 
	{
		delete_cookie('uid');
		redirect(base_url('/'.$this->Data['lng'].'/home'));
	}
	public function agreement()
	{
		$this->Data['title'] 		= $this->lang->line('agreement');
		$this->Data['icon'] 		= 'fa fa-book';
		$this->load->view('agreement_view', $this->Data);
	}
	public function support()
	{
		
		if(isset($_POST['submit'])) {
			$reCaptcha 	= $this->input->post('g-recaptcha-response');
			$email		= $this->input->post('email');
			$message	= $this->input->post('message');
			$checkReCaptcha = $this->Ende_model->reCaptcha($reCaptcha);
			$error = false;

			if($checkReCaptcha == false) {
				$error = true;
				$this->session->set_flashdata('captcha', $this->lang->line('wrong_captcha'));
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$error = true;
				$this->session->set_flashdata('email', $this->lang->line('wrong_email'));
			}
			if (empty($message)) {
				$error = true;
				$this->session->set_flashdata('message', $this->lang->line('message_invalid'));
			}

			if($error == false) { 
				$this->session->set_flashdata('status', $this->lang->line('support_success'));
				$data = array(
					'email'		=> $email,
					'content'	=> $message, 
					'user_id'	=> (isset($this->Data['user']['id']) ? $this->Data['user']['id'] : 0),
					'time'		=> time()
				);
				$this->db->insert('suport', $data);
			}
			redirect(base_url($this->Data['lng'].'/support'), 'refresh'); 
		}

		$this->Data['title'] 		= $this->lang->line('contactus');
		$this->Data['icon'] 		= 'fa fa-envelope-o';
		$this->load->view('support_view', $this->Data);
	}

	public function ratesnlimits()
	{
		$this->Data['title'] 		= $this->lang->line('ratesnlimits');
		$this->Data['icon'] 		= 'fa fa-table';
		$this->Data['wallet_cat']	= $this->Wallet_model->getWalletCat();
		$this->load->view('ratesnlimits_view', $this->Data);
	}

	//PAYMENT
	public function payment($system)
	{
		if($system == 'bitcoin' || $system == 'ethereum') {
			redirect(base_url($this->Data['lng'].'/transactions'));
		}
		$this->Data['last_paymnet'] 	= $this->Transactions_model->getPaymentForSystem($system, $this->Data['user']['id']);
		$this->Data['system_payment'] 	= $system;
		$this->load->view('payment/'.$system, $this->Data);
	}
	public function status_payment()
	{
		
	}
	public function paypal()
	{
		if(isset($_GET['tx']) && isset($_GET['st']) && $_GET['st'] == 'Completed') {
			$dataTs =  $this->Ende_model->paypalTransaction($_GET['tx']);
			$mailPaypal = 'longvu2003@gmail.com';
			$minUsd = 5;
			if($dataTs['ACK'] == 'Failure') {
				echo 'sai';
			}			
			if($dataTs['ACK'] == 'Success' && $dataTs['RECEIVEREMAIL'] == $mailPaypal && $dataTs['AMT'] >= $minUsd) {
				//CHECK DATABASE TRANSACTIONID
				$this->User_model->updateMonney($this->Data['user']['id'], $dataTs['AMT'], '+');


			}
		}
	}
}