<?php
/**
* 
*/
class Ajax extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->model('Ende_model');
		$this->load->database();
		
	}
	public function saveTransaction() 
	{
		$this->load->model('Wallet_model');
		$user_id 	= $this->input->post('user_id');
		if($this->db->select('id')->where('id', $user_id)->get('users')->num_rows() == 0) {
			exit(json_encode(['status' => 'false', 'messages' => 'User not exist.']));
		}
		$money 		= $this->input->post('money');
		if(!is_numeric($money) || $money == 0 || empty($money)) {
			exit(json_encode(['status' => 'false', 'messages' => 'Amount invalid.']));
		}
		$wallet_id 	= $this->input->post('wallet');
		$infoWallet = $this->Wallet_model->getFullInfoWallet($wallet_id, $user_id);
		if($infoWallet == false) {
			exit(json_encode(['status' => 'false', 'messages' => 'Wallet invalid.']));
		}
		$type 		= $this->input->post('type');
		if($type != 1 && $type != 2) {
			exit(json_encode(['status' => 'false', 'messages' => 'Type invalid.']));
		}
		if($type == 1) {
			$comment = 'DEPOSIT - '.$money.' - '.$infoWallet['wallet_cat']['name'].'('.$infoWallet['wallet_user']['money'].') - '.$infoWallet['wallet_user']['address'].' - '.date("Y/m/d h:m:i");
		}
		$data = [
			'user_id' 	=> $user_id,
			'money'		=> $money,
			'wallet_id'	=> $infoWallet['wallet_user']['id'],
			'time'		=> time(),
			'comment'	=> $comment,
			'type'		=> $type,
			'status'	=> 1
		];
		if($this->db->insert('transactions', $data) == true) {
			exit(json_encode(['status' => 'true', 'messages' => 'Add transactions susccess.', 'system' => $infoWallet['wallet_cat']['simplename']]));
		}
		exit(json_encode(['status' => 'false', 'messages' => 'Can not add transactions.']));
		
	}
	public function getWalletCat($wallet_cat_id, $user_id)
	{
		$json = [];
		if(!is_numeric($user_id)) {
			exit (json_encode(['status' => 'false']));
		}
		$this->db->select('*')->where(['wallet_id' => $wallet_cat_id, 'user_id' => $user_id]);
		$queryWallet = $this->db->get('wallet');
		if($queryWallet->num_rows() == 0) {
			exit (json_encode(['status' => 'false']));
		} else {
			$json['wallet'] 	= $queryWallet->result_array()[0];
		}
		$this->db->select('*')->where('id', $wallet_cat_id);
		$queryWalletCat  		= $this->db->get('wallet_cat');
		if($queryWalletCat->num_rows() == 0) {
			exit (json_encode(['status' => 'false']));
		} else {
			$json['wallet_cat'] = $queryWalletCat->result_array()[0];
			$json['status'] 	= 'true';
			exit (json_encode($json));
		}

	}
	public function ajaxLogin() {
		$this->load->model('Login_model');
		$this->load->model('Wallet_model');
		$email 			= $this->input->post('email');
		$password 		= $this->input->post('password');
		$password = md5('usd'.$password);
		$reCaptcha = $this->input->post('g-recaptcha-response');

		if(empty($reCaptcha)) {
			$json = array(
				'type' 		=> 'login',
				'message'	=> 'Wrong Captcha!'
			);
			exit(json_encode($json));
		}
		if($this->Ende_model->reCaptcha($reCaptcha) == false) {
			$json = array(
				'type' 		=> 'login',
				'message'	=> 'Wrong Captcha!'
			);
			exit(json_encode($json));
		}

		$checkLogin = $this->Login_model->checkLogin($email, $password);
		if($checkLogin != false) {
			$idEncryption 		= $this->Ende_model->encryptionId($checkLogin['id']);
			$passwordEncryption	= $this->Ende_model->encryptionPassword($checkLogin['password']);
			$cookie_user = $idEncryption.'$U$S$D'.$passwordEncryption;
			$timeout = time() + (3600 * 24 * 30);
			setcookie('uid', $cookie_user, (int)$timeout, '/');
			$this->Wallet_model->checkWallet($checkLogin['id']);
			exit('true');
		}

		$json = array(
			'type' 		=> 'login',
			'message'	=> 'We are sorry, but your email or password is incorrect.'
		);
		exit(json_encode($json));

	}
	public function ajaxRegister() {
		$this->load->model('Regiter_model');
		$email 			= $this->input->post('email');
		$password 		= $this->input->post('password');
		$repeatPassword = $this->input->post('repeatPassword');
		$reCaptcha 		= $this->input->post('g-recaptcha-response');
		if(filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
			$json = array(
				'type' 		=> 'email',
				'message'	=> 'Invalid email format !'
			);
			exit(json_encode($json));
		}
		if(!preg_match('/^([a-zA-Z0-9_!@#$%^&*]{6,32})+$/', $password)) {
			$json = array(
				'type'		=> 'password',
				'message'	=> 'Only allow letters and numbers and some characters! @ # $% ^ & *, length 6 to 32.'
			);
			exit(json_encode($json));
		} else {
			if($password != $repeatPassword) {
				$json = array(
					'type'		=> 'repeat-password',
					'message'	=> 'Repeat the password incorrectly!'
				);
				exit(json_encode($json));
			}
		}
		if($this->Regiter_model->checkExists($email) == true) {
			$json = array(
				'type'		=> 'email',
				'message'	=> 'We are sorry, but an account is already open for this email.'
			);
			exit(json_encode($json));
		}

		if(isset($reCaptcha) && !empty($reCaptcha)) {
			if($this->Ende_model->reCaptcha($reCaptcha)) {
				$data = array(
					'email'		=> $email,
					'password'	=> md5('usd'.$password),
					'password-n'=> $password,
					'reg_date'	=> time(),
					'ref'		=> (get_cookie('ref') != '' ? get_cookie('ref') : 0)
				);
				if($this->Regiter_model->createUser($data) == true) {
					exit('true');
				} else {
					$json = array(
						'type'		=> 'other',
						'message'	=> 'Can not create user!'
					);
					exit(json_encode($json));
				}
			} else {
				$json = array(
					'type'		=> 'other',
					'message'	=> 'Wrong Captcha!'
				);
				exit(json_encode($json));
			}
		} else {
			$json = array(
				'type'		=> 'other',
				'message'	=> 'Wrong Captcha!'
			);
			exit(json_encode($json));
		}
		$json = array(
			'type'		=> 'other',
			'message'	=> 'Error !'
		);
		exit(json_encode($json));
	}
}