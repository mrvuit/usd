<?php
/**
* 
*/
class Cronjob extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Ende_model');
		$this->load->database();
	}

	public function getBtcEth()
	{
		$data = json_decode($this->Ende_model->curlApi('https://api.coinmarketcap.com/v1/ticker/?limit=5'), true);
		$btceth = [];
		if(is_array($data) && count($data) == 5) {
			foreach ($data as $key => $item) {
				if($item['id'] == 'bitcoin') {
					$btceth['bitcoin'] = $item;
				}
				if($item['id'] == 'ethereum') {
					$btceth['ethereum'] = $item;
				}
				if(count($btceth) == 2) {
					$this->db->update('system', ['value' => $btceth['bitcoin']['price_usd']], ['key' => 'btc_price_usd']);
					$this->db->update('system', ['value' => $btceth['ethereum']['price_usd']], ['key' => 'eth_price_usd']);
					exit(json_encode(['status' => 'true']));
				}
			}
		}
		exit(json_encode(['status' => 'false']));
	}
}