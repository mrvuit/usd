<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'base';
$route['404_override'] = 'base/error';
$route['translate_uri_dashes'] = FALSE;
$route['agreement'] = 'base/agreement';
$route['ratesnlimits'] = 'base/ratesnlimits';
$route['support'] = 'base/support';
$route['register'] = 'base/register';
$route['login'] = 'base/login';
$route['forgot-password'] = 'base/forgot_password';
$route['logout'] = 'base/logout';
$route['wallet'] = 'base/wallet/default';
$route['wallet/(:any)'] = 'base/wallet/$1';
$route['deposit'] = 'base/deposit';
$route['withdraw'] = 'base/withdraw';
$route['transactions'] = 'base/transactions';
$route['referrals'] = 'base/referrals';
$route['account'] = 'base/account';
$route['transactions-admin'] = 'base/transactions_admin';
$route['support-admin'] = 'base/support_admin';
$route['setting-admin'] = 'base/setting_admin';
$route['payment/(:any)'] = 'base/payment/$2';
$route['status-payment'] = 'base/status_payment';

//Language
$route['(:any)/home'] = 'base/index/$1';
$route['(:any)/agreement'] = 'base/agreement';
$route['(:any)/ratesnlimits'] = 'base/ratesnlimits';
$route['(:any)/support'] = 'base/support';
$route['(:any)/register'] = 'base/register';
$route['(:any)/login'] = 'base/login';
$route['(:any)/forgot-password'] = 'base/forgot_password';
$route['(:any)/logout'] = 'base/logout';
$route['(:any)/wallet'] = 'base/wallet/default';
$route['(:any)/deposit'] = 'base/deposit';
$route['(:any)/withdraw'] = 'base/withdraw';
$route['(:any)/transactions'] = 'base/transactions';
$route['(:any)/referrals'] = 'base/referrals';
$route['(:any)/account'] = 'base/account';
$route['(:any)/transactions-admin'] = 'base/transactions_admin';
$route['(:any)/support-admin'] = 'base/support_admin';
$route['(:any)/setting-admin'] = 'base/setting_admin';
$route['(:any)/payment/(:any)'] = 'base/payment/$2';
