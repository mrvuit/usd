<?php
// अन्य
$lang ['confirm'] = 'पुष्टि करें';
$lang ['status'] = 'स्थिति';
$lang ['amount'] = 'राशि';
$lang ['empty'] = 'खाली';
$lang ['balance'] = 'बैलेंस';
$lang ['wallet'] = 'वॉलेट';
$lang ['address'] = 'पता';
$lang ['enter'] = 'दर्ज करें';
$lang ['wrong_captcha'] = 'गलत कैप्चा!';
$lang ['time'] = 'समय';
$lang ['type'] = 'टाइप';
$lang ['cancel'] = 'रद्द करें';
$lang ['cannot'] = 'नहीं कर सकते';
$lang ['close'] = 'बंद';
$lang ['copy'] = 'प्रतिलिपि';
$lang ['send'] = 'भेजें';

// 404 पृष्ठ
$lang ['404_title'] = 'त्रुटि पृष्ठ';
$lang ['404_note'] = 'यह वेबपेज नहीं मिला।';

// लॉग इन और रजिस्टार और लॉगआउट
$lang ['login'] = 'लॉगिन';
$lang ['register'] = 'रजिस्टर';
$lang ['logout'] = 'लॉगआउट';
$lang ['login_register'] = 'अभी जुड़ें';
$lang ['your_password'] = 'आपका पासवर्ड:';
$lang ['repeat_password'] = 'पासवर्ड दोहराएं:';
$lang ['register_note'] = 'मैंने उपयोगकर्ता के समझौते को पढ़ा और सभी जोखिमों को समझते हैं जो उच्च उपज निवेश के साथ जुड़ा हो सकता है।';
$lang ['register_success'] = 'सफलतापूर्वक पंजीकृत, आप लॉगिन करने में सक्षम हुए हैं।';
$lang ['login_success'] = '1 सेकंड के बाद रीडायरेक्ट करें या क्लिक करें';
$lang ['here'] = 'यहां';

//मेन्यू
$lang ['home'] = 'निवेश करें और पैसा ऑनलाइन बनाएं';
$lang ['dashboard'] = 'डैशबोर्ड';
$lang ['deposit'] = 'जमा';
$lang ['withdraw'] = 'निकालना';
$lang ['myWallet'] = 'मेरा वॉलेट';
$lang ['history'] = 'इतिहास';
$lang ['myAccount'] = 'मेरा खाता';
$lang ['myReferrals'] = 'मेरा रेफरल';
$lang ['ratesnlimits'] = 'दरें और सीमा';
$lang ['contactus'] = 'हमसे संपर्क करें';
$lang ['agreement'] = 'अनुबंध';

// डैशबोर्ड || होम
$lang ['total_balance'] = 'कुल शेष';
$lang ['interest_money'] = 'ब्याज मनी';
$lang ['interest_after'] = 'ब्याज दरों में वृद्धि के बाद';
$lang ['you_earn'] = 'आप कमाते हैं:';
$lang ['note_interest'] = '* नोट: ब्याज 00:00 (जीएमटी + 0) में जोड़ा गया है';
$lang ['hold'] = 'अस्थायी पकड़';
$lang ['note_hold'] = '12 घंटे के दौरान पूरा हो जाएगा';
$lang ['parameters'] = 'आपके पैरामीटर';
$lang ['deposit_interest'] = 'प्रति दिन जमा ब्याज दर';
$lang ['deposit_interest_note'] = 'प्रतिदिन 1.3% कमाई';
$lang ['my_referrer'] = 'मेरा संदर्भ';
$lang ['my_referrer_note'] = 'सफलतापूर्वक प्रस्तुत और सक्रिय मित्रों की संख्या।';

// जमा
$lang ['select_payment'] = 'अपना भुगतान सिस्टम प्रकार चुनें:';
$lang ['select_one_wallet'] = 'एक वॉलेट चुनें';
$lang ['enter_amount'] = 'धन राशि दर्ज करें:';
$lang ['deposit_limit'] = 'जमा सीमा';
$lang ['deposit_amount'] = 'जमा राशि';
$lang ['deposit_history'] = 'जमा इतिहास';
$lang ['go_to_wallet'] = 'अपने वॉलेट पर जाएं';
$lang ['cannot_deposit'] = 'आप अपने वॉलेट को अपडेट किए बिना धन जमा नहीं कर सकते!';
$lang ['press_the_button'] = 'बटन दबाएं';
$lang ['to_payment_now'] = 'अब भुगतान करने के लिए';
$lang ['modal_note'] = 'कृपया राशि को मत बदलें, जो आपके सौदे को प्रभावित कर सकती है।';
$lang ['use_address'] = 'पते का उपयोग करें:';
$lang ['transfer'] = 'स्थानांतरण:';
$lang ['to'] = 'से';
$lang ['after_transferring'] = 'स्थानांतरित करने के बाद "ओके" बटन पर क्लिक करें।';

// हटें
$lang ['withdraw_limit'] = 'वापसी सीमा';
$lang ['withdraw_amount'] = 'राशि निकालना';
$lang ['withdraw_history'] = 'इतिहास निकालना';
$lang ['cannot_withdraw'] = 'आप अपने वॉलेट को अपडेट किए बिना पैसे वापस नहीं ले सकते! ';
$lang ['not_enough_withdraw'] = 'पर्याप्त नहीं निकालेगा!';
$lang ['success_withdraw'] = 'सफल वापसी, आपको 12 घंटों के भीतर धन मिलेगा।';
$lang ['invaild_wallet'] = 'अनवरत वॉलेट!';
$lang ['invaild_amount'] = 'अनवरत राशि!';

// बटुआ
$lang ['wallet_note'] = 'कृपया सही दर्ज करें, गलत भुगतान के मामले में हम आपकी मदद नहीं कर सकते। आप एक समय में एक या अधिक जेब अपडेट कर सकते हैं। ';
$lang ['update_wallet_success'] = 'अपडेट किए गए बटुआ पता सफलतापूर्वक।';
$lang ['address_wallet_invalid'] = 'पता बटुआ अमान्य है';

// लेन-देन
$lang ['take_image'] = 'एक छवि लें';
$lang ['confirm_cancel'] = 'रद्द करें की पुष्टि करें';
$lang ['transaction_proof'] = 'लेनदेन प्रूफ';
$lang ['cancel_note'] = 'क्या आप वाकई वापसी को हटाना चाहते हैं ?, केवल 10 मिनट के बाद ही आपकी वापसी को रद्द कर सकते हैं, आपको रिफंड की रकम जमा कर दी जाएगी।';

//लेखा
$lang ['account_verification'] = 'खाता सत्यापन';
$lang ['account_verified'] = 'आपका खाता सत्यापित हो गया है।';
$lang ['account_not_verified'] = 'आपका खाता सक्रिय नहीं हुआ है, आपको अपने खाते को सक्रिय करने के लिए जमा करने की आवश्यकता है।';
$lang ['your_email'] = 'आपका ईमेल:';
$lang ['deposit_now'] = 'अब जमा करें';
$lang ['change_password'] = 'पासवर्ड बदलें';
$lang ['old_password'] = 'पुराने पासवर्ड';
$lang ['new_password'] = 'नया पासवर्ड';
$lang ['confirm_password'] = 'पासवर्ड की पुष्टि करें';
$lang ['same_password'] = 'पासवर्ड की पुष्टि करना समान नहीं है।';
$lang ['incorrect_password'] = 'पुराना पासवर्ड गलत है।';
$lang ['allow_password'] = 'केवल अक्षरों और संख्याओं और कुछ पात्रों को ही अनुमति दें! @ # $% ^ और *, लंबाई 6 से 32. ';
$lang ['change_password_note'] = '* नोट: यदि आप अपना पासवर्ड बदलते हैं तो आपको फिर से प्रवेश करना होगा।';

// REFERERAL
$lang ['referral_link'] = 'रेफ़रल लिंक';
$lang ['copy_reffereral_link'] = 'नीचे अपना रेफ़रल लिंक कॉपी करें:';
$lang ['referral_list'] = 'रेफ़रल सूची';
$lang ['activated'] = 'सक्रिय';
$lang ['reg_date'] = 'रेग तिथि';

// विगेट राइट
$lang ['latest_transaction'] = 'नवीनतम लेनदेन';
$lang ['active_investors'] = 'सक्रिय निवेशक:';
$lang ['we_accept'] = 'हम स्वीकार करते हैं';
$lang ['deposit_and_withdraw'] = 'जमा और वापसी';
$lang ['yield_calculator'] = 'यील्ड कैलक्यूलेटर';
$lang ['term_deposit'] = 'जमा की अवधि (दिन):';
$lang ['automatic_recap'] = 'स्वचालित पुनर्पूंजीकरण।';
$lang ['every_withdraw'] = 'मुझे हर दिन लाभ वापस लेना है।';
$lang ['your_profit'] = 'आपका सच लाभ';
$lang ['your_absolute'] = 'आपका निरपेक्ष शेष:';
$lang ['yield_calculator_note'] = 'यील्ड कैलक्यूलेटर आपको पहले से निवेश योजना (1.3% / दिन के लिए) बताता है।';

//समर्थन
$lang ['your_message'] = 'आपका संदेश:';
$lang ['support_note'] = 'कृपया अपना ईमेल ध्यान से जांचें और हम आपके सवाल का उत्तरदायी रूप से और जितनी तेज़ी से (12 घंटे के दौरान सामान्य रूप से) उत्तर देंगे।';
$lang ['wrong_email'] = 'गलत ईमेल प्रारूप!';
$lang ['message_invalid'] = 'संदेश खाली नहीं हो सकता!';
$lang ['support_success'] = 'बधाई हो! आपका संदेश टीम का समर्थन करने के लिए भेजा गया था आप अपने ईमेल में 12 घंटे के दौरान उत्तर प्राप्त करेंगे। ';

//कीमत सीमा
$lang ['interest_per_day'] = 'प्रति दिन जमा ब्याज दर';
$lang ['interest_rate'] = 'ब्याज दर';
$lang ['daily_limit'] = 'दैनिक सीमा';
$lang ['fees'] = 'शुल्क';
$lang ['deposit_fees'] = 'जमा शुल्क';
$lang ['withdraw_fees'] = 'फीड वापस';
$lang ['deposit_24h'] = '24h के दौरान जमा की संख्या';
$lang ['withdraw_24h'] = '24h के दौरान वापसी की संख्या';

//पासवर्ड भूल गए
$lang ['forgot_wrong_email'] = 'क्षमा करें, हमें यह ईमेल किसी भी खाते से नहीं मिला।';
$lang ['forgot_success'] = 'कृपया अपना इनबॉक्स (स्पैम मेल सहित) की जांच करें, हमने एक पासवर्ड रिकवरी लिंक भेजा है, लिंक 2 घंटे के भीतर मौजूद है।';
$lang ['reset_pw_success'] = 'सफलतापूर्वक पासवर्ड बदलें, आप लॉगिन कर सकते हैं।';
$lang ['forgot_password'] = 'पासवर्ड भूल गए';

// डैशबोर्ड
$lang ['index_title_h3'] = 'निवेश करना और पैसा कम करना आसान है';
$lang ['index_item_1'] = 'न्यूनतम जमा केवल $ 1 है।';
$lang ['index_item_2'] = 'पुनर्पूंजीकरण के लिए स्वत: लाभ।';
$lang ['index_item_3'] = 'प्रत्येक रेफरल के जमा के लिए 10% अद्वितीय भागीदारी कार्यक्रम।';
$lang ['index_title_h3_2'] = 'हमारा कार्य';
$lang ['index_item_7'] = 'हम सबसे अच्छे निवेशकों का एक संग्रह हैं, हम अपने पैसे का उपयोग लाभ बनाने के लिए करते हैं, यहां तक कि मुश्किल बाजार में भी हम विजेता हैं।';
$lang ['index_item_8'] = 'हम 50 क्रिप्टो के बारे में शोध कर रहे हैं और निवेश कर रहे हैं।';