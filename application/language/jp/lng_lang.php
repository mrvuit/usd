﻿<?php
//その他
$lang ['confirm'] = '確認';
$lang ['status'] = 'ステータス';
$lang ['amount'] = '金額';
$lang ['empty'] = '空';
$lang ['balance'] = '残高';
$lang ['wallet'] = 'Wallet';
$lang ['address'] = 'アドレス';
$lang ['enter'] = 'Enter';
$lang ['wrong_captcha'] = '間違ったキャプチャ！';
$lang ['time'] = '時間';
$lang ['type'] = 'タイプ';
$lang ['cancel'] = 'キャンセル';
$lang ['cannot'] = 'できません';
$lang ['close'] = '閉じる';
$lang ['copy'] = 'コピー';
$lang ['send'] = '送信';

// 404ページ
$lang ['404_title'] = 'エラーページ';
$lang ['404_note'] = 'このウェブページは見つかりませんでした。';

// LOGIN＆REGISTER＆LOGOUT
$lang ['login'] = 'ログイン';
$lang ['register'] = '登録';
$lang ['logout'] = 'ログアウト';
$lang ['login_register'] = '今すぐ参加する';
$lang ['your_password'] = 'あなたのパスワード：';
$lang ['repeat_password'] = 'パスワードを繰り返します：';
$lang ['register_note'] = 'ユーザーの契約書を読み、高収入投資に関連するすべてのリスクを理解しています。';
$lang ['register_success'] = 'ログインできました。';
$lang ['login_success'] = '1秒後にリダイレクトするか、クリックしてください。';
$lang ['ここ'] = 'ここ';

//メニュー
$lang ['home'] = '投資してオンラインでお金を稼ぐ';
$lang ['dashboard'] = 'ダッシュボード';
$lang ['deposit'] = '入金';
$lang ['withdraw'] = '撤退する';
$lang ['myWallet'] = 'My Wallet';
$lang ['history'] = '履歴';
$lang ['myAccount'] = 'マイアカウント';
$lang ['myReferrals'] = '私の紹介';
$lang ['ratesnlimits'] = 'レートと制限';
$lang ['contactus'] = 'お問い合わせ';
$lang ['agreement'] = '契約';

//ダッシュボード||ホーム
$lang ['total_balance'] = '合計残高';
$lang ['interest_money'] = '金利';
$lang ['interest_after'] = '金利が上昇した後';
$lang ['you_earn'] = 'あなたは獲得する：';
$lang ['note_interest'] = '*注：金利は00:00に追加されます（GMT + 0）';
$lang ['hold'] = '一時保留中';
$lang ['note_hold'] = '12時間以内に完了しました。';
$lang ['parameters'] = 'あなたのパラメータ';
$lang ['deposit_interest'] = '1日の預金金利';
$lang ['deposit_interest_note'] = '1日当たり1.3％の収入';
$lang ['my_referrer'] = '私のリファラー';
$lang ['my_referrer_note'] = '友人の紹介と通話が成功した人数。';

//保証金
$lang ['select_payment'] = 'お支払い方法を選択してください：';
$lang ['select_one_wallet'] = 'ワンウォレットを選択';
$lang ['enter_amount'] = '金額を入力してください：';
$lang ['deposit_limit'] = '入金限度額';
$lang ['deposit_amount'] = '入金金額';
$lang ['deposit_history'] = '入金履歴';
$lang ['go_to_wallet'] = 'あなたのウォレットに行く';
$lang ['cannot_deposit'] = 'ウォレットを更新せずに入金することはできません！';
$lang ['press_the_button'] = 'ボタンを押してください';
$lang ['to_payment_now'] = '今支払する';
$lang ['modal_note'] = '金額を変更しないでください。これはあなたの取引に影響する可能性があります。';
$lang ['use_address'] = 'アドレスを使用する：';
$lang ['transfer'] = '転送：';
$lang ['to'] = 'to';
$lang ['after_transferring'] = '転送後に「OK」ボタンをクリックしてください。';

// WITHDRAW
$lang ['withdraw_limit'] = '撤回制限';
$lang ['withdraw_amount'] = '引き出し金額';
$lang ['withdraw_history'] = '履歴を取り消す';
$lang ['cannot_withdraw'] = 'ウォレットを更新せずにお金を引き出すことはできません！ ';
$lang ['not_enough_withdraw'] = '撤退が足りません！';
$lang ['success_withdraw'] = '12時間以内にお金を受け取ることに成功しました。';
$lang ['invaild_wallet'] = 'Invaild Wallet！';
$lang ['invaild_amount'] = 'インビルド額！';

//財布
$lang ['wallet_note'] = '正しく入力してください。間違った支払いがあった場合はお手伝いできません。一度に1つまたは複数のウォレットを更新することができます。 ';
$lang ['update_wallet_success'] = 'ウォレットアドレスを更新しました。';
$lang ['address_wallet_invalid'] = 'アドレスウォレットは無効です';

//トランザクション
$lang ['take_image'] = '画像を撮る';
$lang ['confirm_cancel'] = 'キャンセルの確認';
$lang ['transaction_proof'] = 'トランザクション証明';
$lang ['cancel_note'] = '引き出しを削除してもよろしいですか？\'、10分後にのみ引き出しを取り消すことができます。返金額に加算されます。';

//アカウント
$lang ['account_verification'] = 'アカウント確認';
$lang ['account_verified'] = 'あなたのアカウントは確認済みです。';
$lang ['account_not_verified'] = 'あなたのアカウントは有効化されていません。';
$lang ['your_email'] = 'あなたのメールアドレス：';
$lang ['deposit_now'] = '今すぐ入金する';
$lang ['change_password'] = 'パスワードを変更する';
$lang ['old_password'] = '古いパスワード';
$lang ['new_password'] = '新しいパスワード';
$lang ['confirm_password'] = 'パスワードの確認';
$lang ['same_password'] = 'パスワードの確認は同じではありません。';
$lang ['incorrect_password'] = '古いパスワードが間違っています。';
$lang ['allow_password'] = '文字と数字といくつかの文字だけを許可します！ @＃$％^＆*、長さは6〜32です。 ';
$lang ['change_password_note'] = '*注意：パスワードを変更した場合は、再度ログインする必要があります。';

// REFERERAL
$lang ['referral_link'] = '紹介リンク';
$lang ['copy_reffereral_link'] = '下記のあなたの紹介リンクをコピーしてください：';
$lang ['referral_list'] = '紹介リスト';
$lang ['activated'] = '有効化';
$lang ['reg_date'] = 'レジデント';

// WIDGET RIGHT
$lang ['latest_transaction'] = '最新の取引';
$lang ['active_investors'] = 'アクティブ投資家：';
$lang ['we_accept'] = '受け入れる';
$lang ['deposit_and_withdraw'] = '入出金';
$lang ['yield_calculator'] = '歩留まり計算機';
$lang ['term_deposit'] = '預金期間（日）：';
$lang ['automatic_recap'] = '自動資本化';
$lang ['every_withdraw'] = '毎日利益を取り戻したい。';
$lang ['your_profit'] = 'あなたの本当の利益';
$lang ['your_absolute'] = 'あなたの絶対残高：';
$lang ['yield_calculator_note'] = '歩留まり計算機は事前に投資計画を伝えます（1.3％/日）。';

//サポート
$lang ['your_message'] = 'あなたのメッセージ：';
$lang ['support_note'] = 'あなたのメールアドレスを慎重にチェックしてください。できるだけ速やかに質問に回答します。';
$lang ['wrong_email'] = '間違ったメールフォーマットです！';
$lang ['message_invalid'] = 'メッセージは空ではありません！';
$lang ['support_success'] = 'おめでとうございます！あなたのメッセージはサポートチームに送られました。あなたのメールに12時間以内に回答が届きます。 ';

// RATE＆LIMIT
$lang ['interest_per_day'] = '1日の預金金利';
$lang ['interest_rate'] = '金利';
$lang ['daily_limit'] = '毎日の制限';
$lang ['fees'] = '料金';
$lang ['deposit_fees'] = '入金金額';
$lang ['withdraw_fees'] = '引き取り手数料';
$lang ['deposit_24h'] = '24時間のデポジット数';
$lang ['withdraw_24h'] = '24時間中に撤退する回数';

//パスワードをお忘れですか
$lang ['forgot_wrong_email'] = '申し訳ありません、アカウントでこのメールが見つかりませんでした。';
$lang ['forgot_success'] = 'あなたの受信箱（迷惑メールを含む）を確認してください。パスワード回復リンクを送ってきました。リンクは2時間以内に存在します。';
$lang ['reset_pw_success'] = 'パスワードを正しく変更してください。ログインできます。';
$lang ['forgot_password'] = 'パスワードを忘れました';

//ダッシュボード
$lang ['index_title_h3'] = 'お金を稼ぐことは簡単です';
$lang ['index_item_1'] = '最低入金金額は$ 1です。';
$lang ['index_item_2'] = '自動的に資本増強を行います。';
$lang [' index_item_3 '] ='照会ごとに10％という独自のパートナーシッププログラム。';
$lang ['index_title_h3_2'] = '私たちの仕事';
$lang ['index_item_7'] = 'われわれは最高の投資家の集まりであり、私たちが依然として勝っている最も厳しい市場であっても、あなたのお金を使って利益を上げるために投資します。';
$lang ['index_item_8'] = '約50の暗号を研究して投資しています。';