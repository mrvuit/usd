<?php
//OTHER
$lang['confirm'] 				= 'Confirm';
$lang['status'] 				= 'Status';
$lang['amount'] 				= 'Amount';
$lang['empty'] 					= 'empty';
$lang['balance'] 				= 'Balance';
$lang['wallet'] 				= 'Wallet';
$lang['address'] 				= 'Address';
$lang['enter'] 					= 'Enter';
$lang['wrong_captcha'] 			= 'Wrong captcha!';
$lang['time'] 					= 'Time';
$lang['type'] 					= 'Type';
$lang['cancel'] 				= 'Cancel';
$lang['cannot'] 				= 'Can not';
$lang['close'] 					= 'Close';
$lang['copy'] 					= 'Copy';
$lang['send'] 					= 'Send';

//404 PAGE
$lang['404_title'] 				= 'ERROR PAGE';
$lang['404_note'] 				= 'This webpage was not found.';

//LOGIN & REGISTER & LOGOUT
$lang['login'] 					= 'Login';
$lang['register'] 				= 'Register';
$lang['logout'] 				= 'Logout';
$lang['login_register'] 		= 'Join Now';
$lang['your_password'] 			= 'Your password:';
$lang['repeat_password'] 		= 'Repeat the password:';
$lang['register_note'] 			= 'I read the User\'s Agreement and understand all risks which may be associated with high yield investment.';
$lang['register_success'] 		= 'Successfully registered, you have been able to login.';
$lang['login_success'] 			= 'Redirect after 1 seconds or click';
$lang['here'] 					= 'here';

//MENU
$lang['home'] 					= 'Invest and make money online';
$lang['dashboard'] 				= 'Dashboard';
$lang['deposit'] 				= 'Deposit';
$lang['withdraw'] 				= 'Withdraw';
$lang['myWallet'] 				= 'My Wallet';
$lang['history'] 				= 'History';
$lang['myAccount'] 				= 'My Account';
$lang['myReferrals'] 			= 'My Referrals';
$lang['ratesnlimits'] 			= 'Rates & Limits';
$lang['contactus'] 				= 'Contact Us';
$lang['agreement'] 				= 'Agreement';

//DASHBOARD || HOME
$lang['total_balance'] 			= 'Total Balance';
$lang['interest_money'] 		= 'Interest Money';
$lang['interest_after'] 		= 'Interest rates increase after';
$lang['you_earn'] 				= 'You earn:';
$lang['note_interest'] 			= '*Note: Interest is added at 00:00 (GMT + 0)';
$lang['hold'] 					= 'Temporary on Hold';
$lang['note_hold'] 				= 'Will been completed during 12 hours';
$lang['parameters'] 			= 'Your parameters';
$lang['deposit_interest'] 		= 'Deposit interest rate per day';
$lang['deposit_interest_note'] 	= 'Earnings 1.3% per day';
$lang['my_referrer'] 			= 'My referrer';
$lang['my_referrer_note'] 		= 'Number of friends successfully introduced and activated.';

//DEPOSIT
$lang['select_payment'] 		= 'Select your payment system type:';
$lang['select_one_wallet'] 		= 'Select one wallet';
$lang['enter_amount'] 			= 'Enter adding money amount:';
$lang['deposit_limit'] 			= 'Deposit Limit';
$lang['deposit_amount'] 		= 'Deposit Amount';
$lang['deposit_history'] 		= 'Deposit History';
$lang['go_to_wallet'] 			= 'Go to your Wallet';
$lang['cannot_deposit'] 		= 'You can not deposit money without updating your wallet!';
$lang['press_the_button'] 		= 'Press the button';
$lang['to_payment_now'] 		= 'to payment now';
$lang['modal_note'] 			= 'Please do not change the amount, which may affect your deal.';
$lang['use_address']			= 'Use address:';
$lang['transfer']				= 'Transfer:';
$lang['to']						= 'to';
$lang['after_transferring']		= 'Click on "OK" button after transferring.';

//WITHDRAW
$lang['withdraw_limit'] 		= 'Withdraw Limit';
$lang['withdraw_amount'] 		= 'Withdraw Amount';
$lang['withdraw_history'] 		= 'Withdraw History';
$lang['cannot_withdraw'] 		= 'You can not withdraw money without updating your wallet! ';
$lang['not_enough_withdraw'] 	= 'Not enough withdraw!';
$lang['success_withdraw'] 		= 'Successful withdrawal, you will receive money within 12 hours.';
$lang['invaild_wallet'] 		= 'Invaild Wallet!';
$lang['invaild_amount'] 		= 'Invaild Amount!';

//WALLET
$lang['wallet_note'] 			= 'Please enter correct, in case of wrong payment we can not help you. You can update one or more wallets at a time.';
$lang['update_wallet_success'] 	= 'Updated wallet address successfully.';
$lang['address_wallet_invalid'] = 'Address wallet is invalid';

//TRANSACTION
$lang['take_image'] 			= 'Take a image';
$lang['confirm_cancel'] 		= 'Confirm cancel';
$lang['transaction_proof'] 		= 'Transaction Proof';
$lang['cancel_note'] 			= 'Are you sure you want to delete the withdrawal?, can only cancel your withdrawal after 10 minutes, you will be credited the amount of the refund.';

//ACCOUNT
$lang['account_verification'] 	= 'Account Verification';
$lang['account_verified'] 		= 'Your account has been verified.';
$lang['account_not_verified'] 	= 'Your account has not been activated, you need once deposit to activate your account.';
$lang['your_email'] 			= 'Your email:';
$lang['deposit_now'] 			= 'Deposit now';
$lang['change_password'] 		= 'Change password';
$lang['old_password'] 			= 'Old Password';
$lang['new_password'] 			= 'New password';
$lang['confirm_password'] 		= 'Confirm Password';
$lang['same_password'] 			= 'Confirm password is not the same.';
$lang['incorrect_password'] 	= 'The old password is incorrect.';
$lang['allow_password'] 		= 'Only allow letters and numbers and some characters! @ # $% ^ & *, length 6 to 32.';
$lang['change_password_note'] 	= '*Note: If you change your password you will need to log in again.';

//REFERERAL
$lang['referral_link'] 			= 'Referral Link';
$lang['copy_reffereral_link'] 	= 'Copy your referral Link below:';
$lang['referral_list'] 			= 'Referral List';
$lang['activated'] 				= 'Activated';
$lang['reg_date'] 				= 'Reg Date';

//WIDGET RIGHT
$lang['latest_transaction'] 	= 'Latest Transaction';
$lang['active_investors'] 		= 'Active Investors:';
$lang['we_accept'] 				= 'We accept';
$lang['deposit_and_withdraw'] 	= 'Deposit and Withdraw';
$lang['yield_calculator'] 		= 'Yield Calculator';
$lang['term_deposit'] 			= 'Term of deposit (days):';
$lang['automatic_recap'] 		= 'Automatic recapitalization.';
$lang['every_withdraw'] 		= 'I want to withdraw profit every day.';
$lang['your_profit'] 			= 'Your Really Profit';
$lang['your_absolute '] 		= 'Your Absolute Balance:';
$lang['yield_calculator_note'] 	= 'The Yield Calculator tells you in advance the investment plan (for 1.3%/day).';

//SUPPORT
$lang['your_message'] 			= 'Your message:';
$lang['support_note'] 			= 'Please check carefully your email and we\'ll answer to your question informatively and as fast as possible (as usual during 12 hours).';
$lang['wrong_email'] 			= 'Wrong email format!';
$lang['message_invalid'] 		= 'Message cannot empty!';
$lang['support_success'] 		= 'Congratulations! Your message was sent to support team. You\'ll receive the answer during 12 hours to your email.';

//RATE & LIMIT
$lang['interest_per_day'] 		= 'Deposit interest rate per day';
$lang['interest_rate'] 			= 'Interest rate';
$lang['daily_limit'] 			= 'Daily limits';
$lang['fees'] 					= 'Fees';
$lang['deposit_fees'] 			= 'Deposit fees';
$lang['withdraw_fees'] 			= 'Withdraw fees';
$lang['deposit_24h'] 			= 'Number of Deposit during 24h';
$lang['withdraw_24h'] 			= 'Number of Withdraw during 24h';

//FORGOT PASSWORD
$lang['forgot_wrong_email'] 	= 'Sorry, we did not find this email with any accounts.';
$lang['forgot_success'] 		= 'Please check your inbox (including spam mail), we have sent a password recovery link, the link exists within 2 hours.';
$lang['reset_pw_success'] 		= 'Change password successfully, you can login.';
$lang['forgot_password'] 		= 'Forgot Password';

//DASHBOARD
$lang['index_title_h3'] 		= 'Investing and earning money is easy';
$lang['index_item_1'] 			= 'The minimum deposit is only $1.';
$lang['index_item_2'] 			= 'Automatically profit recapitalization.';
$lang['index_item_3'] 			= 'Unique partnership program such as 10% for each referral\'s deposit.';
$lang['index_title_h3_2'] 		= 'Our work';
$lang['index_item_7'] 			= 'We are a collection of the best investors, we use your money to invest to make a profit, even at the toughest market we are still the winners.';
$lang['index_item_8'] 			= 'We are researching and investing about 50 crypto.';