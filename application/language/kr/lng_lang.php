<?php
//다른
$lang ['confirm'] = '확인';
$lang ['status'] = '상태';
$lang ['amount'] = '금액';
$lang ['empty'] = '비어 있음';
$lang ['balance'] = '잔액';
$lang ['wallet'] = '월렛';
$lang ['address'] = '주소';
$lang ['enter'] = '입력';
$lang ['wrong_captcha'] = '틀린 captcha!';
$lang ['time'] = '시간';
$lang ['type'] = '유형';
$lang ['cancel'] = '취소';
$lang ['can not'] = '할 수 없습니다';
$lang ['close'] = '닫기';
$lang ['copy'] = '복사';
$lang ['send'] = '보내기';

// 404 페이지
$lang ['404_title'] = '오류 페이지';
$lang ['404_note'] = '이 웹 페이지를 찾을 수 없습니다.';

// LOGIN & REGISTER & LOGOUT
$lang ['login'] = '로그인';
$lang ['register'] = '등록';
$lang ['logout'] = '로그 아웃';
$lang ['login_register'] = '지금 가입하십시오';
$lang ['your_password'] = '귀하의 비밀번호 :';
$lang ['repeat_password'] = '암호를 반복하십시오 :';
$lang ['register_note'] = '사용자 동의서를 읽고 고수익 투자와 관련된 모든 위험을 이해합니다.';
$lang ['register_success'] = '성공적으로 등록되었습니다. 로그인하실 수 있습니다.';
$lang ['login_success'] = '1 초 후에 리디렉션하거나 클릭하십시오';
$lang ['여기'] = '여기';

//메뉴
$lang ['home'] = '온라인 투자 및 돈 벌기';
$lang ['dashboard'] = '대시 보드';
$lang ['deposit'] = '보증금';
$lang ['withdraw'] = '빼다';
$lang ['myWallet'] = '내 지갑';
$lang ['history'] = '역사';
$lang ['myAccount'] = '내 계정';
$lang ['myReferrals'] = '내 추천';
$lang ['ratesnlimits'] = '요금 및 제한';
$lang ['contactus'] = '문의하기';
$lang ['agreement'] = '계약';

// 대시 보드 || 집
$lang ['total_balance'] = '총 잔액';
$lang ['interest_money'] = '이자 돈';
$lang ['interest_after'] = '금리 인상 이후';
$lang ['you_earn'] = '벌이십시오 :';
$lang ['note_interest'] = '* 주 :이자는 00:00 (GMT + 0)에 추가됩니다.';
$lang ['hold'] = '임시 보류 중';
$lang ['note_hold'] = '12 시간 동안 완료되었습니다.';
$lang ['parameters'] = '귀하의 매개 변수';
$lang ['deposit_interest'] = '일일 예금 금리';
$lang ['deposit_interest_note'] = '하루 수입 1.3 %';
$lang ['my_referrer'] = '내 추천인';
$lang ['my_referrer_note'] = '성공적으로 소개되고 활성화 된 친구들의 수입니다.';

//예금
$lang ['select_payment'] = '지불 시스템 유형을 선택하십시오 :';
$lang ['select_one_wallet'] = '지갑 하나 선택';
$lang ['enter_amount'] = '금액 추가 입력 :';
$lang ['deposit_limit'] = '입금 한도';
$lang ['deposit_amount'] = '입금액';
$lang ['deposit_history'] = '입금 내역';
$lang ['go_to_wallet'] = '지갑으로 이동';
$lang ['cannot_deposit'] = '지갑을 갱신하지 않고 돈을 예치 할 수 없습니다!';
$lang ['press_the_button'] = '버튼을 누르십시오';
$lang ['to_payment_now'] = '지금 지불하기';
$lang ['modal_note'] = '귀하의 거래에 영향을 줄 수있는 금액을 변경하지 마십시오.';
$lang ['use_address'] = '주소 사용 :';
$lang ['transfer'] = '전송 :';
$lang ['to'] = 'to';
$lang ['after_transferring'] = '전송 후\'확인 \'버튼을 클릭하십시오.';

//빼다
$lang ['withdraw_limit'] = '철회 한도';
$lang ['withdraw_amount'] = '인출 금액';
$lang ['withdraw_history'] = '기록 철회';
$lang ['cannot_withdraw'] = '지갑을 갱신하지 않고 돈을 인출 할 수 없습니다! ';
$lang ['not_enough_withdraw'] = '철회가 충분하지 않습니다!';
$lang ['success_withdraw'] = '취소가 완료되면 12 시간 이내에 돈을 받게됩니다.';
$lang ['invaild_wallet'] = 'Invaild Wallet!';
$lang ['invaild_amount'] = '인보이스 금액!';

//지갑
$lang ['wallet_note'] = '잘못 입력했을 경우를 대비하여 정확한 정보를 입력하십시오. 한 번에 하나 이상의 지갑을 업데이트 할 수 있습니다. ';
$lang ['update_wallet_success'] = '갱신 된 지갑 주소입니다.';
$lang ['address_wallet_invalid'] = '주소 지갑이 유효하지 않습니다';

//트랜잭션
$lang ['take_image'] = '이미지 찍기';
$lang ['confirm_cancel'] = '취소 확인';
$lang ['transaction_proof'] = '거래 증명';
$lang ['cancel_note'] = '철회를 취소 하시겠습니까?, 10 분 후에 만 ​​철회를 취소 할 수 있으며, 환불 금액을 적립하실 수 있습니다. ';

//계정
$lang ['account_verification'] = '계정 확인';
$lang ['account_verified'] = '귀하의 계정이 확인되었습니다.';
$lang ['account_not_verified'] = '귀하의 계정이 활성화되지 않았 으면, 일단 계정을 활성화 시키려면 보증금을 지불하셔야합니다.';
$lang ['your_email'] = '귀하의 이메일 :';
$lang ['deposit_now'] = '지금 입금하십시오';
$lang ['change_password'] = '암호 변경';
$lang ['old_password'] = '이전 암호';
$lang ['new_password'] = '새 비밀번호';
$lang ['confirm_password'] = '암호 확인';
$lang ['same_password'] = '암호가 틀린 지 확인하십시오.';
$lang ['incorrect_password'] = '이전 암호가 틀립니다.';
$lang ['allow_password'] = '글자와 숫자 및 일부 문자 만 허용합니다! @ # $ % ^ & *, 길이 6 ~ 32 ';
$lang ['change_password_note'] = '*주의 : 비밀번호를 변경하면 다시 로그인해야합니다.';

// REFERERAL
$lang ['referral_link'] = '추천 링크';
$lang ['copy_reffereral_link'] = '귀하의 추천 링크를 아래에 복사하십시오 :';
$lang ['referral_list'] = '추천 목록';
$lang ['activated'] = '활성화 됨';
$lang ['reg_date'] = '등록일';

// WIDGET RIGHT
$lang ['latest_transaction'] = '최신 거래';
$lang ['active_investors'] = '적극 투자자 :';
$lang ['we_accept'] = '받아들입니다';
$lang ['deposit_and_withdraw'] = '예금 및 인출';
$lang ['yield_calculator'] = '수율 계산기';
$lang ['term_deposit'] = '정기 예금 (일) :';
$lang ['automatic_recap'] = '자동 자본 재구성';
$lang ['every_withdraw'] = '매일 이익을 내고 싶습니다.';
$lang ['your_profit'] = '당신의 정말 이익';
$lang ['your_absolute'] = '절대 균형 :';
$lang ['yield_calculator_note'] = '수율 계산기는 미리 투자 계획을 알려줍니다 (1.3 % / 일).';

//지원하다
$lang ['your_message'] = '당신의 메시지 :';
$lang ['support_note'] = '이메일을 신중히 확인해주십시오. 가능한 한 빨리 정보에 대해 정중하게 답변 해 드리겠습니다.';
$lang ['wrong_email'] = '잘못된 이메일 형식입니다!';
$lang ['message_invalid'] = '메시지를 비울 수 없습니다!';
$lang ['support_success'] = '축하합니다! 귀하의 메시지가 지원팀에 전송되었습니다. 이메일로 12 시간 동안 답변을 받으실 수 있습니다. ';

// RATE & LIMIT
$lang ['interest_per_day'] = '일일 예금 금리';
$lang ['interest_rate'] = '금리';
$lang ['daily_limit'] = '일일 한도';
$lang ['fees'] = '수수료';
$lang ['deposit_fees'] = '입금 수수료';
$lang ['withdraw_fees'] = '철회 비용';
$lang ['deposit_24h'] = '24 시간 동안 보증금 수';
$lang ['withdraw_24h'] = '24 시간 동안 철회 횟수';

//비밀번호를 잊으 셨나요
$lang ['forgot_wrong_email'] = '죄송합니다. 어떤 이메일 주소로도이 이메일을 찾지 못했습니다.';
$lang ['forgot_success'] = '받은 편지함 (스팸 메일 포함)을 확인해주세요. 비밀번호 복구 링크를 보냈습니다. 링크는 2 시간 이내에 존재합니다.';
$lang ['reset_pw_success'] = '비밀번호를 성공적으로 변경하면 로그인 할 수 있습니다.';
$lang ['forgot_password'] = '비밀번호 분실';

//계기반
$lang ['index_title_h3'] = '투자와 수입은 쉽습니다.';
$lang ['index_item_1'] = '최소 입금액은 $ 1입니다.';
$lang ['index_item_2'] = '자본 확충 이익을 자동으로 얻습니다.';
$lang ['index_item_3'] = '각 추천에 대한 10 %와 같은 독특한 파트너십 프로그램';
$lang ['index_title_h3_2'] = '우리의 일';
$lang ['index_item_7'] = '우리는 최고의 투자자들을 모으고 있습니다. 우리는 여전히 가장 힘든 시장에서 돈을 벌어 투자 수익을 얻습니다.';
$lang ['index_item_8'] = '우리는 약 50 개의 암호를 연구하고 투자하고 있습니다.';