<?php
//OTHER
$lang['confirm'] 				= 'Xác nhận';
$lang['status'] 				= 'Trạng thái';
$lang['amount'] 				= 'Số tiền';
$lang['empty'] 					= 'Trống';
$lang['balance'] 				= 'Số dư';
$lang['wallet'] 				= 'Ví tiền';
$lang['address'] 				= 'Địa chỉ';
$lang['enter'] 					= 'Nhập vào';
$lang['wrong_captcha'] 			= 'Sai mã bảo mật!';
$lang['time'] 					= 'Thời gian';
$lang['type'] 					= 'Loại';
$lang['cancel'] 				= 'Hủy bỏ';
$lang['cannot'] 				= 'Không thể';
$lang['close'] 					= 'Đóng';
$lang['copy'] 					= 'Sao chép';
$lang['send'] 					= 'Gửi';

//404 PAGE
$lang['404_title'] 				= 'TRANG BỊ LỖI';
$lang['404_note'] 				= 'Trang web này không thể tìm thấy.';

//LOGIN & REGISTER & LOGOUT
$lang['login'] 					= 'Đăng nhập';
$lang['register'] 				= 'Đăng ký';
$lang['logout'] 				= 'Đăng xuất';
$lang['login_register'] 		= 'Tham gia ngay';
$lang['your_password'] 			= 'Mật khẩu của bạn:';
$lang['repeat_password'] 		= 'Nhập lại mật khẩu:';
$lang['register_note'] 			= 'Tôi đọc Thỏa thuận của Người dùng và hiểu tất cả các rủi ro liên quan đến đầu tư có năng suất cao.';
$lang['register_success'] 		= 'Đăng ký thành công, bạn đã có thể đăng nhập.';
$lang['login_success'] 			= 'Chuyển hướng sau 1 giây hoặc nhấp vào';
$lang['here'] 					= 'đây';

//MENU
$lang['home'] 					= 'Đầu tư và kiếm tiền trên mạng';
$lang['dashboard'] 				= 'Bảng Điều Khiển';
$lang['deposit'] 				= 'Gửi tiền';
$lang['withdraw'] 				= 'Rút tiền';
$lang['myWallet'] 				= 'Ví của tôi';
$lang['history'] 				= 'Lịch sử';
$lang['myAccount'] 				= 'Tài khoản';
$lang['myReferrals'] 			= 'Giới thiệu';
$lang['ratesnlimits'] 			= 'Giá & giới hạn';
$lang['contactus'] 				= 'Liên hệ';
$lang['agreement'] 				= 'Hợp đồng';

//DASHBOARD || HOME
$lang['total_balance'] 			= 'Tổng số dư';
$lang['interest_money'] 		= 'Tiền lãi';
$lang['interest_after'] 		= 'Tiền lãi tăng sau';
$lang['you_earn'] 				= 'Bạn nhận:';
$lang['note_interest'] 			= '*Ghi chú: Tiền lãi được thêm vào lúc 00:00 (GMT + 0)';
$lang['hold'] 					= 'Tiền tạm giữ';
$lang['note_hold'] 				= 'Sẽ hoàn thành trong vòng 12h';
$lang['parameters'] 			= 'Thông số của bạn';
$lang['deposit_interest'] 		= 'Lãi suất tiền gửi mỗi ngày';
$lang['deposit_interest_note'] 	= 'Nhận 1.3% mỗi ngày';
$lang['my_referrer'] 			= 'Giới thiệu';
$lang['my_referrer_note'] 		= 'Số lượng bạn bè được giới thiệu và kích hoạt thành công.';

//DEPOSIT
$lang['select_payment'] 		= 'Lựa chọn hệ thống thanh toán:';
$lang['select_one_wallet'] 		= 'Chọn một ví';
$lang['enter_amount'] 			= 'Nhập vào số tiền:';
$lang['deposit_limit'] 			= 'Giới hạn gửi';
$lang['deposit_amount'] 		= 'Số tiền gửi';
$lang['deposit_history'] 		= 'Lịch sử gửi';
$lang['go_to_wallet'] 			= 'Đi tới cài đặt ví';
$lang['cannot_deposit'] 		= 'Bạn không thể gửi tiền khi chưa cập nhật ví tiền của mình !';
$lang['press_the_button'] 		= 'Nhấn vào nút';
$lang['to_payment_now'] 		= 'để thanh toán ngay';
$lang['modal_note'] 			= 'Vui lòng không thay đổi số tiền, có thể ảnh hưởng đến giao dịch của bạn.';
$lang['use_address']			= 'Dùng địa chỉ:';
$lang['transfer']				= 'Chuyển khoản:';
$lang['to']						= 'đến';
$lang['after_transferring']		= 'Nhấn vào nút "OK" sau khi chuyển khoản thành công.';

//WITHDRAW
$lang['withdraw_limit'] 		= 'Giới hạn rút';
$lang['withdraw_amount'] 		= 'Số tiền rút';
$lang['withdraw_history'] 		= 'Lịch sử rút tiền';
$lang['cannot_withdraw'] 		= 'Bạn không thể rút tiền khi chưa cập nhật ví tiền của mình !';
$lang['not_enough_withdraw'] 	= 'Không đủ số dư để rút tiền!';
$lang['success_withdraw'] 		= 'Rút tiền thành công, bạn sẽ nhận được tiền trong vòng 12h.';
$lang['invaild_wallet'] 		= 'Ví không hợp lệ!';
$lang['invaild_amount'] 		= 'Số tiền không hợp lệ!';

//WALLET
$lang['wallet_note'] 			= 'Vui lòng nhập chính xác, trong trường hợp thanh toán sai chúng tôi không thể giúp bạn. Bạn có thể cập nhật một hoặc nhiều ví tại một thời điểm.';
$lang['update_wallet_success'] 	= 'Cập nhật thành công địa chỉ ví';
$lang['address_wallet_invalid'] = 'Địa chỉ ví không hợp lệ';

//TRANSACTION
$lang['take_image'] 			= 'Tạo một hình ảnh';
$lang['confirm_cancel'] 		= 'Xác nhận hủy bỏ';
$lang['transaction_proof'] 		= 'Bằng Chứng Giao Dịch';
$lang['cancel_note'] 			= 'Bạn có chắc chắn muốn xóa việc thu hồi không ?, chỉ có thể huỷ bỏ việc rút tiền sau 10 phút, bạn sẽ được cộng số tiền hoàn lại.';

//ACCOUNT
$lang['account_verification'] 	= 'Xác nhận tài khoản';
$lang['account_verified'] 		= 'Tài khoản của bạn đã được xác nhận.';
$lang['account_not_verified'] 	= 'Tài khoản của bạn chưa được xác nhận, bạn cần ít nhất một lần gửi tiền để xác nhận tài khoản.';
$lang['your_email'] 			= 'Email của bạn:';
$lang['deposit_now'] 			= 'Gửi tiền ngay';
$lang['change_password'] 		= 'Đổi mật khẩu';
$lang['old_password'] 			= 'Mật khẩu cũ';
$lang['new_password'] 			= 'Mật khẩu mới';
$lang['confirm_password'] 		= 'Xác nhận mật khẩu';
$lang['same_password'] 			= 'Xác nhận mật khẩu không giống nhau.';
$lang['incorrect_password'] 	= 'Mật khẫu cũ sai.';
$lang['allow_password'] 		= 'Chỉ cho phép chữ cái, số và một số ký tự! @ # $% ^ & *, độ dài từ 6 đến 32.';
$lang['change_password_note'] 	= '*Ghi chú: Nếu bạn đổi mật khẩu, bạn sẽ cần phải đăng nhập lại.';

//REFERERAL
$lang['referral_link'] 			= 'Liên kết giới thiệu';
$lang['copy_reffereral_link'] 	= 'Sao chép liên kết giới thiệu dưới đây:';
$lang['referral_list'] 			= 'Danh sách giới thiệu';
$lang['activated'] 				= 'Đã kích hoạt';
$lang['reg_date'] 				= 'Ngày đăng ký';

//WIDGET RIGHT
$lang['latest_transaction'] 	= 'Giao dịch mới nhất';
$lang['active_investors'] 		= 'Nhà đầu tư hoạt động:';
$lang['we_accept'] 				= 'Chúng tôi chấp nhận';
$lang['deposit_and_withdraw'] 	= 'Gửi tiền và rút tiền';
$lang['yield_calculator'] 		= 'Máy tính năng xuất';
$lang['term_deposit'] 			= 'Thời gian đầu tư (ngày):';
$lang['automatic_recap'] 		= 'Tái cấp vốn tự động.';
$lang['every_withdraw'] 		= 'Tôi muốn rút tiền lãi hàng ngày.';
$lang['your_profit'] 			= 'Tiền lãi của bạn:';
$lang['your_absolute'] 			= 'Số dư tổng của bạn:';
$lang['yield_calculator_note'] 	= 'Máy tính năng xuất cho bạn biết trước kế hoạch đầu tư (đối với 1.3%/ngày).';

//SUPPORT
$lang['your_message'] 			= 'Tin nhắn của bạn:';
$lang['support_note'] 			= 'Vui lòng kiểm tra kỹ email của bạn và chúng tôi sẽ trả lời cho câu hỏi của bạn một cách chính xác và càng nhanh càng tốt (như thường lệ trong 12 giờ).';
$lang['wrong_email'] 			= 'Định dạng email sai!';
$lang['message_invalid'] 		= 'Tin nhắn không được để trống.';
$lang['support_success'] 		= 'Xin chúc mừng! Thông điệp của bạn đã được gửi đến nhóm hỗ trợ. Bạn sẽ nhận được câu trả lời trong 12 giờ tới email của bạn.';

//RATE & LIMIT
$lang['interest_per_day'] 		= 'Lợi nhuận tiền gửi mỗi ngày';
$lang['interest_rate'] 			= 'Lãi suất';
$lang['daily_limit'] 			= 'Giới hạn hàng ngày';
$lang['fees'] 					= 'Phí';
$lang['deposit_fees'] 			= 'Phí gửi tiền';
$lang['withdraw_fees'] 			= 'Phí rút tiền';
$lang['deposit_24h'] 			= 'Số yêu cầu gửi tiền trong 24h';
$lang['withdraw_24h'] 			= 'Số yêu cầu rút tiền trong 24h';

//QUÊN MẬT KHẨU
$lang ['forgot_wrong_email'] 	= 'Xin lỗi, chúng tôi không tìm thấy email này với bất kỳ tài khoản nào';
$lang ['forgot_success'] 		= 'Hãy kiểm tra hộp thư đến của bạn (bao gồm cả thư spam), chúng tôi đã gửi một liên kết khôi phục mật khẩu, liên kết này tồn tại trong vòng 2 giờ.';
$lang ['reset_pw_success']		= 'Thay đổi mật khẩu thành công, bạn có thể đăng nhập.';
$lang['forgot_password'] 		= 'Quên mật khẩu';

//DASHBOARD
$lang['index_title_h3'] 		= 'Đầu tư và kiếm tiền dễ dàng.';
$lang['index_item_1'] 			= 'Tiền đầu tư tối thiểu là 1$, lợi nhuận tối thiểu là 1.3% / mỗi ngày.';
$lang['index_item_2'] 			= 'Tiền lãi được tự động tính chung vào tiền gốc.';
$lang['index_item_3'] 			= 'Bất cứ người dùng nào đầu tư vào hệ thống được bạn giới thiệu, bạn sẽ được nhận 10% số tiền trọn đời.';
$lang['index_title_h3_2'] 		= 'Công việc của chúng tôi';
$lang['index_item_7'] 			= 'Chúng tôi là một tập hợp các nhà đầu tư tốt nhất, chúng tôi sử dụng tiền của nhà đầu tư để kiếm thêm lợi nhuận, thậm chí tại thị trường khó khăn nhất, chúng tôi vẫn là những người chiến thắng.';
$lang['index_item_8'] 			= 'Chúng tôi đang nghiên cứu và đầu tư với khoảng 50 đồng tiền kỹ thuật số.';