<?php
//其他
$lang ['confirm'] ='确认';
$lang ['status'] ='状态';
$lang ['amount'] ='金额';
$lang ['empty'] ='空';
$lang ['balance'] ='平衡';
$lang ['wallet'] ='钱包';
$lang ['address'] ='地址';
$lang ['enter'] ='输入';
$lang ['wrong_captcha'] ='错误captcha！';
$lang ['time'] ='时间';
$lang ['type'] ='类型';
$lang ['cancel'] ='取消';
$lang ['can not'] ='不能';
$lang ['close'] ='关闭';
$lang ['copy'] ='复制';
$lang ['send'] ='发送';

// 404 PAGE
$lang ['404_title'] ='错误页';
$lang ['404_note'] ='未找到此网页。';

//登录＆注册＆注销
$lang ['login'] ='登录';
$lang ['register'] ='注册';
$lang ['logout'] ='注销';
$lang ['login_register'] ='立即加入';
$lang ['your_password'] ='您的密码：';
$lang ['repeat_password'] ='重复密码：';
$lang ['register_note'] ='我读了用户\的协议，并了解可能与高收益的投资相关联的所有风险“。';
$lang ['register_success'] ='成功注册，您已经可以登录。';
$lang ['login_success'] ='1秒后重定向或点击';
$lang ['here'] ='这里';

//菜单
$lang ['home'] ='在线投资和赚钱';
$lang ['dashboard'] ='仪表板';
$lang ['deposit'] ='存款';
$lang ['withdraw'] ='撤销';
$lang ['myWallet'] ='我的钱包';
$lang ['history'] ='历史';
$lang ['myAccount'] ='我的帐户';
$lang ['myReferrals'] ='我的推荐';
$lang ['ratesnlimits'] ='利率和限额';
$lang ['contactus'] ='联系我们';
$lang ['agreement'] ='协议';

// DASHBOARD || HOME
$lang ['total_balance'] ='总余额';
$lang ['interest_money'] ='利息资金';
$lang ['interest_after'] ='利率在\'之后增加';
$lang ['you_earn'] ='您赚取：';
$郎[ 'note_interest'] ='*注：兴趣是在00:00加入（GMT + 0）';
$lang ['hold'] ='暂时保留';
$lang ['note_hold'] ='12小时内完成';
$lang ['parameters'] ='您的参数';
$lang ['deposit_interest'] ='每天存款利率';
$lang ['deposit_interest_note'] ='每日盈利1.3％';
$lang ['my_referrer'] ='我的推荐人';
$lang ['my_referrer_note'] = '成功引入和激活的朋友数量。';

//存款
$lang ['select_payment'] ='选择您的支付系统类型：';
$lang ['select_one_wallet'] ='选择一个钱包';
$lang ['enter_amount'] ='输入添加金额：';
$lang ['deposit_limit'] ='存款限额';
$lang ['deposit_amount'] ='存款金额';
$lang ['deposit_history'] ='存款历史';
$lang ['go_to_wallet'] ='去你的电子钱包';
$lang ['cannot_deposit'] ='您不能更新您的钱包不能存款！';
$lang ['press_the_button'] ='按下按钮';
$lang ['to_payment_now'] ='现在付款';
$lang ['modal_note'] = '请不要改变量，这可能会影响您的交易。';
$lang ['use_address'] ='使用地址：';
$lang ['transfer'] ='转账：';
$lang ['to'] ='to';
$lang ['after_transferring'] ='转移后点击“确定”按钮。';

//提取
$lang ['withdraw_limit'] ='取款限制';
$lang ['withdraw_amount'] ='提取金额';
$lang ['withdraw_history'] ='撤回历史';
$lang ['cannot_withdraw'] ='如果不更新您的钱包，就无法取款！ ';
$lang ['not_enough_withdraw'] ='没有足够的退出！';
$lang ['success_withdraw'] ='成功退出，您将在12小时内收到钱。';
$lang ['invaild_wallet'] ='Invaild钱包！';
$lang ['invaild_amount'] ='入金额！';

//钱包
$lang ['wallet_note'] = '请输入正确的，在错误的支付，我们不能帮你的情况。您可以一次更新一个或多个钱包。';
$lang ['update_wallet_success'] ='成功更新钱包地址。';
$lang ['address_wallet_invalid'] ='地址钱包无效';

// TRANSACTION
$lang ['take_image'] ='拍一张图片';
$lang ['confirm_cancel'] ='确认取消';
$lang ['transaction_proof'] ='交易证明';
$lang ['cancel_note'] = '您确定要删除的退出，只能取消您的提款10分钟后，你将被记入退款的金额是多少？。';

//帐户
$lang ['account_verification'] ='帐户验证';
$lang ['account_verified'] ='您的帐户已被验证。';
$lang ['account_not_verified'] ='您的帐户尚未激活，您需要先存款才能激活您的帐户。';
$lang ['your_email'] ='您的电子邮件：';
$lang ['deposit_now'] ='立即存款';
$lang ['change_password'] ='更改密码';
$lang ['old_password'] ='旧密码';
$lang ['new_password'] ='新密码';
$lang ['confirm_password'] ='确认密码';
$lang ['same_password'] ='确认密码不一样。';
$lang ['incorrect_password'] ='旧密码不正确。';
$lang ['allow_password'] ='只允许字母和数字以及一些字符！ @＃$％^＆*，长度6到32。';
$lang ['change_password_note'] ='*注意：如果您更改密码，则需要重新登录。';

// REFERERAL
$lang ['referral_link'] ='推荐链接';
$lang ['copy_reffereral_link'] ='复制您​​的推荐链接如下：';
$lang ['referral_list'] ='推荐列表';
$lang ['activated'] ='激活';
$lang ['reg_date'] ='注册日期';

// WIDGET RIGHT
$lang ['latest_transaction'] ='最新交易';
$lang ['active_investors'] ='积极的投资者：';
$lang ['we_accept'] ='我们接受';
$lang ['deposit_and_withdraw'] ='存款和提款';
$lang ['yield_calculator'] ='收益计算器';
$lang ['term_deposit'] ='存款期限（天）：';
$lang ['automatic_recap'] ='自动重新注册。';
$lang ['every_withdraw'] ='我想每天提取利润。';
$lang ['your_profit'] ='您的真正利润';
$lang ['your_absolute'] ='您的绝对平衡：';
$lang ['yield_calculator_note'] ='收益率计算器提前告诉您投资计划（1.3％/天）。';

//支持
$lang ['your_message'] ='您的留言：';
$lang ['support_note'] ='请仔细检查您的电子邮件，我们会尽快回复您的问题并尽可能快（如通常在12小时内）。';
$lang ['wrong_email'] ='错误的电子邮件格式！';
$lang ['message_invalid'] ='消息不能为空！';
$lang ['support_success'] ='恭喜！您的消息已发送给支持团队。你会在12个小时内收到你的邮件回复。';

//速度和限制
$lang ['interest_per_day'] ='每天存款利率';
$lang ['interest_rate'] ='利率';
$lang ['daily_limit'] ='每日限制';
$lang ['fees'] ='费用';
$lang ['deposit_fees'] ='存款费';
$lang ['withdraw_fees'] ='取消费用';
$lang ['deposit_24h'] ='24小时保证金数量';
$lang ['withdraw_24h'] ='24小时内提款数量';

//忘记密码
$lang ['forgot_wrong_email'] ='对不起，我们没有找到任何帐户的电子邮件。';
$lang ['forgot_success'] ='请检查您的收件箱（包括垃圾邮件），我们已发送密码恢复链接，该链接在2小时内存在。';
$lang ['reset_pw_success'] ='成功更改密码，您可以登录。';
$lang ['forgot_password'] ='忘记密码';

//仪表板
$lang ['index_title_h3'] ='投资和赚钱很容易';
$lang ['index_item_1'] ='最低存款只有1美元。';
$lang ['index_item_2'] ='自动实现利润重组。';
$lang ['index_item_3'] ='独特的合作伙伴计划，例如每个转介存款的10％。';
$lang ['index_title_h3_2'] ='我们的工作';
$lang ['index_item_7'] ='我们是最好的投资者的集合，我们用你的钱投资赚取利润，即使在最艰难的市场中，我们仍然是赢家。';
$lang ['index_item_8'] ='我们正在研究和投资约50密码。';