<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-lg-<?php echo (isset($menu_right) ? '12' : '8'); ?>">
		<div class="card mb-4">
			<div class="card-block" id="register-div">
				<div class="row">
					<div class="col-md-12 col-lg-8">
						<form method="POST" id="form-wallet">
						  <?php if($this->session->flashdata('error') != '') { ?>
							<div class="alert bg-danger" role="alert">
								<em class="fa fa-warning mr-2"></em> 
								<?php echo $this->session->flashdata('error'); ?>
							</div>
						  <?php } ?>
						  <?php if($this->session->flashdata('status') != '') { ?>
							<div class="alert bg-success" role="alert">
								<em class="fa fa-check-circle mr-2"></em> 
								<?php echo $this->session->flashdata('status'); ?>
							</div>
						  <?php } ?>
						<p class="lead"><?php echo $this->lang->line('wallet_note'); ?></p>
						<?php foreach ($wallet_cat as $key => $item) { ?>
						  <div class="form-group" id="block-perfect">
						    <label for="<?php echo $item['simplename']; ?>">
						    	<img src="<?php echo base_url('images/icon/wallet/small/'.$item['simplename'].'.png');?>"> <?php echo $item['name']; ?> <?php echo $this->lang->line('address',2); ?>:
						    </label>
						    <input type="text" value="<?php echo $user_wallet[$key]['address']; ?>" class="form-control" id="<?php echo $item['simplename']; ?>" name="<?php echo $item['id']; ?>" placeholder="<?php echo $this->lang->line('enter'); ?> <?php echo $item['name']; ?> <?php echo $this->lang->line('address', 2); ?>...">
							<span class="text-danger form-text" id="<?php echo $item['simplename']; ?>-text">
								<?php echo ($this->session->flashdata($item['simplename']) != '' ? $this->session->flashdata($item['simplename']) : ''); ?>
							</span>
						  </div>
						  <?php } ?>
						  <div class="form-group">
						  	<div class="g-recaptcha" data-sitekey="6LfHkD4UAAAAABO6R-6b_EZI890_QJoFqLHjKaXb"></div> 
							<span class="text-danger form-text" id="captcha-text">
								<?php echo ($this->session->flashdata('captcha') != '' ? $this->session->flashdata('captcha') : ''); ?>
							</span>
						  </div>
						  <div class="form-group">
						  	<button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-check"></i> <?php echo $this->lang->line('confirm'); ?></button> 
						  </div>
						</form>
					</div>
					<div class="col-md-12 col-lg-4">
						<div class="row">
							<?php foreach ($wallet_cat as $key => $item) { ?>
							<div class="col-lg-12 mb-3 col-6">
								<div class="card">
									<div class="card-header text-center" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
										<img src="<?php echo base_url('images/icon/wallet/small/'.$item['simplename'].'.png'); ?>"> 
										<strong><?php echo $item['name']; ?></strong>
									</div>
									<div class="card-block text-center">
										<p class="lead"><i class="fa fa-usd"></i> <strong><?php echo $user_wallet[$key]['money']; ?></strong></p>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>