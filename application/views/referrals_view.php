<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-lg-<?php echo (isset($menu_right) ? '12' : '8'); ?>">
		<div class="card mb-4">
			<div class="card-block">
				<div class="text-center">
					<h4><?php echo $this->lang->line('referral_link'); ?></h4>
					<div class="text" id="animated-copy"><?php echo $this->lang->line('copy_reffereral_link'); ?></div>
				</div>
				<div class="input-group mb-2 mr-sm-2 mb-sm-0">
					<div class="input-group-addon bg-primary" style="color: white;"><i class="fa fa-link"></i></div>
					<input type="text" class="form-control form-control-lg" value="<?php echo base_url($lng.'/home?ref='.$user['id']); ?>" id="link-ref">
					<div class="input-group-addon bg-primary" style="color: white;" onclick="copyTextInput('link-ref', 'animated-copy');"><i class="fa fa-clone fa-lg"></i><?php echo $this->lang->line('copy'); ?></div>
				</div>	
			<hr>
			<?php echo $this->lang->line('index_item_3'); ?>
			<br>
			<h4><?php echo $this->lang->line('referral_list'); ?></h4>
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>#</th>
				      <th>Email</th>
				      <th><?php echo $this->lang->line('activated'); ?></th>
				      <th><?php echo $this->lang->line('reg_date'); ?></th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php if($referrals_list != false) foreach ($referrals_list as $key => $value) { ?>
				    <tr>
				      <th scope="row"><?php echo ($key + 1); ?></th>
				      <td><?php echo $value['email']; ?></td>
				      <td><i class="fa <?php echo ($value['activated'] == 1 ? 'fa-check text-success' : 'fa-times text-danger'); ?>"></i></td>
				      <td><?php echo $value['reg_date']; ?></td>
				    </tr>
					<?php }  ?>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>