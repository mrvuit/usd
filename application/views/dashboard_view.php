<?php $this->load->view('inc/header'); ?>

	<div class="col-md-12 col-xl-8" id="body-content">
		<?php if(1 == 2) { ?>
		<div class="alert alert-dismissible fade show" role="alert" style="background: rgba(193, 66, 66, 0.6);">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span>&times;</span>
			</button>
			<strong><i class="fa fa-exclamation-circle"></i> Verification:</strong> Verify Your Account. 
			<a href="<?php echo base_url('account'); ?>"><u>Click here!</u></a>
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-lg-5">

				<div class="card text-center mb-3" style="height: 250px;">
					<h4 class="card-header card-inverse card-primary" style="color: white;"><?php echo $this->lang->line('total_balance'); ?></h4>
					<div class="card-block">
					<h3><i class="fa fa-usd"></i> <?php echo number_format($amount,2); ?></h3>
						<hr>
						<a class="btn btn-primary btn-block" href="<?php echo base_url($lng.'/deposit'); ?>">
							<em class="fa fa-plus-circle"></em> <?php echo $this->lang->line('deposit'); ?>
						</a>
						<a class="btn btn-primary btn-block" href="<?php echo base_url($lng.'/withdraw'); ?>">
							<em class="fa fa-arrow-circle-o-down"></em> <?php echo $this->lang->line('withdraw'); ?> 
						</a>
			        </div>
				</div>

			</div>
			<div class="col-md-12 col-sm-12 col-lg-7">
				<div class="card text-center mb-3" style="height: 250px;">
					<h4 class="card-header card-inverse card-primary" style="color: white;"><?php echo $this->lang->line('interest_money'); ?></h4>
					<div class="card-block">
						<h4 class="card-title"><?php echo $this->lang->line('interest_after'); ?><sup>*</sup>:</h4>
						<h3 id="count-down"><i class="fa fa-clock-o"></i> <?php echo $countd_view['time']; ?></h3>
						<h4 class="lead"><?php echo $this->lang->line('you_earn'); ?> <strong><i class="fa fa-usd"></i> <?php echo number_format($interest, 2); ?></strong></h4>
						<small class="small"><?php echo $this->lang->line('note_interest'); ?></small>
						<div class="progress">
							<div class="progress-bar bg-primary" style="width: <?php echo $countd_view['progressbar']; ?>%" role="progressbar" aria-valuenow="<?php echo $countd_view['progressbar']; ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $countd_view['progressbar']; ?>%</div>
						</div>
			        </div>
				</div>
			</div>
		</div>

		<div class="row">
			<?php foreach ($wallet_cat as $key => $item) { ?>
			<div class="col-lg-6 mb-6 col-6">
				<div class="card">
					<div class="card-header text-center" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
						<img src="<?php echo base_url('images/icon/wallet/small/'.$item['simplename'].'.png'); ?>"> 
						<strong><?php echo $item['name']; ?></strong>
					</div>
					<div class="card-block text-center">
						<p class="lead"><i class="fa fa-usd"></i> <strong><?php echo number_format($user_wallet[$key]['money'], 2); ?></strong></p>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>



		<div class="row">
			<div class="col-md-12 text-center">
				<div style="background: #fff;padding: 3px;border-radius: 5px;">
			  <table class="table table-bordered">
			    <thead>
					<tr>
						<th colspan="3" class="text-center">
							<i class="fa fa-lock"></i> <?php echo $this->lang->line('hold'); ?>
						</th>
					</tr>
					<tr>
						<th class="text-center" style="width: 50%;">
							<i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('deposit'); ?>
						</th>
						<th class="text-center" style="width: 50%;">
							<i class="fa fa-arrow-circle-o-down"></i> <?php echo $this->lang->line('withdraw'); ?>
						</th>
					</tr>
			    </thead>
			    <tbody>
					<tr>
						<td class="text-center"><p class="lead"><i class="fa fa-usd"></i> <?php echo $temporary['deposit']; ?></p></td>
						<td class="text-center"><p class="lead"><i class="fa fa-usd"></i> <?php echo $temporary['withdraw']; ?></p></td>
					</tr>
					<tr>
						<td colspan="2">
							<span class="text-muted"><?php echo $this->lang->line('note_hold'); ?></span><br>
							<a href="<?php echo base_url($lng.'/transactions'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-list-alt"></i> <?php echo $this->lang->line('history'); ?></a>
						</td>
					</tr>
			    </tbody>
			  </table>
			  
			  </div>
			  <hr>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card mb-4">
					<div class="card-block">
						<h3 class="card-title"><?php echo $this->lang->line('parameters'); ?></h3>
						<h6 class="card-subtitle mb-2 text-muted"><?php echo $user['email']; ?></h6>
						<div class="divider" style="margin-top: 1rem;"></div>
						<div class="articles-container">
							<div class="article border-bottom">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-2 date">
											<div class="large">1.3</div>
											<div class="text-muted"><i class="fa fa-percent fa-lg"></i></div>
										</div>
										<div class="col-10">
											<h4><a href="<?php echo base_url($lng.'/deposit'); ?>"><?php echo $this->lang->line('deposit_interest'); ?></a></h4>
											<p><?php echo $this->lang->line('deposit_interest_note'); ?></p>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="divider" style="margin-top: 1rem;"></div>
						<div class="articles-container">
							<div class="article border-bottom">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-2 date">
											<div class="large"><?php echo $referrer; ?></div>
											<div class="text-muted"><i class="fa fa-users"></i></div>
										</div>
										<div class="col-10">
											<h4><a href="<?php echo base_url($lng.'/referrals'); ?>"><?php echo $this->lang->line('my_referrer'); ?></a></h4>
											<p><?php echo $this->lang->line('my_referrer_note'); ?></p>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>