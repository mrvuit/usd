<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12">
		<div class="card mb-4">
			<div class="card-block">
				<?php if($available == false) { ?>
				<div class="alert bg-warning" role="alert">
					<em class="fa fa-exclamation-triangle mr-2"></em>
					<?php echo $this->lang->line('cannot_withdraw'); ?>
				</div>				
				<a href="<?php echo base_url($lng.'/wallet'); ?>" class="btn btn-primary"><i class="fa fa-server"></i> <?php echo $this->lang->line('go_to_wallet'); ?></a>
				<?php } else { ?>
				<div class="row">
					<div class="col-md-12 col-lg-6">
						<form method="POST" id="form-withdraw">
							<?php if($this->session->flashdata('error') != '' || $this->session->flashdata('success') != '') { ?>
							<div class="alert bg-<?php echo ($this->session->flashdata('error') != '' ? 'danger' : 'success'); ?>" role="alert">
								<em class="fa fa-warning mr-2"></em> 
								<?php echo ($this->session->flashdata('error') != '' ? $this->session->flashdata('error') : $this->session->flashdata('success')); ?>
							</div>
							<?php } ?>
				    		<div class="form-group">
							    <label for="money">
							    	<i class="fa fa-usd"></i>
							    	<?php echo $this->lang->line('select_payment'); ?>
							    </label>
							    <select class="form-control form-control-lg" id="wallet-withdraw" name="wallet">
							    	<option value="0"><?php echo $this->lang->line('select_one_wallet'); ?></option>
							    	<?php foreach ($wallet_cat as $key => $item) { ?>
							    		<?php if(!empty($user_wallet[$key]['address'])) { ?>
							    		<option value="<?php echo $item['id']; ?>"><?php echo $wallet_cat[$key]['name']; ?></option>
							    		<?php } ?>
							    	<?php } ?>
							    </select>
							</div>

							<div class="form-group">
							    <label for="money">
							    	<i class="fa fa-usd"></i>
							    	<?php echo $this->lang->line('enter_amount'); ?>
							    </label>
					    		<input type="number" step="any" placeholder="$" class="form-control form-control-lg" id="input-money" name="amount" required="required" disabled="disabled" autocomplete="off">
					    	</div>

							<table class="table table-bordered" style="table-layout: fixed;">
							    <tbody>
							      <tr>
							        <td style="width: 30%;"><strong><?php echo $this->lang->line('wallet'); ?></strong></td>
							        <td id="wallet_name"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('address'); ?></strong></td>
							        <td id="wallet_address" style="word-wrap: break-word;"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('balance'); ?></strong></td>
							        <td id="wallet_balance" style="word-wrap: break-word;"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('withdraw_limit'); ?></strong></td>
							        <td id="withdraw_limit"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('withdraw_amount'); ?></strong></td>
							        <td id="withdraw_deposit_amount"></td>
							      </tr>
							    </tbody>
							  </table>

							<div class="form-group">
							  	<button type="submit" name="submit" class="btn btn-primary btn-lg btn-block mt-2" id="submit-confirm" disabled="disabled">
							  		<i class="fa fa-paper-plane" aria-hidden="true"></i> <?php echo $this->lang->line('confirm'); ?>
							  	</button> 
						  	</div>
						</form>
			    	</div>
			    	<div class="col-md-12 col-lg-6">
			    		<div class="row">
			    			<div class="col-md-12">
								<div class="card">
					 				<h4 class="card-header text-center"><?php echo $this->lang->line('withdraw_history'); ?></h4>
									<div class="card-block" style="padding: 0px;">
						    			<div style="overflow: scroll;max-height: 430px;height: 430px;">
											<table class="table table-striped table-sm table-bordered">
											  <thead>
											    <tr>
											      <th style="width: 10%;">Id</th>
											      <th style="width: 30%;"><?php echo $this->lang->line('wallet'); ?></th>
											      <th style="width: 30%;"><?php echo $this->lang->line('status'); ?></th>
											      <th style="width: 30%;"><?php echo $this->lang->line('amount'); ?></th>
											    </tr>
											  </thead>
											  <tbody>
											  	<?php if($withdraw_htr != false)  foreach ($withdraw_htr as $key => $item) {  ?>
											    <tr>
											      <th>#<?php echo $item['id']; ?></th>
											      <td><img src="/images/icon/wallet/small/<?php echo $item['wallet_cat']['simplename']; ?>.png">  <?php echo $item['wallet_cat']['name']; ?></td>
											      <td><?php echo $item['status']; ?></td>
											      <td>$<?php echo $item['money']; ?></td>
											    </tr>
											    <?php } ?>
											    <?php if($withdraw_htr == false) { ?>
											    <tr><td colspan="4" class="text-center"><?php echo $this->lang->line('empty'); ?> !</td></tr>
											    <?php } ?>
											  </tbody>
											</table>
						    			</div>
						        	</div>
								</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
				<?php }  ?>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>