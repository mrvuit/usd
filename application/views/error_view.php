<?php $this->load->view("inc/header"); ?>

	<div class="col-md-12 col-xl-12">
		<div class="jumbotron text-center">
			<img src="<?php echo base_url('images/404.png'); ?>">
			<h1><?php echo $this->lang->line('404_note'); ?></h1>
			<a href="<?php echo base_url($lng.'/home'); ?>" class="btn btn-primary btn-lg"><i class="fa fa-home"></i> <?php echo $this->lang->line('home'); ?></a>
			<a href="<?php echo base_url($lng.'/support'); ?>" class="btn btn-warning btn-lg" style="color: white;"><i class="fa fa-envelope-o"></i> <?php echo $this->lang->line('contactus'); ?></a>
		</div>
	</div>

<?php $this->load->view("inc/footer"); ?>