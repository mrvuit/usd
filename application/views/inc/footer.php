					<?php if(!isset($menu_right)) { ?>
					<div class="col-md-12 col-xl-4">
						<div class="card mb-4 content-sticky">
              <div class="card-block" style="padding-bottom: 0px;">
                  <table class="table table-striped table-sm table-bordered">
                    <tbody>
                      <tr class="text-center">
                        <td colspan="4"><i class="fa fa-money"></i> <strong><?php echo $this->lang->line('latest_transaction'); ?></strong></td>
                      </tr>
                      <?php for($i = 0; $i < 5; $i ++) { $rand = rand(1,2); ?>
                      <tr>
                        <td style="width: 30%;max-width:100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><img src="/images/icon/wallet/small/<?php echo ($rand == 1 ? 'bitcoin' : 'ethereum'); ?>.png"><?php echo ($rand == 1 ? 'Bitcoin' : 'Ethereum'); ?></td>
                        <td style="width: 30%;max-width:80px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><?php echo (rand(1,3) == 1 ? 'WITHDRAW' : 'DEPOSIT'); ?></td>
                        <td style="width: 30%;">$<?php echo number_format(rand(10, 1411),2); ?></td>
                      </tr>
                      <?php } ?>

                      <tr class="text-center">
                        <td colspan="3">
                          <a href="https://coinmarketcap.com/currencies/bitcoin/" title="Bitcoin" target="_blank">
                            <code>
                              <img src="<?php echo base_url('images/icon/wallet/small/bitcoin.png'); ?>">BTC:$<?php echo number_format($system['btc_price_usd'],2); ?>
                            </code>
                          </a>
                          <a href="https://coinmarketcap.com/currencies/ethereum/" title="ethereum" target="_blank">
                            <code>
                              <img src="<?php echo base_url('images/icon/wallet/small/ethereum.png'); ?>">ETH:$<?php echo number_format($system['eth_price_usd'],2); ?>
                            </code>
                          </a>
                        </td>
                      </tr>
                      <tr class="text-center">
                        <td colspan="4"><i class="fa fa-users text-success"></i> <strong><?php echo $this->lang->line('active_investors'); ?> <?php echo $all_user; ?></strong></td>
                      </tr>
                    </tbody>
                  </table>
            </div>
							<div class="card-block" style="padding-bottom: 0px;">
								<h4 class="card-title"><i class="fa fa-credit-card-alt"></i> <?php echo $this->lang->line('we_accept'); ?></h4>
								<h6 class="card-subtitle mb-2 text-muted"><?php echo $this->lang->line('deposit_and_withdraw'); ?></h6>
								<div class="user-progress" style="border: 2px solid #ddd;border-radius: 5px;padding: 10px;">
									<div id="logo-pay" class="carousel slide" data-ride="carousel" data-interval="3050" data-pause="hover">
									  <div class="carousel-inner" role="listbox" style="background: #fff;">

									    <div class="carousel-item active">
									      <a href="https://blockchain.info" target="_blank">
									      	<img class="d-block img-fluid" src="<?php echo base_url('images/bitcoin.png'); ?>" alt="payeer">
									      </a>
									    </div>

									    <div class="carousel-item">
									    	<a href="https://www.myetherwallet.com/" target="_blank">
									      		<img class="d-block img-fluid" src="<?php echo base_url('images/ethereum.png'); ?>" alt="perfectmoney">
									  		</a>
									    </div>
									    <div class="carousel-item">
									    	<a href="https://blockchain.info" target="_blank">
									      		<img class="d-block img-fluid" src="<?php echo base_url('images/bitcoin.png'); ?>" alt="bitcoin">
									      	</a>
									    </div>
									    <div class="carousel-item">
									    	<a href="https://www.myetherwallet.com/" target="_blank">
									      		<img class="d-block img-fluid" src="<?php echo base_url('images/ethereum.png'); ?>" alt="ethereum">
									  		</a>
									    </div>    

									  </div>
									  <a class="carousel-control-prev" href="#logo-pay" role="button" data-slide="prev">
									    <i class="fa fa-arrow-left fa-lg text-primary"></i>
									  </a>
									  <a class="carousel-control-next" href="#logo-pay" role="button" data-slide="next">
									    <i class="fa fa-arrow-right fa-lg text-primary"></i>
									  </a>
									</div>
								</div>	
              </div>
							<div class="card-block" style="padding-bottom: 0px;">

								<h4 class="card-title"><i class="fa fa-calculator"></i> <?php echo $this->lang->line('yield_calculator'); ?></h4>
								<h6 class="card-subtitle mb-2 text-muted"><?php echo $this->lang->line('yield_calculator_note'); ?></h6>
								<div class="form-group">
								<label for="formGroupExampleInput"><?php echo $this->lang->line('deposit_amount'); ?></label>
								<input id="deposit" type="number" class="form-control" value="10000" placeholder="USD" onkeyup="Calculator();">
								</div>
								<label for="formGroupExampleInput2"><?php echo $this->lang->line('term_deposit'); ?></label>
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="form-group">
											<input id="days" min="1" max="999" value="30" type="number" class="form-control" placeholder="days" onkeyup="Calculator();">
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="form-group">
											<button type="button" class="btn btn-primary btn-block" onclick="Calculator();"><i class="fa fa-refresh"></i> Load</button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label><input type="radio" name="recap" value="1" checked="checked"> <?php echo $this->lang->line('automatic_recap'); ?></label>
									<label><input type="radio" name="recap" value="2"> <?php echo $this->lang->line('every_withdraw'); ?></label>
								</div>
								<h5><?php echo $this->lang->line('your_profit'); ?></h5>
								<h4 class="display-4" style="font-size: 1.8rem;" id="realy-profit"></h4>
								<h5><?php echo $this->lang->line('your_absolute'); ?></h5>
								<h4 class="display-4" style="font-size: 1.8rem;" id="abs-balance"></h4>	
							</div>
						</div>
					</div>
					<?php } ?>
				</section>
				<section class="container-fluid">
				  <div class="row">
            <div class="col-md-12" style="background: #eee;margin-top: 10px;padding: 15px;border: 1px solid #ddd;font-weight: bold;bottom: 0;">
              <i class="fa fa-copyright"></i> <a href="<?php echo base_url(); ?>">MyPerDay.Com</a> 2017
            </div>    
          </div>
		    </section>
		</main>
	</div>
</div>
<noscript>
 For full functionality of this site it is necessary to enable JavaScript.
 Here are the <a href="https://www.enable-javascript.com/" target="_blank">
 instructions how to enable JavaScript in your web browser</a>.
</noscript>

	 <!-- Bootstrap core JavaScript
     http://www.vworldz.com/2017/09/telegram-group-link.html
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>/temp/js/jquery-3.2.1.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>/theme/js/jquery-1.7.1.min.js"></script> -->
    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>/temp/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>/temp/js/custom.js"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
    <?php  if(isset($countd)) { ?>
   		countDown(<?php echo $countd; ?>000);
   	<?php } ?>
    $( document).ready(function() {
      $('#wrapper').show();
        if($(window).width() > 1181 && $('#body-content').height() > $(".content-sticky").height()) {
          $(window).on("scroll", function() {
              $(window).scrollTop() > $(".content-sticky").height() - $(window).height() + 153 ? $(".content-sticky").addClass("affix") : $(".content-sticky").removeClass("affix"), 0 == $(window).scrollTop() && $(".content-sticky").removeClass("affix")
          });
        } else {
          $(".content-sticky").removeClass("affix");
        }
    });
    


      var reCAPTCHA;
      var onloadCallback = function() {
          reCAPTCHA = grecaptcha.render(document.getElementById('div-recaptcha'), {
              'sitekey': '6LfHkD4UAAAAABO6R-6b_EZI890_QJoFqLHjKaXb'
          });
      };

      window.takeScreenShot = function() {
          $('#div-img-transaction').show();
          $('#show-img').html('');
          html2canvas($('#transaction_list'), {
              backgroundColor: '#FFFFFF',
              allowTaint: false,
              useCORS: false,
              removeContainer: true,
              onrendered: function(canvas) {
                  var data = canvas.toDataURL();
                  $('#show-img').html('<img id="transaction_img" src="' + data + '" style="border:2px dashed #777;border-radius:15px;height:250px;max-width:100%;"><br><a class="btn btn-link"  download="transaction.png" href="' + data + '"><i class="fa fa-arrow-circle-down"></i> DOWNLOAD</a> <button class="btn btn-link" onclick="closeTransactionImg();"><i class="fa fa-times-circle-o"></i> CLOSE</button> ');
              },
              windowWidth: $("#transaction_list").width(),
              windowHeight: $("#transaction_list").height()
          });
      }

      function closeTransactionImg() {
          $('#div-img-transaction').hide();
      }
      //TRANSACTION
      function confirmCancelTs(id_transaction) {
        $('#id-cancel').html(id_transaction);
        $('#link-cancel').attr('href', '?cancel_id=' + id_transaction);
        $('#model-cancel').modal('show');
      }
      function copyAdressSystem() {
        copyText($('#'+ $("#wallet-deposit option:selected" ).text() + '-system-address').html());
        $('#copied').show();
      }
      function loadDataToModal() {
        $('#modal_wallet').html($('#wallet_name').html().replace('small', 'medium'));
        $('#modal_amount').html($('#withdraw_deposit_amount').html());
        $('#deposit-alert-error').hide();
        $('#Bitcoin-price').hide();
        $('#Ethereum-price').hide();
        $('#copied').hide();
        $('#Bitcoin-system-address').hide();
        $('#Ethereum-system-address').hide();
        if($("#wallet-deposit option:selected" ).text() == 'Bitcoin' || $("#wallet-deposit option:selected" ).text() == 'Ethereum') {
          $('#normal-payment').hide();
          $('#abnormal-payment').show();
          var price = $('#'+ $("#wallet-deposit option:selected" ).text() + '-price-usd').html().replace(',', '');
          $('#amount-abnormal').html(($('#input-money').val() / price).toString().substring(0,8) + " " + $("#wallet-deposit option:selected" ).text());
          $('#'+ $("#wallet-deposit option:selected" ).text() + '-price').show();
          $('#'+ $("#wallet-deposit option:selected" ).text() + '-system-address').show();
          $('#user_address').html($('#wallet_address').html());
        } else {
          $('#normal-payment').show();
          $('#abnormal-payment').hide();
        }
      }
      // DEPOSIT & WITHDRAW
      $('#input-money').change(function() {
          var money = parseFloat($('#input-money').val());
          money = money.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
          $('#display-money').html('<strong>Amount: <i class="fa fa-usd"></i>' + money + '</strong>');
          $('#input-money').val(parseFloat($('#input-money').val()).toFixed(2));
      });
      $('#ok_deposit_button').click(function() {
          $.ajax({
              url: "/ajax/saveTransaction",
              data: "user_id=" + $('#user_id').html() + "&money=" + $('#withdraw_deposit_amount').html().replace('$', '') + "&wallet=" + $('#wallet-deposit').val() + "&type=1",
              type: "POST",
              beforeSend: function() {
                $('#model-loading-icon').show();
              },
              success: function(response) {
                  var json = JSON.parse(response);
                  $('#model-loading-icon').hide();
                  if(json['status'] == 'false') {
                      $('#deposit-alert-error').show();
                      $('#text-error-deposit').html(json['messages']);
                  } else {
                      window.location.href = '/' + $('#language-key').html() + '/payment/' + json['system'];
                  }
              },
              error: function(response) {
                alert('ERROR');
              }
          });
      });
      $('#input-money').keyup(function() {
          var max = parseFloat($('#input-money').attr('max'));
          var min = parseFloat($('#input-money').attr('min'));
          var money = parseFloat($('#input-money').val());
          if (money > max) {
              $('#input-money').val(max);
          }
          if (isNaN(money)) {
              $("#submit-confirm").attr({
                  "disabled": true
              });
          } else {
              if (money >= min) {
                  $("#submit-confirm").attr({
                      "disabled": false
                  });
              } else {
                  $("#submit-confirm").attr({
                      "disabled": true
                  });
              }
          }
          $('#withdraw_deposit_amount').html('$' + parseFloat($('#input-money').val()).toFixed(2));
      });

      $('#wallet-deposit').change(function() {
          var option = $('#wallet-deposit').val();
          $('#input-money').val('');
          $("#submit-confirm").attr({
              "disabled": true
          });
          $("#input-money").attr({
              "disabled": true
          });
          $('#wallet_name').html('');
          $('#deposit_limit').html('');
          $('#wallet_address').html('');
          $('#withdraw_deposit_amount').html('');

          if(option != 0)
          $.ajax({
              url: "/ajax/getWalletCat/" + option + "/" + $('#user_id').html(),
              type: "GET",
              beforeSend: function() {

              },
              success: function(response) {
                  var json = JSON.parse(response);
                  if (json['status'] == 'true') {
                      var limit = json['wallet_cat']['deposit'].split('-');
                      $('#wallet_name').html('<img src="/images/icon/wallet/small/' + json['wallet_cat']['simplename'] + '.png">' + json['wallet_cat']['name']);
                      $('#deposit_limit').html('$' + limit[0] + " - $" + limit[1]);
                      $('#wallet_address').html(json['wallet']['address']);
                      $("#input-money").attr({
                          "max": limit[1],
                          "min": limit[0],
                          "disabled": false
                      });
                  } else {
                      $("#input-money").attr({
                          "disabled": true
                      });
                  }
              },
              error: function(response) {

              }
          });
      });

      $('#wallet-withdraw').change(function() {
          var option = $('#wallet-withdraw').val();
          $('#input-money').val('');
          $("#submit-confirm").attr({
              "disabled": true
          });
          $("#input-money").attr({
              "disabled": true
          });
          $('#wallet_name').html('');
          $('#withdraw_limit').html('');
          $('#wallet_address').html('');
          $('#wallet_balance').html('');
          $('#withdraw_deposit_amount').html('');

          if(option != 0)
          $.ajax({
              url: "/ajax/getWalletCat/" + option + "/" + $('#user_id').html(),
              type: "GET",
              beforeSend: function() {

              },
              success: function(response) {
                  var json = JSON.parse(response);
                  if (json['status'] == 'true') {
                      var limit = json['wallet_cat']['withdraw'].split('-');
                      console.log(json['wallet']['money']);
                      console.log(limit[0]);
                      if(parseFloat(limit[0]) < parseFloat(json['wallet']['money'])) {
                        var status = '<span class="text-success">Can withdraw</span>';
                      } else {
                        var status = '<span class="text-danger">Can not withdraw</span>';
                      }
                      $('#wallet_name').html('<img src="/images/icon/wallet/small/' + json['wallet_cat']['simplename'] + '.png">' + json['wallet_cat']['name']);
                      $('#withdraw_limit').html('$' + limit[0] + " - $" + limit[1]);
                      $('#wallet_balance').html("$" + json['wallet']['money'] + " - "+ status);
                      $('#wallet_address').html(json['wallet']['address']);
                      $("#input-money").attr({
                          "max": json['wallet']['money'],
                          "min": limit[0],
                          "disabled": false
                      });
                  } else {
                      $("#input-money").attr({
                          "disabled": true
                      });
                  }
              },
              error: function(response) {

              }
          });
      });
   function countDown(countDownDate) {
       var x = setInterval(function() {
           var now = new Date().getTime();
           var distance = countDownDate - now;
           var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
           var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
           var seconds = Math.floor((distance % (1000 * 60)) / 1000);
           $('#count-down').html('<i class="fa fa-clock-o"></i> ' + hours + 'h ' + minutes + 'm ' + seconds + 's');
           if (distance < 0) {
               clearInterval(x);
               $('#count-down').html('<i class="fa fa-clock-o"></i> EXPIRED');
           }
       }, 1000);
   }

   function copyTextInput(id, animated = null) {
       var copyText = document.getElementById(id);
       copyText.select();
       document.execCommand("Copy");
       if (animated != null) {
           $('#' + animated).html('Copied !');
       }
   }

   function GetURLParameter(sParam) {
       var sPageURL = window.location.search.substring(1);
       var sURLVariables = sPageURL.split('&');
       for (var i = 0; i < sURLVariables.length; i++) {
           var sParameterName = sURLVariables[i].split('=');
           if (sParameterName[0] == sParam) {
               return sParameterName[1];
           }
       }
   }

   function sortTransaction() {
       var status = $("#select-status").val();
       var sort = $("#select-sort").val();
       var type = $("#select-type").val();
       var user = GetURLParameter('user');
       if (user == undefined)
           window.location.href = "?status=" + status + "&sort=" + sort + "&type=" + type;
       else
           window.location.href = "?status=" + status + "&sort=" + sort + "&type=" + type + "&user=" + user;
   }
   $("#select-status").change(function() {
       sortTransaction();
   });
   $("#select-sort").change(function() {
       sortTransaction();
   });
   $("#select-type").change(function() {
       sortTransaction();
   });

   function copyText(text) {
       let textarea = document.createElement('textarea');
       textarea.id = 't';
       textarea.style.height = 0;
       document.body.appendChild(textarea);
       textarea.value = text;
       let selector = document.querySelector('#t');
       selector.select();
       document.execCommand('copy');
       document.body.removeChild(textarea);
   }
    </script>
    

    
	  </body>

</html>