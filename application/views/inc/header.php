<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url('favicon.ico'); ?>">
	<title><?php echo (isset($title) ? $title : 'Default title'); ?></title>
    <link href="<?php echo base_url(); ?>temp/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="<?php echo base_url(); ?>temp/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>temp/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('theme/css/style.css'); ?>" rel="stylesheet">
</head>
<body>
	<div class="container-fluid" id="wrapper" style="display: none;">
		<div class="row">
			<nav class="sidebar col-md-3 col-xs-12 col-sm-4 col-lg-3 col-xl-2 main-menu-content">
				<h1 class="site-title"><a href="<?php echo(base_url($lng.'/home')); ?>"><img src="<?php echo base_url('images/logo.png'); ?>" style="width: 100%;"></a></h1>
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em> Menu</a>

				<ul class="nav nav-pills flex-column sidebar-nav">
					<?php if(get_cookie('uid') != NULL && $user['id'] == ADMIN) { ?>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'transactions-admin' || $this->uri->segment(2) == 'transactions-admin' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/transactions-admin'); ?>"><em class="fa fa-list"></em> Payment <span class="badge badge-success"><?php echo $admin_data['count_trans']; ?></span></a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'support-admin' || $this->uri->segment(2) == 'support-admin' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/support-admin'); ?>"><em class="fa fa-envelope-o"></em> Contact <span class="badge badge-success"><?php echo $admin_data['count_suport']; ?></span></a> </li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'setting-admin' || $this->uri->segment(2) == 'setting-admin' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/setting-admin'); ?>"><em class="fa fa-cogs"></em> Setting </a> </li>
					<li style="border-bottom: 2px solid #ddd;"></li>
					<?php } ?>

					<?php if(get_cookie('uid') == NULL) { ?>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == false || $this->uri->segment(2) == 'home' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/home'); ?>"><em class="fa fa-home"></em> <?php echo $this->lang->line('home'); ?></a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'register' || $this->uri->segment(2) == 'register' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/register'); ?>"><em class="fa fa-registered"></em> <?php echo $this->lang->line('register'); ?></a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'login' || $this->uri->segment(2) == 'login' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/login'); ?>"><em class="fa fa-sign-in"></em> <?php echo $this->lang->line('login'); ?></a></li>
					<?php } else { ?>
					
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == false || $this->uri->segment(2) == 'home' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/home'); ?>"><em class="fa fa-dashboard"></em> <?php echo $this->lang->line('dashboard'); ?> </a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'deposit' || $this->uri->segment(2) == 'deposit' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/deposit'); ?>"><em class="fa fa-arrow-circle-o-up"></em> <?php echo $this->lang->line('deposit'); ?></a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'withdraw' || $this->uri->segment(2) == 'withdraw' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/withdraw'); ?>"><em class="fa fa-arrow-circle-o-down"></em> <?php echo $this->lang->line('withdraw'); ?></a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'wallet' || $this->uri->segment(2) == 'wallet' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/wallet'); ?>"><em class="fa fa-server"></em> <?php echo $this->lang->line('myWallet'); ?> </a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'transactions' || $this->uri->segment(2) == 'transactions' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/transactions'); ?>"><em class="fa fa-list"></em> <?php echo $this->lang->line('history'); ?></a></li>

					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'account' || $this->uri->segment(2) == 'account' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/account'); ?>"><em class="fa fa-user"></em> <?php echo $this->lang->line('myAccount'); ?> </a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'referrals' || $this->uri->segment(2) == 'referrals' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/referrals'); ?>"><em class="fa fa-users"></em> <?php echo $this->lang->line('myReferrals'); ?> </a></li>
					<?php } ?>

					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'ratesnlimits' || $this->uri->segment(2) == 'ratesnlimits' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/ratesnlimits'); ?>"><em class="fa fa-table"></em> <?php echo $this->lang->line('ratesnlimits'); ?> </a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'support' || $this->uri->segment(2) == 'support' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/support'); ?>"><em class="fa fa-envelope-o"></em> <?php echo $this->lang->line('contactus'); ?></a></li>
					<li class="nav-item"><a class="nav-link<?php echo ($this->uri->segment(1) == 'agreement' || $this->uri->segment(2) == 'agreement' ? ' active' : '');  ?>" href="<?php echo base_url($lng.'/agreement'); ?>"><em class="fa fa-book"></em> <?php echo $this->lang->line('agreement'); ?></a></li>
				</ul>
			</nav>
			<main class="col-xs-12 col-sm-8 offset-sm-4 col-md-9 offset-md-3 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 main-background">
				<header class="page-header row header-content">
				   <div class="col-md-12 col-xl-8">
				      <h1 class="float-left text-center text-md-left"><i class="<?php echo (isset($icon) ? $icon : 'fa fa-home'); ?>"></i> <?php echo (isset($title) ? $title : 'Hello'); ?></h1>
				   </div>

					<div class="col-md-12 col-xl-4 d-flex flex-row-reverse justify-content-around">
					<?php if(get_cookie('uid') !== NULL) { ?>
					   	<div class="dropdown">
					      <a class="btn btn-stripped dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown">
					         <div class="username mt-1">
					            <h4 class="mb-1"><i class="fa fa-user-circle-o"></i> <?php echo substr(explode('@', $user['email'])[0], 0, 10); ?></h4>
					         </div>
					         <span class="visible-print-block" id="user_id"><?php echo $user['id']; ?></span>
					      </a>
					      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
							<a class="dropdown-item" href="<?php echo base_url($lng.'/account'); ?>">
								<em class="fa fa-user text-<?php echo ($user['activated'] == 1 ? 'success' : 'danger') ?>"></em> 
								<?php echo $this->lang->line('myAccount'); ?>
							</a>
							<a class="dropdown-item" href="<?php echo base_url($lng.'/logout'); ?>">
								<em class="fa fa-power-off text-danger"></em> <?php echo $this->lang->line('logout'); ?>
							</a>
					      </div>
					    </div>
					<?php } else {  ?>
					   	<div class="dropdown">
					      <a class="btn btn-stripped dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown">
					         <div class="username mt-1">
					            <h4 class="mb-1">
					            	<i class="fa fa-user-circle-o"></i> <?php echo $this->lang->line('login_register'); ?>
					            </h4>
					         </div>
					      </a>
					      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
							<a class="dropdown-item" href="<?php echo base_url($lng.'/login'); ?>">
								<strong><i class="fa fa-sign-in"></i> <?php echo $this->lang->line('login'); ?></strong>
							</a>
							<a class="dropdown-item" href="<?php echo base_url($lng.'/register'); ?>">
								<strong><i class="fa fa-registered"></i> <?php echo $this->lang->line('register'); ?></strong>
							</a>
					      </div>
					    </div>
					<?php } ?>
		   				<div class="dropdown">
						  <button class="btn btn-stripped dropdown-toggle" type="button" id="drdLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    <img src="<?php echo base_url('/images/icon/flag/'.$lng.'.png'); ?>" style="height: 32px;">
						  </button>
						  <div class="dropdown-menu dropdown-menu-left" aria-labelledby="drdLanguage" id="dropdown-language">
						  	<?php if($lng != 'en') { ?>

						    <a class="dropdown-item" href="<?php echo base_url('en/'.($this->uri->segment(2) != false ? $this->uri->segment(2) : 'home')); ?>"><img src="<?php echo base_url('/images/icon/flag/en.png'); ?>"> English</a>
						    <?php } ?>
						    <?php if($lng != 'kr') { ?>

						    <a class="dropdown-item" href="<?php echo base_url('kr/'.($this->uri->segment(2) != false ? $this->uri->segment(2) : 'home')); ?>"><img src="<?php echo base_url('/images/icon/flag/kr.png'); ?>"> 한국어</a>
						    <?php } ?>
						    <?php if($lng != 'jp') { ?>

						    <a class="dropdown-item" href="<?php echo base_url('jp/'.($this->uri->segment(2) != false ? $this->uri->segment(2) : 'home')); ?>"><img src="<?php echo base_url('/images/icon/flag/jp.png'); ?>"> 日本語</a>
						    <?php } ?>
						    <?php if($lng != 'in') { ?>

						    <a class="dropdown-item" href="<?php echo base_url('in/'.($this->uri->segment(2) != false ? $this->uri->segment(2) : 'home')); ?>"><img src="<?php echo base_url('/images/icon/flag/in.png'); ?>"> भारतीय भाषाओं</a>
						    <?php } ?>
						    <?php if($lng != 'cn') { ?>

						    <a class="dropdown-item" href="<?php echo base_url('cn/'.($this->uri->segment(2) != false ? $this->uri->segment(2) : 'home')); ?>"><img src="<?php echo base_url('/images/icon/flag/cn.png'); ?>"> 中文</a>
						    <?php } ?>
						    <?php if($lng != 'vn') { ?>

						    <a class="dropdown-item" href="<?php echo base_url('vn/'.($this->uri->segment(2) != false ? $this->uri->segment(2) : 'home')); ?>"><img src="<?php echo base_url('/images/icon/flag/vn.png'); ?>"> Tiếng Việt</a>
						    <?php } ?>
						  </div>
						</div>
						<span class="visible-print-block" id="language-key"><?php echo $lng; ?></span>
					</div>
				   <div class="clear"></div>
				</header>

				<section class="row">
					
