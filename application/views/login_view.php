<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12">
		<div class="card mb-4">
			<div class="card-block" id="login-div">
				<form method="POST" id="form-login">
				  <div class="form-group" id="block-email">
				    <label for="email"><i class="fa fa-envelope-o"></i> <?php echo $this->lang->line('your_email'); ?></label>
				    <input type="text" class="form-control" id="email" name="email" autocomplete="off">
					<span class="text-danger form-text" id="email-text"></span>
				  </div>
				  <div class="form-group" id="block-password">
				    <label for="password"><i class="fa fa-key"></i> <?php echo $this->lang->line('your_password'); ?></label>
						<div class="input-group">
						  <input type="password" class="form-control" id="password" name="password" minlength="6" maxlength="32" autocomplete="off">
						  <span class="input-group-addon" id="show-password" title="Show/hide"><i class="fa fa-eye"></i></span>
						</div>
				    <span class="text-danger form-text" id="password-text"></span>
				  </div>
      			  <div id="div-recaptcha"><div class="g-recaptcha" data-sitekey="6LfHkD4UAAAAABO6R-6b_EZI890_QJoFqLHjKaXb"></div></div>
      			  <span class="text-danger form-text" id="login-text"></span>
				  <div class="form-group">
				  	<button type="button" name="submit" id="submitLogin" class="btn btn-primary btn-lg mt-2"><i class="fa fa fa-sign-in"></i> <?php echo $this->lang->line('login'); ?></button> 
				  	<span style="display: none;" id="login-loading"><img src="/images/load.gif" style="height: 70px;"></span>
				  </div>
				</form>
				<p><a href="<?php echo base_url($lng.'/register'); ?>"><i class="fa fa-registered"></i> <?php echo $this->lang->line('register'); ?></a></p>
				<p><a href="<?php echo base_url($lng.'/forgot-password'); ?>"><i class="fa fa-question-circle"></i> <?php echo $this->lang->line('forgot_password'); ?></a></p>
			</div>
			<div class="card-block text-center" id="login-success" style="display: none;">
				<img src="/images/load.gif" style="height: 150px;">
				<hr>
				<h4><?php echo $this->lang->line('login_success'); ?> <a href="<?php echo base_url($lng.'/home'); ?>"><?php echo $this->lang->line('here'); ?></a></h4>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>