<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-lg-<?php echo (isset($menu_right) ? '12' : '8'); ?>">
		<div class="card mb-4">
			<div class="card-block">
				<div class="row">
					<div class="col-md-12 text-center">
						<h4>Total amount: <i class="fa fa-usd"></i> <?php echo $all_amount; ?></h4>
						<?php if(isset($data_user) && $data_user != false) { ?>
						<a class="btn btn-xs btn-primary" style="margin-bottom: 3px;" href="<?php echo base_url('transactions-admin'); ?>">
							<i class="fa fa-user"></i> <?php echo $data_user['email-f']; ?>
							<i class="fa fa-times text-danger"></i>
						</a>
						<br>
						<?php echo $data_user['id']; ?> || <?php echo $data_user['email']; ?> || <?php echo $data_user['money']; ?> || <?php echo $data_user['reg_date']; ?>
						<?php } ?>
					</div>
				</div>
				<div class="row">
				  <div class="col-lg-4">
				    <div class="input-group">
				      <span class="input-group-addon"><i class="fa fa-exchange"></i></span>
						<select id="select-type" class="form-control">
							<option value="0">
						  		All
							</option>
						  	<option value="1"<?php echo (isset($_GET['type']) && $_GET['type'] == 1 ? ' selected' : ''); ?>>
						  		Deposit
						  	</option>
						  	<option value="2"<?php echo (isset($_GET['type']) && $_GET['type'] == 2 ? ' selected' : ''); ?>>
						  		Withdraw
						  	</option>
						  	<option value="3"<?php echo (isset($_GET['type']) && $_GET['type'] == 3 ? ' selected' : ''); ?>>
						  		Interest Accrual
						  	</option>
						  	<option value="4"<?php echo (isset($_GET['type']) && $_GET['type'] == 4 ? ' selected' : ''); ?>>
						  		System
						  	</option>
						</select>
				    </div>
				  </div>
				  <div class="col-lg-4">
				    <div class="input-group">
				      <span class="input-group-addon"><i class="fa fa-check"></i></span>
						<select id="select-status" class="form-control">
							<option value="0"<?php echo (isset($_GET['status']) && $_GET['status'] == 0 ? ' selected' : ''); ?>>
						  		All
							</option>
						  	<option value="1"<?php echo (isset($_GET['status']) && $_GET['status'] == 1 ? ' selected' : ''); ?>>
						  		Pending
						  	</option>
						  	<option value="2"<?php echo (isset($_GET['status']) && $_GET['status'] == 2 ? ' selected' : ''); ?>>
						  		Completed
						  	</option>
						  	<option value="3"<?php echo (isset($_GET['status']) && $_GET['status'] == 3 ? ' selected' : ''); ?>>
						  		Decline
						  	</option>
						</select>
				    </div>
				  </div>
				  <div class="col-lg-4">
				    <div class="input-group">
				      <span class="input-group-addon"><i class="fa fa-sort"></i></span>
						<select id="select-sort" class="form-control">
						  	<option value="TIME-DESC"<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'TIME-DESC' ? ' selected' : ''); ?>>
						  		Time decrease
						  	</option>
							<option value="TIME-ASC"<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'TIME-ASC' ? ' selected' : ''); ?>>
							  	Time ascending
							</option>
						  	<option value="MONEY-DESC"<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'MONEY-DESC' ? ' selected' : ''); ?>>
						  		Amount decrease
						  	</option>
						  	<option value="MONEY-ASC"<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'MONEY-ASC' ? ' selected' : ''); ?>>
						  		Amount ascending
						  	</option>
						  	<option value="UPDATED-DESC"<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'UPDATED-DESC' ? ' selected' : ''); ?>>
						  		Updated decrease
						  	</option>
						  	<option value="UPDATED-ASC"<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'UPDATED-ASC' ? ' selected' : ''); ?>>
						  		Updated ascending
						  	</option>
						</select>
				    </div>
				  </div>
				</div>
				<hr>
				<div class="table-responsive">
					<table class="table table-hover">
					    <thead>
					      <tr>
					        <th>Id</th>
					        <th>Amount</th>
					        <th>Wallet</th>
					        <th>Time</th>
					        <th>Type</th>
					        <th>Status</th>
					        <?php echo ($user['id'] == ADMIN) ? '<th>Update</th>' : ''; ?>
					      </tr>
					    </thead>
					    <tbody>
					    	<?php if($list_transactions != false) foreach ($list_transactions as $key => $value) { ?>
						      <tr>
						        <td><strong>#<?php echo $value['id']; ?></strong></td>
						        <td>
						        	<button data-value="<?php echo $value['money']; ?>" class="btn btn-xs" onclick="copyText(<?php echo $value['money']; ?>);">
						        		<i class="fa fa-usd"></i> <?php echo $value['money']; ?>
						        	</button>
						        </td>
						        <td>
						        	<button class="btn btn-xs" style="height: 30px;" onclick="copyText('<?php echo $value['wallet']['user_wallet']['address']; ?>');">
						        		<img src="<?php echo base_url('images/icon/wallet/small/'.$value['wallet']['wallet_cat']['simplename'].'.png'); ?>">
						        	</button>
						        </td>
						        <td><?php echo $value['time']; ?></td>
						        <td><?php echo $value['type-t']; ?></td>
						        <td><?php echo $value['status']; ?></td>
						        <?php if($user['id'] == ADMIN) { ?>
						        <td>
									<div class="dropdown">
									  <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    <i class="fa fa-cog"></i>
									  </button>
									  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
									  	<?php if($value['type'] != 4) { ?>
									    <a class="dropdown-item" href="<?php echo base_url('transactions-admin?trans_id='.$value['id'].'&change_status=1'); ?>"><i class="fa fa-spinner text-warning"></i> Pennding</a>
									    <?php } ?>
									    <a class="dropdown-item" href="<?php echo base_url('transactions-admin?trans_id='.$value['id'].'&change_status=2'); ?>"><i class="fa fa-check text-success"></i> Completed</a>
									    <?php if($value['type'] != 4) { ?>
									    <a class="dropdown-item" href="<?php echo base_url('transactions-admin?trans_id='.$value['id'].'&change_status=3&return=1'); ?>"><i class="fa fa-refresh text-primary"></i> Restore</a>
									    <a class="dropdown-item" href="<?php echo base_url('transactions-admin?trans_id='.$value['id'].'&change_status=3&return=0'); ?>"><i class="fa fa-times text-danger"></i> Decline</a>
									    <?php } ?>
									    <?php if($value['type'] == 4) { ?>
									    <a class="dropdown-item" href="<?php echo base_url('transactions-admin?trans_id='.$value['id'].'&delete=true'); ?>"><i class="fa fa-trash text-danger"></i> Delete</a>
									    <?php } ?>
									  </div>
									</div>
						        </td>
						        <?php } ?>
						      </tr>
						      <tr style="font-size: 8pt;border-bottom: 2px solid #ddd;">
						      	<td style="display: flex;"><i class="fa fa-user-o"></i>&nbsp;<a href="?user=<?php echo $value['user']['id']; ?>"><?php echo $value['user']['email-f']; ?></a> </td>
						      	<td><?php echo $value['updated']; ?></td>
						      	<td colspan="5">
						      		<i class="fa fa-comment-o"></i> <?php echo $value['comment']; ?>
						      	</td>
						      </tr>
					    	<?php } ?>
					    </tbody>
					 </table>
					 </div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>