<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-lg-<?php echo (isset($menu_right) ? '12' : '8'); ?>">
		<div class="card mb-4">
			<div class="card-block">
				<table class="table table-hover">
				  <thead>
				    <tr>
				      <th>Id</th>
				      <th>Email</th>
				      <th>Is User</th>
				      <th>Reg Date</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php if($contact_list != false) foreach ($contact_list as $key => $item) { ?>
				  	<tr data-toggle="collapse" href="#collapse<?php echo $item->id; ?>" aria-expanded="false" aria-controls="collapse<?php echo $item->id; ?>">
				  		<td><strong>#<?php echo $item->id; ?></strong></td>
				  		<td><?php echo $item->email; ?></td>
				  		<td><?php echo ($item->user_id != 0 ? 'YES' : 'NO'); ?></td>
				  		<td><?php echo $item->time; ?></td>
				  	</tr>
				  	<tr class="collapse" id="collapse<?php echo $item->id; ?>">
				  		<td colspan="4"><i class="fa fa-comments-o"></i> <?php echo $item->content; ?></td>
				  	</tr>
				  	<?php } ?>
				  	<?php if($contact_list == false) { ?>
				  	<tr>
				  		<td colspan="4" class="text-center">Empty !</td>
				  	</tr>
				  	<?php } ?>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>