<?php $this->load->view('inc/header'); ?>

	<div class="col-md-12 col-xl-12">
		<div class="row">
			<div class="col-xl-12">
				<div class="card mb-3">
					<h4 class="card-header card-inverse card-primary" style="color: white;">
						<i class="fa fa-cog"></i> System setting
					</h4>
					<div class="card-block">
						<form method="POST">
							<?php if($data_setting != false) foreach ($data_setting as $key => $item) { ?>
							<div class="form-group">
							    <label for="<?php echo $item->key; ?>"><?php echo ($key+1); ?>. <?php echo strtoupper($item->key); ?></label>
							    <input type="text" name="<?php echo $item->key; ?>" class="form-control" id="<?php echo strtoupper($item->key); ?>" value="<?php echo $item->value; ?>" required="required" autocomplete="off">
							</div>
							<?php } ?>
							<div class="form-group">
								<button type="submit" name="submit" class="btn btn-primary btn-block"><i class="fa fa-check"></i> Update</button>
							</div>
						</form>
			        </div>
				</div>

				<div class="card mb-3">
					<h4 class="card-header card-inverse card-primary" style="color: white;">
						<i class="fa fa-cog"></i> Wallet Default Amount
					</h4>
					<div class="card-block">
						<form method="POST">
							<?php if($wallet_cat != false) foreach ($wallet_cat as $key => $item) { ?>
							<div class="form-group">
							    <label for="<?php echo $item->simplename; ?>"><?php echo ($key+1); ?>. <?php echo strtoupper($item->name); ?></label>
							    <input type="text" name="<?php echo $item->simplename; ?>" class="form-control" id="<?php echo strtoupper($item->simplename); ?>" value="<?php echo $item->amount; ?>" required="required" autocomplete="off">
							</div>
							<?php } ?>
							<div class="form-group">
								<button type="submit" name="sub_wallet_cat" class="btn btn-primary btn-block"><i class="fa fa-check"></i> Update</button>
							</div>
						</form>
			        </div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>