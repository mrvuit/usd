<!DOCTYPE html>
<html>
<head>
	<title>Deposit PerfectMoney</title>
	<style type="text/css">
		body {
			text-align: center;
		}
		button {
			font-size: 12px;
			padding: 5px;
			border: 1px solid #fff;
			color: blue;
		}
	</style>
</head>
<body>
	<img src="<?php echo base_url('images/loading.gif'); ?>">
	<form action='https://perfectmoney.is/api/step1.asp' method='POST' id='payform'>
		<input type='hidden' name='PAYEE_ACCOUNT' value='U15831830'>
		<input type='hidden' name='PAYEE_NAME' value='myperday.com'>
		<input type='hidden' name='PAYMENT_ID' value='<?php echo $user['id']; ?>'>
		<input type='hidden' name='PAYMENT_AMOUNT' value='<?php echo $last_paymnet->money; ?>'>
		<input type='hidden' name='PAYMENT_UNITS' value='USD'>
		<input type='hidden' name='STATUS_URL' value='<?php echo base_url('status-payment'); ?>'>
		<input type='hidden' name='PAYMENT_URL' value='<?php echo base_url($lng.'/deposit?status=1'); ?>'>
		<input type='hidden' name='PAYMENT_URL_METHOD' value='LINK'>
		<input type='hidden' name='NOPAYMENT_URL' value='<?php echo base_url($lng.'/deposit?status=0'); ?>'>
		<input type='hidden' name='NOPAYMENT_URL_METHOD' value='LINK'>
		<button type="submit"><img src="<?php echo base_url('images/icon/wallet/small/perfectmoney.png'); ?>"> Pay via PerfectMoney</button>
	</form>

<script type="text/javascript">
document.getElementById("payform").submit();
</script>
</body>
</html>