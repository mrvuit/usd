<!DOCTYPE html>
<html>
<head>
	<title>Deposit Payeer</title>
	<style type="text/css">
		body {
			text-align: center;
		}
		button {
			font-size: 12px;
			padding: 5px;
			border: 1px solid #fff;
			color: blue;
		}
	</style>
</head>
<body>
	<img src="<?php echo base_url('images/loading.gif'); ?>">
	<?php
		$m_shop = '483682055';
		$m_orderid = $user['id'];
		$m_amount = number_format($last_paymnet->money, 2, '.', '');
		$m_curr = 'USD';
		$m_desc = base64_encode($last_paymnet->comment);
		$m_key = '123';
		$arHash = array(
			$m_shop,
			$m_orderid,
			$m_amount,
			$m_curr,
			$m_desc
		);
		$arHash[] = $m_key;
		$sign = strtoupper(hash('sha256', implode(':', $arHash)));
	?>
	<form method="post" action="https://payeer.com/merchant/">
		<input type="hidden" name="m_shop" value="<?=$m_shop?>">
		<input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
		<input type="hidden" name="m_amount" value="<?=$m_amount?>">
		<input type="hidden" name="m_curr" value="<?=$m_curr?>">
		<input type="hidden" name="m_desc" value="<?=$m_desc?>">
		<input type="hidden" name="m_sign" value="<?=$sign?>">
		<button type="submit" name="m_process"><img src="<?php echo base_url('images/icon/wallet/small/payeer.png'); ?>"> Pay via Payeer</button>
	</form>

	<script type="text/javascript">
	document.getElementById("payform").submit();
	</script>
</body>
</html>