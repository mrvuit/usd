<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-xl-8" id="body-content">
		<div class="card mb-4">
			<div class="card-block">
				<h3 class="card-title"><?php echo $this->lang->line('interest_per_day'); ?></h3>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th><?php echo $this->lang->line('amount'); ?></th>
								<th><?php echo $this->lang->line('interest_rate'); ?></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>< $<?php echo number_format(1000,2) ?></td>
								<td>1.3%</td>
							</tr>
							<tr>
								<td>$<?php echo number_format(10000,2) ?> - $<?php echo number_format(100000,2) ?></td>
								<td>1.5%</td>
							</tr>
							<tr>
								<td>$<?php echo number_format(100000,2) ?> - $<?php echo number_format(1000000,2) ?></td>
								<td>1.7%</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="card mb-4">
			<div class="card-block">
				<h3 class="card-title"><?php echo $this->lang->line('daily_limit'); ?></h3>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th></th>
								<th><?php echo $this->lang->line('deposit_limit'); ?></th>
								<th><?php echo $this->lang->line('withdraw_limit'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($wallet_cat as $key => $item) { ?>
							<tr>
								<td><img src="<?php echo base_url('images/icon/wallet/small/'.$item['simplename'].'.png'); ?>"> <?php echo $item['name']; ?></td>
								<td>$<?php echo explode('-', $item['deposit'])[0]; ?> - $<?php echo explode('-', $item['deposit'])[1]; ?></td>
								<td>$<?php echo explode('-', $item['withdraw'])[0]; ?> - $<?php echo explode('-', $item['withdraw'])[1]; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="card mb-4">
			<div class="card-block">
				<h3 class="card-title"><?php echo $this->lang->line('fees'); ?></h3>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th></th>
								<th><?php echo $this->lang->line('deposit_fees'); ?></th>
								<th><?php echo $this->lang->line('withdraw_fees'); ?></th>
								<th><?php echo $this->lang->line('deposit_24h'); ?></th>
								<th><?php echo $this->lang->line('withdraw_24h'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($wallet_cat as $key => $item) { ?>
							<tr>
								<td><img src="<?php echo base_url('images/icon/wallet/small/'.$item['simplename'].'.png'); ?>"> <?php echo $item['name']; ?></td>
								<td>0%</td>
								<td>0%</td>
								<td>Unlimited</td>
								<td>1</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>