<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12">
		<div class="modal fade" id="deposit_modal">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title">Confirmation of deposit</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body" style="word-wrap: break-word;">
				<p class="lead">
					<?php echo $this->lang->line('wallet'); ?>: <span id="modal_wallet"></span><br>
					<?php echo $this->lang->line('deposit_amount'); ?>: <span  id="modal_amount"></span><br>
					<span id="Bitcoin-price" style="display: none;">
						Bitcoin Price: $<span id="Bitcoin-price-usd"><?php echo number_format($system['btc_price_usd'], 2); ?></span>
						<a href="https://coinmarketcap.com/currencies/bitcoin/" target="_blank"><u>CMC</u></a>
					</span>
				</p>
				<p class="lead" id="Ethereum-price" style="display: none;">
					Ethereum Price: $<span id="Ethereum-price-usd"><?php echo number_format($system['eth_price_usd'], 2); ?></span>
					<a href="https://coinmarketcap.com/currencies/ethereum/" target="_blank"><u>CMC</u></a>
				</p>
				<p id="abnormal-payment">
					<?php echo $this->lang->line('use_address'); ?> <span id="user_address"></span><br>
					<?php echo $this->lang->line('transfer'); ?> <span id="amount-abnormal" style="font-weight: bold;"></span> <?php echo $this->lang->line('to'); ?>
					<span id="Bitcoin-system-address" style="display: none;font-weight: bold;"><?php echo $system['btc_address']; ?></span> 
					<span id="Ethereum-system-address" style="display: none;font-weight: bold;"><?php echo $system['eth_address']; ?></span>[<a href="javascript:void:(0);" onclick="copyAdressSystem();">Copy</a>] <span id="copied" style="display: none;">Copied !</span><br>
					<?php echo $this->lang->line('after_transferring'); ?>
				</p>
				<p id="normal-payment"><?php echo $this->lang->line('press_the_button'); ?> "<strong>OK</strong>" <?php echo $this->lang->line('to_payment_now'); ?>.</p>
				<p><small class="text-muted"><i class="fa fa-warning"></i> <?php echo $this->lang->line('modal_note'); ?></small></p>
				<div class="alert" role="alert" style="background: rgba(193, 66, 66, 0.6); display: none;" id="deposit-alert-error">
					<small>
						<em class="fa fa-warning mr-2"></em> <span id="text-error-deposit"></span>
					</small>
				</div>
		      </div>
		      <div class="modal-footer">
		      	<span id="model-loading-icon" style="display: none;"><img src="<?php echo base_url('images/loading.gif'); ?>"></span>
		        <button type="button" class="btn btn-primary btn-sm" id="ok_deposit_button"><i class="fa fa-lock"></i> OK</button>
		        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('cancel'); ?></button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="card mb-4">
			<div class="card-block">
				<?php if($available == false) { ?>
				<div class="alert bg-warning" role="alert">
					<em class="fa fa-exclamation-triangle mr-2"></em>
					<?php echo $this->lang->line('cannot_deposit'); ?>
				</div>				
				<a href="<?php echo base_url($lng.'/wallet'); ?>" class="btn btn-primary"><i class="fa fa-server"></i> <?php echo $this->lang->line('go_to_wallet'); ?></a>
				<?php } else { ?>
			    <div class="row">
			    	<div class="col-md-12 col-lg-6">
						<form method="POST" id="form-wallet">
				    		<div class="form-group">
							    <label for="money">
							    	<i class="fa fa-usd"></i>
							    	<?php echo $this->lang->line('select_payment'); ?>
							    </label>
							    <select class="form-control form-control-lg" id="wallet-deposit">
							    	<option value="0"><?php echo $this->lang->line('select_one_wallet'); ?></option>
							    	<?php foreach ($wallet_cat as $key => $item) { ?>
							    		<?php if(!empty($user_wallet[$key]['address'])) { ?>
							    		<option value="<?php echo $item['id']; ?>"><?php echo $wallet_cat[$key]['name']; ?></option>
							    		<?php } ?>
							    	<?php } ?>
							    </select>
							</div>

							<div class="form-group">
							    <label for="money">
							    	<i class="fa fa-usd"></i>
							    	<?php echo $this->lang->line('enter_amount'); ?>
							    </label>
					    		<input type="number" step="any" placeholder="$" class="form-control form-control-lg" id="input-money" name="money" required="required" disabled="disabled" autocomplete="off">
					    	</div>

							<table class="table table-bordered" style="table-layout: fixed;">
							    <tbody>
							      <tr>
							        <td style="width: 30%;"><strong><?php echo $this->lang->line('wallet'); ?></strong></td>
							        <td id="wallet_name"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('address'); ?></strong></td>
							        <td id="wallet_address" style="word-wrap: break-word;"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('deposit_limit'); ?></strong></td>
							        <td id="deposit_limit"></td>
							      </tr>
							      <tr>
							        <td><strong><?php echo $this->lang->line('deposit_amount'); ?></strong></td>
							        <td id="withdraw_deposit_amount"></td>
							      </tr>
							    </tbody>
							  </table>

							<div class="form-group">
							  	<button type="button" name="submit" class="btn btn-primary btn-lg btn-block mt-2" id="submit-confirm" data-toggle="modal" data-target="#deposit_modal" disabled="disabled" onclick="loadDataToModal();">
							  		<i class="fa fa-paper-plane" aria-hidden="true"></i> <?php echo $this->lang->line('confirm'); ?>
							  	</button> 
						  	</div>
						</form>
			    	</div>
			    	<div class="col-md-12 col-lg-6">
			    		<div class="row">
			    			<div class="col-md-12">
								<div class="card">
					 				<h4 class="card-header text-center"><?php echo $this->lang->line('deposit_history'); ?> </h4>
									<div class="card-block" style="padding: 0px;">
						    			<div style="overflow: scroll;max-height: 390px;height: 390px;">
											<table class="table table-striped table-sm table-bordered">
											  <thead>
											    <tr>
											      <th style="width: 10%;">Id</th>
											      <th style="width: 30%;"><?php echo $this->lang->line('wallet'); ?></th>
											      <th style="width: 30%;"><?php echo $this->lang->line('status'); ?></th>
											      <th style="width: 30%;"><?php echo $this->lang->line('amount'); ?></th>
											    </tr>
											  </thead>
											  <tbody>
											  	<?php if($deposit_htr != false) foreach ($deposit_htr as $key => $item) {  ?>
											    <tr>
											      <th>#<?php echo $item['id']; ?></th>
											      <td><img src="/images/icon/wallet/small/<?php echo $item['wallet_cat']['simplename']; ?>.png"> <?php echo $item['wallet_cat']['name']; ?></td>
											      <td><?php echo $item['status']; ?></td>
											      <td>$<?php echo $item['money']; ?></td>
											    </tr>
											    <?php } ?>
											    <?php if($deposit_htr == false) { ?>
											    <tr><td colspan="4" class="text-center"><?php echo $this->lang->line('empty'); ?> !</td></tr>
											    <?php } ?>
											  </tbody>
											</table>
						    			</div>
						        	</div>
								</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
				<?php }  ?>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>