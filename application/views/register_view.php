<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12">
		<div class="card mb-4">
			<div class="card-block" id="register-div">
				<form method="POST" id="form-register">
				  <div class="form-group" id="block-email">
				    <label for="email"><i class="fa fa-envelope-o"></i> <?php echo $this->lang->line('your_email'); ?></label>
				    <input type="text" class="form-control" id="email" name="email" autocomplete="off">
					<span class="text-danger form-text" id="email-text"></span>
				  </div>
				  <div class="form-group" id="block-password">
				    <label for="password"><i class="fa fa-key"></i><?php echo $this->lang->line('your_password'); ?></label>
						<div class="input-group">
						  <input type="password" class="form-control" id="password" name="password" minlength="6" maxlength="32" autocomplete="off">
						  <span class="input-group-addon" id="show-password" title="Show/hide"><i class="fa fa-eye"></i></span>
						</div>
				    <span class="text-danger form-text" id="password-text"></span>
				  </div>
				  <div class="form-group" id="block-repeat-password">
				    <label for="repeatPassword"><i class="fa fa-key"></i> <?php echo $this->lang->line('repeat_password'); ?></label>
				    <input type="password" class="form-control" id="repeatPassword" name="repeatPassword" minlength="6" maxlength="32" autocomplete="off">
				    <span class="text-danger form-text" id="repeat-password-text"></span>
				  </div>
      			  <div class="form-group" id="block-agreement">
					<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
					<input type="checkbox" class="custom-control-input" name="agreement" id="agreement">
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description"> <?php echo $this->lang->line('register_note'); ?></span>
					</label>
      			  </div>

      			  <div class="g-recaptcha" data-sitekey="6LfHkD4UAAAAABO6R-6b_EZI890_QJoFqLHjKaXb"></div>
      			  <span class="text-danger form-text" id="other-text"></span>
				  <div class="form-group">
				  	<button type="button" name="submit" id="submitRegister" class="btn btn-primary btn-lg mt-2"><i class="fa fa fa-registered"></i> <?php echo $this->lang->line('register'); ?></button> 
				  	<span style="display: none;" id="register-loading"><img src="/images/load.gif" style="height: 70px;"></span>
				  </div>
				</form>
				<p><a href="<?php echo base_url($lng.'/login'); ?>"><i class="fa fa-sign-in"></i> <?php echo $this->lang->line('login'); ?></a></p>
			</div>
			<div class="card-block" id="register-success" style="display: none;">
				<div class="alert bg-primary" role="alert">
					<em class="fa fa-check-circle mr-2"></em> 
					<?php echo $this->lang->line('register_success'); ?><br>
					<hr>
					<div class="text-center">
						<a href="<?php echo base_url(); ?>login" class="btn" title="Login"><em class="fa fa-sign-in"></em> Login</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>