<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-xl-8">
		<div class="jumbotron">
			<h3 class="mb-4"><?php echo $this->lang->line('index_title_h3'); ?></h3>
				<p class="lead">&bull; <strong><?php echo $this->lang->line('index_item_1'); ?></strong></p>
				<p class="lead">&bull; <?php echo $this->lang->line('index_item_2'); ?></p>
				<p class="lead">&bull; <strong><?php echo $this->lang->line('index_item_3'); ?></strong></p>
			<h3 class="mb-4"><?php echo $this->lang->line('index_title_h3_2'); ?></h3>
			<p class="lead">&bull; <?php echo $this->lang->line('index_item_7'); ?></p>
			<p class="lead">&bull; <?php echo $this->lang->line('index_item_8'); ?></p>
				<div class="row justify-content-center">
					<img src="<?php echo base_url('images/teamwork.jpeg'); ?>" class="img-thumbnail" style="width: 80%;object-fit: cover;height: 320px">
				</div>
			<p class="lead">
				<a class="btn btn-primary btn-lg btn-block mt-2" href="<?php echo base_url($lng.'/register'); ?>" role="button">
					<i class="fa fa-circle-o"></i> <?php echo $this->lang->line('login_register'); ?>
				</a>
			</p>
		</div>
		
	</div>
<?php $this->load->view('inc/footer'); ?>