<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-xl-8" id="body-content">
		<div class="card mb-4">
			<div class="card-block">
				<h3 class="card-title">Basic agreements</h3>
				<div class="divider" style="margin-top: 1rem;"></div>
				<div class="articles-container">
					<div class="article border-bottom">
						<div class="col-xs-12">
							<h6>1.1. The project myperday.com (further - Project) is the financial organization with not a banking jurisdiction position.</h6>
							<h6>1.2. The Project is located at a single address in the Internet https://myperday.com</h6>
							<h6>1.3. The Project provides high-interest accruals on deposits</h6>
							<h6>1.4. The investor (further - User) accepts all the terms of this agreement when registering for the Project.</h6>
							<h6>1.5. The User understands all risks and possible force majeure situations associated with the Project.</h6>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="card-block">
				<h3 class="card-title">Financial terms</h3>
				<div class="divider" style="margin-top: 1rem;"></div>
				<div class="articles-container">
					<div class="article border-bottom">
						<div class="col-xs-12">
							<h6>2.1. The Project's currency is USD.</h6>
							<h6>2.2. The minimum deposit amount is 1 USD, the minimum withdrawal amount is 0.1$. More info here .</h6>
							<h6>2.3. Accepting of payments is carried out through payment systems PayPal, PerfectMoney and Bitcoin in automatic mode.</h6>
							<h6>2.4. Withdrawals are processing during 12 hours.</h6>
							<h6>2.5. When replenishing and withdrawing funds there are daily limits .</h6>
							<h6>2.6. Withdrawing funds is allowed once every 24 hours, for new accounts mandatory hold for withdrawing is 72 hours.</h6>
							<h6>2.7. The withdrawal of funds is possible only for those purses from which the replenishment was made.</h6>
							<h6>2.8. The designation of purses for withdrawal by the User is possible only on accounts intended for exclusive withdrawal of partner earnings.</h6>
							<h6>2.9. Interest rates of the Project are observed according to the table .</h6>
							<h6>2.10. When the interest rate is changed, the Project undertakes to inform the User about it no later than 15 days.</h6>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="card-block">
				<h3 class="card-title">Partnership program</h3>
				<div class="divider" style="margin-top: 1rem;"></div>
				<div class="articles-container">
					<div class="article border-bottom">
						<div class="col-xs-12">
							<h6>3.1. Each User receives his unique affiliate link during registration.</h6>
							<h6>3.2. Partner is assigned to the User who invited forever.</h6>
							<h6>3.3. The User get instantly to own balance 5x daily interest amount for each partner's deposit.</h6>
							<h6>3.4. In case of abuse of the partner program (for example, numerous hit-n-run), the probability of obtaining partner accruals can be lowered by the analytically module of the Project.</h6>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>						
<?php $this->load->view('inc/footer'); ?>