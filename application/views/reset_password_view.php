<?php $this->load->view('inc/header'); ?>

	<div class="col-md-12 col-xl-12">
		<div class="row">
			<div class="col-xl-12">
				<div class="card mb-3">
					<h4 class="card-header card-inverse card-primary" style="color: white;"> <i class="fa fa-lock"></i> <?php echo $this->lang->line('change_password'); ?></h4>
					<div class="card-block">
						<?php if($this->session->flashdata('error') != '') { ?>
							<div class="alert bg-danger" role="alert">
								<em class="fa fa-exclamation-triangle mr-2"></em> <?php echo $this->session->flashdata('error'); ?>
							</div>
						<?php } ?>
						<form method="POST" id="form-changepass">
						  <div class="form-group" id="block-user">
						    <label for="newPassword" class="text-muted"><i class="fa fa-user"></i> <?php echo $dataUser->email; ?></label>
						  </div>
						  <div class="form-group" id="block-password">
						    <label for="newPassword"><i class="fa fa-key"></i> <?php echo $this->lang->line('new_password'); ?></label>
								<div class="input-group">
								  <input type="password" class="form-control" id="password" name="newPassword" minlength="6" maxlength="32">
								  <span class="input-group-addon" id="show-password" title="Show/hide"><i class="fa fa-eye"></i></span>
								</div>
						  </div>
						  <div class="form-group" id="block-repeat-password">
						    <label for="repeatPassword"><i class="fa fa-key"></i> <?php echo $this->lang->line('confirm_password'); ?></label>
						    <input type="password" class="form-control" id="repeatPassword" name="repeatPassword" minlength="6" maxlength="32">
						  </div>
		      			  <span class="text-danger form-text" id="other-text"></span>
						  <div class="form-group">
						  	<button type="submit" name="submitReset" class="btn btn-primary btn-lg"><i class="fa fa-lock"></i> <?php echo $this->lang->line('confirm'); ?></button>
						  </div>
						</form>
			        </div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>