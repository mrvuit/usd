<?php $this->load->view('inc/header'); ?>

	<div class="col-md-12 col-xl-12">
		<div class="row">
			<div class="col-xl-12">
				<div class="card mb-3">
					<h4 class="card-header card-inverse card-primary" style="color: white;"> <i class="fa fa-check-circle"></i> <?php echo $this->lang->line('account_verification'); ?></h4>
					<div class="card-block">
						<?php if($user['activated'] == 1) { ?>
						<p class="lead text-success"><em class="fa fa-check-circle"></em> <?php echo $this->lang->line('account_verified'); ?></p>
						<?php } else { ?>
						<p class="lead text-danger"><em class="fa fa-times"></em> <?php echo $this->lang->line('account_not_verified'); ?></p>
						<p>
							<a href="<?php echo base_url('deposit'); ?>" class="btn btn-primary"><em class="fa fa-arrow-circle-o-up"></em> <?php echo $this->lang->line('deposit_now'); ?></a>
						</p>
						<?php } ?>
						<p class="lead"><i class="fa fa-user"></i> <?php echo $this->lang->line('your_email'); ?> <?php echo $user['email']; ?></p>
			        </div>
				</div>
				<div class="card mb-3">
					<h4 class="card-header card-inverse card-primary" style="color: white;"> <i class="fa fa-lock"></i> <?php echo $this->lang->line('change_password'); ?></h4>
					<div class="card-block">
						<?php if($this->session->flashdata('error') != '') { ?>
							<div class="alert bg-danger" role="alert">
								<em class="fa fa-exclamation-triangle mr-2"></em> <?php echo $this->session->flashdata('error'); ?>
							</div>
						<?php } ?>
						<form method="POST" id="form-changepass">
						  <div class="form-group" id="block-oldPassword">
						    <label for="oldPassword"><i class="fa fa-key"></i> <?php echo $this->lang->line('old_password'); ?></label>
						    <input type="password" class="form-control" id="oldPassword" name="oldPassword">
						  </div>
						  <div class="form-group" id="block-password">
						    <label for="newPassword"><i class="fa fa-key"></i> <?php echo $this->lang->line('new_password'); ?></label>
								<div class="input-group">
								  <input type="password" class="form-control" id="password" name="newPassword" minlength="6" maxlength="32">
								  <span class="input-group-addon" id="show-password" title="Show/hide"><i class="fa fa-eye"></i></span>
								</div>
						  </div>
						  <div class="form-group" id="block-repeat-password">
						    <label for="repeatPassword"><i class="fa fa-key"></i> <?php echo $this->lang->line('confirm_password'); ?></label>
						    <input type="password" class="form-control" id="repeatPassword" name="repeatPassword" minlength="6" maxlength="32">
						  </div>
		      			  <span class="text-danger form-text" id="other-text"></span>
						  <div class="form-group">
						  	<button type="submit" name="submit" class="btn btn-primary btn-lg"><i class="fa fa-lock"></i> <?php echo $this->lang->line('confirm'); ?></button>
						  </div>
						  <span class="text-muted"><?php echo $this->lang->line('change_password_note'); ?></span>
						</form>
			        </div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>