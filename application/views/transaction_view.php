<?php $this->load->view('inc/header'); ?>
	<div class="col-md-8 offset-md-2" style="display: none;" id="div-img-transaction">
			<div class="card-block text-center">
				<h3 class="card-title"><?php echo $this->lang->line('transaction_proof'); ?></h3>
				<div id="show-img"></div>
			</div>
	</div>
	<img src="" id="scream">
	<div class="col-md-12 col-lg-<?php echo (isset($menu_right) ? '12' : '8'); ?>">

		<div class="card mb-4">
			<div class="card-block">
				<h3 class="card-title"><button class="btn btn-primary btn-sm" onclick="takeScreenShot();"><i class="fa fa-picture-o"></i> <?php echo $this->lang->line('take_image'); ?></button></h3>
				<div class="" style="background: #FFFFFF;" id="transaction_list">
					<table class="table table-hover">
					    <thead>
					      <tr>
					        <th>Id</th>
					        <th><?php echo $this->lang->line('amount'); ?></th>
					        <th><?php echo $this->lang->line('wallet'); ?></th>
					        <th class="hidden-sm-down"><?php echo $this->lang->line('time'); ?></th>
					        <th class="hidden-sm-down"><?php echo $this->lang->line('type'); ?></th>
					        <th><?php echo $this->lang->line('status'); ?></th>
					        <th><?php echo $this->lang->line('cancel'); ?></th>
					      </tr>
					    </thead>
					    <tbody>
					    	<?php if($list_transactions != false) foreach ($list_transactions as $key => $value) { ?>
						      <tr>
						        <td><strong>#<?php echo $value['id']; ?></strong></td>
					        	<td><i class="fa fa-usd"></i> <?php echo $value['money']; ?></td>
					        	<td><img src="<?php echo base_url('images/icon/wallet/small/'.$value['wallet_cat']['simplename'].'.png'); ?>"> <?php echo $value['wallet_cat']['name']; ?></td>
						        <td class="hidden-sm-down"><?php echo $value['time']; ?></td>
						        <td class="hidden-sm-down"><?php echo $value['type']; ?></td>
						        <td><?php echo $value['status']; ?></td>
						        <td>
						        	<?php if($value['cancel'] == 1) { ?>
						        	<button class="btn btn-link btn-xs" onclick="confirmCancelTs(<?php echo $value['id']; ?>);" type="button">
						        		<i class="fa fa-times"></i> <?php echo $this->lang->line('cancel'); ?>
						        	</button>
						        	<?php } else { ?>
						        	<?php echo $this->lang->line('cannot'); ?>
						        	<?php } ?>
						        </td>
						      </tr>
						      <tr ><td colspan="7"><i class="fa fa-comment-o"></i> <?php echo $value['comment']; ?></td> </tr>
					    	<?php } ?>
					    	<tr>
					    		<td colspan="7"><h4>MyPerDay.Com</h4></td>
					    	</tr>
					    </tbody>
					  </table>
				</div>
			</div>
		</div>
		<div class="modal fade" id="model-cancel">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title"><?php echo $this->lang->line('confirm_cancel'); ?> <strong>(Id: #<span id="id-cancel">1</span>)</strong></h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		        <?php echo $this->lang->line('cancel_note'); ?>
		      </div>
		      <div class="modal-footer">
		        <a class="btn btn-danger btn-sm" href="" id="link-cancel"><?php echo $this->lang->line('confirm'); ?></a>
		        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><?php echo $this->lang->line('close'); ?></button>
		      </div>

		    </div>
		  </div>
		</div>
	</div>

<?php $this->load->view('inc/footer'); ?>