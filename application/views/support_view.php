<?php $this->load->view('inc/header'); ?>
	<div class="col-md-12 col-xl-8">
		<div class="card mb-4" id="body-content">
			<div class="card-block">
				<?php if($this->session->flashdata('status') != '') { ?>
				<div class="alert bg-success" role="alert">
					<em class="fa fa-comment mr-2"></em> <?php echo $this->session->flashdata('status'); ?>
				</div>
				<?php } ?>

				<form method="POST">
				  <div class="form-group">
				    <label for="formGroupExampleInput"><?php echo $this->lang->line('your_email'); ?></label>
				    <input type="text" type="email" class="form-control" name="email" value="<?php echo (isset($user['email']) ? $user['email'] : ''); ?>">
				    <?php if($this->session->flashdata('email') != '') { ?>
				    <span class="text-danger form-text" id="email-text">
				    	<i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('email'); ?>
				    </span>
				    <?php } ?>
				  </div>
				  <div class="form-group">
				    <label for="formGroupExampleInput2"><?php echo $this->lang->line('your_message'); ?></label>
				    <textarea class="form-control" placeholder="" name="message" rows="10"></textarea>
				    <?php if($this->session->flashdata('message') != '') { ?>
				    <span class="text-danger form-text" id="message-text">
				    	<i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('message'); ?>
				    </span>
				    <?php } ?>
				  </div>
				  <div class="g-recaptcha" data-sitekey="6LfHkD4UAAAAABO6R-6b_EZI890_QJoFqLHjKaXb"></div>
				    <?php if($this->session->flashdata('captcha') != '') { ?>
				    <span class="text-danger form-text" id="captcha-text">
				    	<i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('captcha'); ?>
				    </span>
				    <?php } ?>
				  <div class="form-group">
				  	<button class="btn btn-primary btn-lg mt-2" name="submit" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i><?php echo $this->lang->line('send'); ?></button>
				  </div>
				</form>

				<hr>
				<i class="fa fa-lightbulb-o"></i> <?php echo $this->lang->line('support_note'); ?>
			</div>
		</div>
	</div>
<?php $this->load->view('inc/footer'); ?>

